//
//  Card.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 4.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


enum CardType: String {
	case User, LinkedIn, Twitter, Endorsement, Topic, BioName, BioAge
}


class Card: APIEntity {
	var closeCallback: ((Void) -> Void)?
}


class UserCard: Card {
	var text: String!
	var language: String!
	var price: Money?
	var creationDate: NSDate!
	var user: User!
	var topics: [Topic]!
	var live: Bool = false
	var numberOfViews: Int = 0
	var caller: User?
}


class SystemEndorsementCard: Card {
	var user: User!
	var topic: Topic!
}


class TopicCard: Card {
	var topic: Topic!
}


class PushNotificationsCard: Card {
}
