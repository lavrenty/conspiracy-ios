//
//  DashboardViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import FBSDKLoginKit


let UserCardDidUpdateNotification = "UserCardDidUpdateNotification"
let OpenURLNotification = "OpenURLNotification"


class DashboardViewController: UIViewController, CardViewActionDelegate, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
	@IBOutlet weak var dashboardView: DashboardView!
	@IBOutlet weak var spinnerView: UIView!
	@IBOutlet weak var spinner: Spinner!
	@IBOutlet weak var spinnerLabel: UILabel!
	@IBOutlet weak var errorLabel: UILabel!
	@IBOutlet weak var APIServerLabel: UILabel!
	private var authenticationController: UIViewController?
	private var webSocket: APIWebSocket?
	private var tapToConnectRecognizer: UITapGestureRecognizer!
	private var reconnectInterval: NSTimeInterval = 0
	private var shouldReconnectWhenActive = false
	private let cardCount = 4
	private var openCard: Card?
	private var openCardView: CardView?
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	#if INTERNAL_TEST_BUILD
	internal func showTest(recognizer: UIRotationGestureRecognizer) {
		if recognizer.state == .Ended {
			if recognizer.rotation < -CGFloat(M_PI_2) {
				self.presentViewController(UINavigationController(rootViewController: TestsViewController()), animated: true, completion: nil)
			}
			if recognizer.rotation > CGFloat(M_PI_2) {
				self.presentViewController(UINavigationController(rootViewController: APIServerPicker()), animated: true, completion: nil)
			}
		}
	}
	#endif
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		#if INTERNAL_TEST_BUILD
		let testGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(DashboardViewController.showTest(_:)))
		self.view.addGestureRecognizer(testGestureRecognizer)
		
		APIServerLabel.text = API.sharedAPI.APIServerPreference
		APIServerLabel.hidden = false
		#endif
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.radarDidConnect(_:)), name: RadarDidConnectNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.radarDidDisconnect(_:)), name: RadarDidDisconnectNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.cardHasBeenAnswered(_:)), name: CardHasBeenAnsweredNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.showCurrentUserProfile(_:)), name: OpenURLNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.userLanguagesDidUpdate(_:)), name: UserLanguagesDidUpdateNotification, object: nil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DashboardViewController.applicationDidBecomeActive(_:)), name: UIApplicationDidBecomeActiveNotification, object: nil)
		
		errorLabel.text = NSLocalizedString("dashboard.label.failed to connect", comment: "Dashboard")
		spinnerLabel.text = NSLocalizedString("dashboard.label.connecting", comment: "Dashboard")
		
		tapToConnectRecognizer = UITapGestureRecognizer(target: self, action: #selector(DashboardViewController.retryConnectingWebSocket))
		tapToConnectRecognizer.enabled = false
		dashboardView.addGestureRecognizer(tapToConnectRecognizer)
		
		for _ in 1...cardCount {
			let slotView = CardSlotView(frame: CGRectZero, actionDelegate: self)
			slotView.swipeHandler = { (direction) -> Void in
				self.closeCard(slotView, direction: direction)
			}
			
			let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(DashboardViewController.handleCardTap(_:)))
			slotView.addGestureRecognizer(tapRecognizer)
			
			dashboardView.addArrangedSubview(slotView)
		}
		
		if API.sharedAPI.accessToken == nil {
			showAuthorizationController()
		} else {
			API.sharedAPI.getCurrentUser(nil)
		}
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		if self.authenticationController == nil {
			Log("\(#function) calling connectWebSocket()")
			connectWebSocket()
		}
	}
	
	func showAuthorizationController() {
		disconnectWebSocket()
		webSocket = nil
		
		let viewController = OnboardingViewController { (authorized) -> Void in
			if authorized {
				let authController = self.authenticationController!
				UIView.animateWithDuration(0.4,
					animations: { () -> Void in
						authController.view.alpha = 0
					},
					completion: { (finished) -> Void in
						authController.willMoveToParentViewController(nil)
						authController.view.removeFromSuperview()
						authController.removeFromParentViewController()
						self.authenticationController = nil
						self.setNeedsStatusBarAppearanceUpdate()
				})
				Log("Authorized, calling connectWebSocket()")
				self.connectWebSocket()
			}
		}
		viewController.view.frame = self.view.bounds
		self.addChildViewController(viewController)
		self.view.addSubview(viewController.view)
		viewController.didMoveToParentViewController(self)
		authenticationController = viewController
		self.setNeedsStatusBarAppearanceUpdate()
	}
	
	// MARK: -
	
	func connectWebSocket() {
		self.errorLabel.hidden = true
		self.spinnerView.hidden = false
		self.spinner.startAnimating()
		self.tapToConnectRecognizer.enabled = false
		
		if webSocket == nil {
			setupWebSocket()
		}
		webSocket?.open()
	}
	
	func disconnectWebSocket() {
		webSocket?.close()
		removeAllCards()
	}
	
	func retryConnectingWebSocket() {
		self.shouldReconnectWhenActive = false
		if self.webSocket != nil {
			Log("\(#function)")
			self.errorLabel.hidden = true
			self.spinnerView.hidden = false
			self.spinner.startAnimating()
			self.tapToConnectRecognizer.enabled = false
			
			self.webSocket!.open()
		}
	}
	
	func setupWebSocket() {
		guard webSocket == nil else { return } // the web socket is already set up
		guard API.sharedAPI.accessToken != nil else { return } // not authorized
		
//		Log("\(#function)")
		webSocket = APIWebSocket(APIEndpoint: "/cards/dashboard")
		webSocket!.addRoute("update", handler: { (message) -> Void in
			let cardID = message["card_id"] as! Int
			let matchingSlotView = self.slotViewMatchingPredicate({ (card) -> Bool in
				card != nil && card!.identifier == cardID
			})
			if let theMatchingSlotView = matchingSlotView {
				theMatchingSlotView.updateCardView(message)
				NSNotificationCenter.defaultCenter().postNotificationName(UserCardDidUpdateNotification, object: theMatchingSlotView.card)
			} else {
				Log("*** Card not found: \(message)")
			}
		})
		
		webSocket!.addRoute("show", handler: { (message) -> Void in
			let card = API.sharedAPI.parseCardObject(message["card"] as! [String: AnyObject], json: message)
//			card.closeCallback = {
//				self.closeCard(card, direction: .Left)
//			}
			self.animateShowCard(card)
		})
		
		webSocket!.addRoute("system", handler: { (message) -> Void in
			if let card = API.sharedAPI.parseSystemCardObject(message) {
				card.closeCallback = {
					self.closeCard(card, direction: .Left)
					self.dismissViewControllerAnimated(true, completion: nil)
				}
				self.animateShowCard(card)
			} else {
				Log("*** Failed to parse message: \(message)")
			}
		})
		
		webSocket!.addRoute("close", handler: { (message) -> Void in
			self.webSocket!.call("close", parameters: message)
			self.animateHideCard(message["card_id"] as! Int)
		})
		
		webSocket!.onOpenRoute = {
			self.removeAllCards()
			
			self.spinner.stopAnimating()
			self.spinnerView.hidden = true
			self.tapToConnectRecognizer.enabled = false
			for sv in self.dashboardView.subviews {
				sv.hidden = false
			}
		}
		
		webSocket!.onErrorRoute = { (error) -> Void in
			if self.shouldReconnectWhenActive {
				Log("Failed to connect: \(error)")
				self.errorLabel.hidden = false
				self.spinner.stopAnimating()
				self.spinnerView.hidden = true
				self.tapToConnectRecognizer.enabled = true
				for sv in self.dashboardView.subviews {
					sv.hidden = true
				}
			}
		}
		
		webSocket!.onCloseRoute = {
			Log("Web socket is closed")
			self.removeAllCards()
			
			if self.webSocket != nil && !self.shouldReconnectWhenActive {
				self.spinnerView.hidden = false
				self.spinner.startAnimating()
				
				Log("Reconnecting in \(self.reconnectInterval) seconds...")
				let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(self.reconnectInterval * Double(NSEC_PER_SEC)))
				dispatch_after(delayTime, dispatch_get_main_queue()) {
					self.webSocket!.open()
				}
				self.reconnectInterval = self.reconnectInterval == 0 ? 1 : 2 * self.reconnectInterval
				if self.reconnectInterval > 8 {
					self.reconnectInterval = 0
					self.shouldReconnectWhenActive = true
				}
			}
		}
	}
	
	func applicationDidBecomeActive(notification: NSNotification) {
		if self.shouldReconnectWhenActive {
			retryConnectingWebSocket()
		}
	}
	
	func animateShowCard(card: Card) {
		let emptySlotView = slotViewMatchingPredicate({ (card) -> Bool in
			card == nil
		})
		if emptySlotView != nil {
			emptySlotView!.card = card
		} else {
			Log("*** No empty slots found")
		}
	}
	
	func animateHideCard(cardID: Int) {
		let matchingSlotView = slotViewMatchingPredicate({ (card) -> Bool in
			card != nil && card!.identifier == cardID
		})
		if matchingSlotView != nil {
			matchingSlotView!.card = nil
		} else {
			Log("*** Card not found: \(cardID)")
		}
	}
	
	private func slotViewMatchingPredicate(predicate: (card: Card?) -> Bool) -> CardSlotView? {
		for slotView in dashboardView.arrangedSubviews {
			if predicate(card: slotView.card) {
				return slotView
			}
		}
		return nil
	}
	
	func radarDidConnect(notification: NSNotification) {
		Log("\(#function) calling disconnectWebSocket()")
		disconnectWebSocket()
	}
	
	func radarDidDisconnect(notification: NSNotification) {
		Log("\(#function) calling connectWebSocket()")
		connectWebSocket()
	}
	
	func cardHasBeenAnswered(notification: NSNotification) {
		Log("\(#function) calling connectWebSocket()")
		connectWebSocket()
	}
	
	func showCurrentUserProfile(notification: NSNotification) {
		Log("object: \(notification.object)")
		self.performSegueWithIdentifier("showUserProfile", sender: self)
	}
	
	func userLanguagesDidUpdate(notification: NSNotification) {
		Log("\(#function) reconnecting web socket")
		disconnectWebSocket()
		connectWebSocket()
	}
	
	func handleCardTap(sender: UITapGestureRecognizer) {
		let slotView = sender.view! as! CardSlotView
		openCard = slotView.card
		if openCard != nil && slotView.cardView != nil {
			openCardView = slotView.cardView
			self.performSegueWithIdentifier("openCard", sender: sender)
		} else {
			openCardView = nil
		}
	}
	
	func openCard(card: Card) {
		if self.presentedViewController != nil {
			self.dismissViewControllerAnimated(true, completion: { 
				self.openCard(card)
			})
			return
		}
		
		openCard = card
		self.performSegueWithIdentifier("openCard", sender: self)
	}
	
	func closeCard(slotView: CardSlotView, direction: UISwipeGestureRecognizerDirection) {
		if let card = slotView.card {
			self.closeCard(card, direction: direction)
		}
	}
	
	func closeCard(card: Card, direction: UISwipeGestureRecognizerDirection) {
		if webSocket != nil {
			let parameters: [String: AnyObject]
			if card is UserCard {
				parameters = [
					"card_id": card.identifier,
					"swipe": direction == .Right ? "right" : "left"
				]
			} else {
				parameters = [
					"system_id": card.identifier,
					"swipe": direction == .Right ? "right" : "left"
				]
			}
			webSocket!.call("close", parameters: parameters)
			animateHideCard(card.identifier)
		}
	}
	
	func removeAllCards() {
		for slotView in dashboardView.arrangedSubviews {
			slotView.card = nil
		}
	}
	
//	func reloadCards() {
//		if webSocket != nil {
//			for slotView in dashboardView.arrangedSubviews {
//				if let card = slotView.card {
//					webSocket!.call("close", parameters: ["card_id": card.identifier])
//					slotView.card = nil
//				}
//			}
//		}
//	}
	
	override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
		if identifier == "addCard" {
			return User.currentUser != nil
		}
		return true
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "openCard" {
			segue.destinationViewController.transitioningDelegate = self
			let nc = (segue.destinationViewController as! SpecialPresentationController).contentViewController as! UINavigationController
			let vc = nc.topViewController! as! OpenCardViewController
			vc.card = openCard!
			vc.actionDelegate = self
			
			openCard = nil
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		let childViewController = self.childViewControllers.last
		return childViewController == nil ? .LightContent : childViewController!.preferredStatusBarStyle()
	}
}


// MARK: - UIViewControllerTransitioningDelegate


extension DashboardViewController {
	func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//		Log("\(presented.dynamicType) \(presenting.dynamicType) \(source.dynamicType)")
		return self.openCardView != nil ? self : nil
	}
	
	func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		if self.openCardView == nil { return nil }
		if let presentationController = dismissed as? SpecialPresentationController {
			if let navigationController = presentationController.contentViewController as? UINavigationController {
				if navigationController.topViewController is OpenCardViewController {
					return self
				}
			}
		}
		return nil
	}
}


// MARK: - UIViewControllerAnimatedTransitioning


extension DashboardViewController {
	func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
		return 0.3
	}
	
	func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
		guard let containerView = transitionContext.containerView() else {
			fatalError("No container view")
		}
		guard let cardView = self.openCardView else {
			fatalError("No card view")
		}
		
		let tempView = UIView(frame: cardView.bounds)
		tempView.layer.cornerRadius = cardView.layer.cornerRadius
		tempView.alpha = 0
		containerView.addSubview(tempView)
		
		if transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) != self {
			// Presenting
//			Log("Presenting \(transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!)")
			
			tempView.backgroundColor = cardView.backgroundColor
			tempView.frame = self.view.convertRect(cardView.bounds, fromView: cardView)
			
			let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)! as! SpecialPresentationController
			let toView = toVC.view //transitionContext.viewForKey(UITransitionContextToViewKey)!
			let openCardViewController = (toVC.contentViewController as! UINavigationController).topViewController as! OpenCardViewController
			toView.alpha = 0
			toView.frame = containerView.bounds
			toView.layoutIfNeeded()
			containerView.addSubview(toView)
			
			let finalFrame = toVC.contentViewController.view.frame
			
			let options = UIViewKeyframeAnimationOptions(rawValue: UIViewAnimationOptions.CurveLinear.rawValue)
			UIView.animateKeyframesWithDuration(self.transitionDuration(transitionContext), delay: 0, options: options, animations: { () -> Void in
				UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.33, animations: { () -> Void in
					tempView.alpha = 1
				})
				UIView.addKeyframeWithRelativeStartTime(0.33, relativeDuration: 0.67, animations: { () -> Void in
					tempView.frame = finalFrame
					tempView.backgroundColor = openCardViewController.view.backgroundColor
				})
				UIView.addKeyframeWithRelativeStartTime(1.0, relativeDuration: 0.0, animations: { () -> Void in
					toView.alpha = 1
				})
				}, completion: { (finished) -> Void in
					tempView.removeFromSuperview()
					transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
			})
		} else {
			// Dismissing
//			Log("Dismissing \(transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!)")
			
			let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)! as! SpecialPresentationController
			let openCardViewController = (fromVC.contentViewController as! UINavigationController).topViewController as! OpenCardViewController
			
			tempView.backgroundColor = openCardViewController.view.backgroundColor
			tempView.frame = containerView.convertRect(openCardViewController.view.frame, fromView: openCardViewController.view.superview!)
			
			let finalFrame = self.view.convertRect(cardView.bounds, fromView: cardView)
			
			let options = UIViewKeyframeAnimationOptions(rawValue: UIViewAnimationOptions.CurveLinear.rawValue)
			UIView.animateKeyframesWithDuration(self.transitionDuration(transitionContext), delay: 0, options: options, animations: { () -> Void in
				UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 0.0, animations: { () -> Void in
					tempView.alpha = 1
					openCardViewController.view.hidden = true
				})
				UIView.addKeyframeWithRelativeStartTime(0.0, relativeDuration: 0.67, animations: { () -> Void in
					tempView.frame = finalFrame
					tempView.backgroundColor = cardView.backgroundColor
				})
				UIView.addKeyframeWithRelativeStartTime(0.67, relativeDuration: 0.33, animations: { () -> Void in
					tempView.alpha = 0
				})
				}, completion: { (finished) -> Void in
					tempView.removeFromSuperview()
					transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
			})
		}
	}
}


// MARK: - CardViewActionDelegate


extension DashboardViewController {
	func endorse(card: SystemEndorsementCard) {
		API.sharedAPI.endorseUser(card.user, topic: card.topic, completion: { (error) -> Void in
			if error != nil {
				self.reportError(error)
			} else {
				self.closeCard(card, direction: .Left)
			}
		})
	}
	
	func addTopic(card: TopicCard) {
		API.sharedAPI.subscribeToTopic(card.topic) { (topic, error) -> Void in
			if error != nil {
				self.reportError(error)
			} else {
				self.closeCard(card, direction: .Left)
			}
		}
	}
	
	func enablePushNotifications(card: PushNotificationsCard) {
		API.sharedAPI.registerDeviceForPushNotifications { (error) -> Void in
			if error != nil {
				self.reportError(error)
			} else {
				self.closeCard(card, direction: .Left)
			}
		}
	}
	
	func answerQuestion(card: UserCard) {
		Log("\(#function) calling disconnectWebSocket()")
		disconnectWebSocket()
	}
	
	func closeCard(card: Card) {
		self.closeCard(card, direction: .Left)
	}
}
