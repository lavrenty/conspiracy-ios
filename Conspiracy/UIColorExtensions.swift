//
//  UIColorExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 4.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


extension UIColor {
	class func brandBlueColor() -> UIColor {
		return UIColor(red: 74.0/255.0, green: 144.0/255.0, blue: 226.0/255.0, alpha: 1.0)
	}
	class func brandGreenColor() -> UIColor {
		return UIColor(red: 33.0/255.0, green: 211.0/255.0, blue: 128.0/255.0, alpha: 1.0)
	}
	class func brandOrangeColor() -> UIColor {
		return UIColor(red: 245.0/255.0, green: 166.0/255.0, blue: 35.0/255.0, alpha: 1.0)
	}
}
