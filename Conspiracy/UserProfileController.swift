//
//  UserProfileController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 13.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import FBSDKLoginKit


let UserLanguagesDidUpdateNotification = "UserLanguagesDidUpdateNotification"


class UserProfileController: UIViewController {
	@IBOutlet weak var closeButton: UIButton!
	@IBOutlet weak var settingsButton: UIButton!
	@IBOutlet weak var badge: UIView!
	@IBOutlet weak var badgeTopConstraint: NSLayoutConstraint!
	@IBOutlet weak var badgeTitleLabel: UILabel!
	@IBOutlet weak var badgeMessageLabel: UILabel!
	@IBOutlet weak var avatarView: AvatarView!
	@IBOutlet weak var userNameLabel: UILabel!
	@IBOutlet weak var facebookButton: SocialButton!
	@IBOutlet weak var linkedInButton: SocialButton!
	@IBOutlet weak var answerLegendLabel: UILabel!
	@IBOutlet weak var answerStatLabel: UILabel!
	@IBOutlet weak var questionLegendLabel: UILabel!
	@IBOutlet weak var questionStatLabel: UILabel!
	@IBOutlet weak var balanceLegendLabel: UILabel!
	@IBOutlet weak var balanceValueLabel: UILabel!
	@IBOutlet weak var notificationLegendLabel: UILabel!
	@IBOutlet weak var notificationSwitch: UISwitch!
	@IBOutlet weak var languagesLegendLabel: UILabel!
	@IBOutlet weak var languagesLabel: LanguagesLabel!
	@IBOutlet weak var topicTableView: UITableView!
	@IBOutlet weak var addTopicButton: UIButton!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	private var topics: [Topic]?
	private var user: User! {
		didSet {
			topics = user!.topics ?? []
			topics = topics!.filter { return !$0.isSuggested }
			sortTopics()
		}
	}
	private var suggestedTopics: [Topic]?
	
	// MARK: -
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.user = User.currentUser!
		
		answerLegendLabel.text = NSLocalizedString("profile.legend.answers", comment: "Profile")
		questionLegendLabel.text = NSLocalizedString("profile.legend.questions", comment: "Profile")
		balanceLegendLabel.text = NSLocalizedString("profile.legend.balance", comment: "Profile")
		notificationLegendLabel.text = NSLocalizedString("profile.legend.notifications", comment: "Profile")
		languagesLegendLabel.text = NSLocalizedString("profile.legend.languages", comment: "Profile")
		addTopicButton.setTitle(NSLocalizedString("profile.button.add topic", comment: "Profile"), forState: .Normal)
		
		topicTableView.hidden = true
		addTopicButton.hidden = true
		activityIndicator.startAnimating()
		API.sharedAPI.getCurrentUser({ (user, error) -> Void in
			if user != nil {
				self.user = user
			}
			if self.isViewLoaded() {
				self.updateUI()
				self.activityIndicator.stopAnimating()
				self.topicTableView.hidden = false
				self.addTopicButton.hidden = false
			}
		})
		
		API.sharedAPI.getSuggestedTopicsForUserProfile { (topics, error) -> Void in
			self.suggestedTopics = topics
		}
	}
	
	func sortTopics() {
		topics?.sortInPlace {
			if $0.callCount != $1.callCount {
				return $0.callCount > $1.callCount
			}
			return $0.name.localizedCaseInsensitiveCompare($1.name) == NSComparisonResult.OrderedAscending
		}
	}
	
	func updateUI() {
		userNameLabel.text = user.fullName
		avatarView.user = user
		facebookButton.connected = user.hasConnectedFacebook
		linkedInButton.connected = user.hasConnectedLinkedIn
		answerStatLabel.text = "\(user.answerCount)"
		questionStatLabel.text = "\(user.questionCount)"
		
		if user.isVerified && !user.hasUnpaidInvoice {
			badge.hidden = true
			badgeTopConstraint.constant = 40.0 - CGRectGetHeight(badge.frame)
			closeButton.tintColor = UIColor.blackColor()
			settingsButton.tintColor = UIColor.blackColor()
		} else {
			if !user.isVerified {
				badge.hidden = false
				badgeTopConstraint.constant = 0
				badge.backgroundColor = UIColor.brandBlueColor()
				badgeTitleLabel.text = NSLocalizedString("profile.badge.unverified account.title", comment: "Profile")
				badgeMessageLabel.text = NSLocalizedString("profile.badge.unverified account.message", comment: "Profile")
				closeButton.tintColor = UIColor.whiteColor()
				settingsButton.tintColor = UIColor.whiteColor()
			} else
			if user.hasUnpaidInvoice {
				badge.hidden = false
				badgeTopConstraint.constant = 0
				badge.backgroundColor = UIColor.brandOrangeColor()
				badgeTitleLabel.text = NSLocalizedString("profile.badge.unpaid invoice.title", comment: "Profile")
				badgeMessageLabel.text = NSLocalizedString("profile.badge.unpaid invoice.message", comment: "Profile")
				closeButton.tintColor = UIColor.blackColor()
				settingsButton.tintColor = UIColor.blackColor()
			}
		}
		let headerView = topicTableView.tableHeaderView!
		headerView.frame.size.height = headerView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
		topicTableView.tableHeaderView = headerView
		
		balanceValueLabel.text = user.balance.formattedValue
		notificationSwitch.on = user.hasPushNotificationsEnabled
		languagesLabel.languageCodes = user.languages
		topicTableView.reloadData()
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if let segueIdentifier = segue.identifier {
			if segueIdentifier == "addTopic" {
				let topicPicker = segue.destinationViewController as! TopicPicker
				topicPicker.suggestedTopics = suggestedTopics
				topicPicker.excludedTopics = topics
				topicPicker.canAddNewTopic = true
				topicPicker.completion = { (topic) -> Void in
					API.sharedAPI.subscribeToTopic(topic) { (theTopic, error) -> Void in
						if error != nil {
							self.reportError(error)
						} else {
							self.topics!.append(topic)
							self.user.topics?.append(topic)
							self.sortTopics()
							self.topicTableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self.topics!.indexOf(topic)!, inSection: 0)], withRowAnimation: .Automatic)
						}
					}
				}
			} else
			if segueIdentifier == "selectLanguages" {
				let languagePicker = segue.destinationViewController as! LanguagePicker
				languagePicker.selectedLanguages = user.languages
				languagePicker.completion = { (languageCodes) -> Void in
//					Log("\(languageCodes)")
					let oldLanguages = Set(self.user.languages!)
					let newLanguages = Set(languageCodes)
					if newLanguages != oldLanguages {
						API.sharedAPI.updateUserLanguages(languageCodes, completion: { (error) in
							if error == nil {
								self.languagesLabel.languageCodes = self.user.languages
								NSNotificationCenter.defaultCenter().postNotificationName(UserLanguagesDidUpdateNotification, object: nil)
							} else {
								self.reportError(error)
							}
						})
					}
				}
			}
		}
	}
	
	@IBAction func connectLinkedIn() {
	}
	
	@IBAction func showPayoutDetails() {
		let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/billing/setup")!
		let webViewController = WebViewController.navigationController(url, title: NSLocalizedString("payout.controller title", comment: "Payout"), excludedDomains: ["stripe.com"], callback: { (callbackURL) -> Void in
			API.sharedAPI.getCurrentUser(nil)
		})
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
	
	@IBAction func toggleNotifications() {
		if notificationSwitch.on {
			API.sharedAPI.registerDeviceForPushNotifications { (error) -> Void in
				if error != nil {
					self.reportError(error)
					self.notificationSwitch.on = false
					return
				}
				
				self.user.hasPushNotificationsEnabled = true
			}
		} else {
			API.sharedAPI.unregisterDeviceForPushNotifications { (error) -> Void in
				if error != nil {
					self.reportError(error)
					self.notificationSwitch.on = true
					return
				}
				
				self.user.hasPushNotificationsEnabled = false
			}
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
}


// MARK: - UITableViewDataSource


extension UserProfileController: UITableViewDataSource {
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return topics != nil ? topics!.count : 0
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let topic = topics![indexPath.row]
		
		let cell = tableView.dequeueReusableCellWithIdentifier("topic", forIndexPath: indexPath) as! TopicStatsTableCell
		cell.topicNameLabel!.text = topic.name.isEmpty ? "<untitled topic>" : topic.name
		cell.callCountLabel!.text = "\(topic.callCount)"
		return cell
	}
}


// MARK: - UITableViewDelegate


extension UserProfileController: UITableViewDelegate {
	func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
		return [UITableViewRowAction(style: UITableViewRowActionStyle.Destructive, title: "Delete", handler: { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
			let topic = self.topics![indexPath.row]
			API.sharedAPI.unsubscribeFromTopic(topic) { (error) -> Void in
				if error != nil {
					self.reportError(error)
				} else {
					self.topics!.removeAtIndex(indexPath.row)
					self.user.topics = self.topics
					tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
				}
			}
		})]
	}
}


// MARK: - TopicStatsTableCell


class TopicStatsTableCell: UITableViewCell {
	@IBOutlet weak var topicNameLabel: UILabel!
	@IBOutlet weak var callCountLabel: UILabel!
}


// MARK: - LanguagesLabel


class LanguagesLabel: UILabel {
	var languageCodes: [String] = [] {
		didSet {
			if languageCodes.isEmpty {
				self.text = ""
			} else {
				self.setNeedsLayout()
			}
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		var text = ""
		if !languageCodes.isEmpty {
			let labelWidth = CGRectGetWidth(self.bounds)
			let locale = NSLocale.currentLocale()
			var languageNameArray = languageCodes.map { locale.displayNameForKey(NSLocaleLanguageCode, value: $0)! }
			var numOthers = 0
			repeat {
				let andOthersText = NSString(format: NSLocalizedString("and %d others", comment: ""), numOthers) as String
				text = ((languageNameArray as NSArray).componentsJoinedByString(", ") + andOthersText).uppercaseString
				if self.sizeForText(text).width > labelWidth {
					languageNameArray.removeLast()
					numOthers = numOthers + 1
				} else {
					break
				}
			} while !languageNameArray.isEmpty
		}
		
		if self.text != text {
			self.text = text
		}
	}
	
	private var internalLabel: UILabel?
	private func sizeForText(text: String) -> CGSize {
		if internalLabel == nil {
			internalLabel = UILabel(frame: self.frame)
			internalLabel!.font = self.font
			internalLabel!.lineBreakMode = self.lineBreakMode
			internalLabel!.numberOfLines = self.numberOfLines
			internalLabel!.adjustsFontSizeToFitWidth = self.adjustsFontSizeToFitWidth
			internalLabel!.minimumScaleFactor = self.minimumScaleFactor
			internalLabel!.allowsDefaultTighteningForTruncation = self.allowsDefaultTighteningForTruncation
		}
		
		internalLabel!.text = text
		return internalLabel!.intrinsicContentSize()
	}
}
