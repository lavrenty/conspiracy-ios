//
//  Sheet.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 1.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class Sheet: UIViewController, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
	private var sheetTitle: String?
	private var sheetMessage: String?
	private var actions: [UIAlertAction]!
	private var dimmingView: UIView!
	private var contentView: UIView!
	private var titleLabel: UILabel?
	private var messageLabel: UILabel?
	private var buttons: [UIButton]!
	var backgroundColor: UIColor?
	var tintColor: UIColor?
	private(set) var sheetContentHeight: CGFloat = 130
	private var isModal = true
	
	convenience init(title: String?, message: String?) {
		self.init(nibName: nil, bundle: nil)
		sheetTitle = title
		sheetMessage = message
		actions = Array()
		
		self.modalPresentationStyle = .Custom
		self.transitioningDelegate = self
	}
	
	func addAction(action: UIAlertAction) {
		actions.append(action)
	}
	
	func actionButton(index: Int) -> UIButton? {
		guard index < actions.count else { return nil }
		if buttons == nil {
			if self.view != nil {} // make sure the view is loaded
		}
		return buttons[index]
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		dimmingView = UIView(frame: self.view.bounds)
		dimmingView.backgroundColor = UIColor(white: 0, alpha: 0.2)
		dimmingView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		self.view.addSubview(dimmingView)
		
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(Sheet.cancel))
		dimmingView.addGestureRecognizer(tapRecognizer)
		
		contentView = UIView(frame: CGRect(x: 0, y: CGRectGetHeight(self.view.bounds) - sheetContentHeight, width: CGRectGetWidth(self.view.bounds), height: sheetContentHeight))
		contentView.backgroundColor = backgroundColor ?? UIColor.whiteColor()
		contentView.autoresizingMask = [.FlexibleWidth, .FlexibleTopMargin]
		self.view.addSubview(contentView)
		
		let buttonBackgroundImage = UIImage(named: "btn-alert-action")!
		let normalStateColor = tintColor ?? UIColor.blackColor()
		let highlightedStateColor = normalStateColor.colorWithAlphaComponent(0.6)
		let buttonNormalStateBackgroundImage = buttonBackgroundImage.imageWithTintColor(normalStateColor).resizableImageWithCapInsets(buttonBackgroundImage.capInsets)
		let buttonHighlightedStateBackgroundImage = buttonBackgroundImage.imageWithTintColor(highlightedStateColor).resizableImageWithCapInsets(buttonBackgroundImage.capInsets)
		
		buttons = Array()
		var buttonCenter = CGPoint(x: CGRectGetMidX(contentView.bounds), y: CGRectGetHeight(contentView.bounds) - 15 - 0.5 * buttonBackgroundImage.size.height)
		for action in actions {
			let button = SheetButton(type: .Custom)
			button.addTarget(self, action: #selector(Sheet.handleButtonAction(_:)), forControlEvents: .TouchUpInside)
			button.action = action
			button.titleLabel!.font = UIFont.systemFontOfSize(18)
			button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
			
			button.setTitleColor(normalStateColor, forState: .Normal)
			button.setBackgroundImage(buttonNormalStateBackgroundImage, forState: .Normal)
			button.setTitleColor(highlightedStateColor, forState: .Highlighted)
			button.setBackgroundImage(buttonHighlightedStateBackgroundImage, forState: .Highlighted)
			button.tintColor = normalStateColor
			
			button.setTitle(action.title, forState: .Normal)
			button.sizeToFit()
			if CGRectGetWidth(button.frame) < 200 {
				button.frame.size.width = 200
			}
			button.center = buttonCenter
			button.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin, .FlexibleTopMargin]
			contentView.addSubview(button)
			
			buttonCenter.y = buttonCenter.y - (buttonBackgroundImage.size.height + 8)
			buttons.append(button)
		}
		
		if sheetTitle != nil {
			titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: CGRectGetWidth(contentView.bounds) - 2 * 20, height: CGRectGetMinY(contentView.subviews.last!.frame)))
			titleLabel!.font = UIFont.systemFontOfSize(14)
			titleLabel!.numberOfLines = 0
			titleLabel!.textAlignment = .Center
			titleLabel!.textColor = tintColor ?? UIColor.blackColor()
			titleLabel!.text = sheetTitle!
			titleLabel!.autoresizingMask = [.FlexibleWidth, .FlexibleBottomMargin]
			contentView.addSubview(titleLabel!)
		}
	}
	
	override func willMoveToParentViewController(parent: UIViewController?) {
		super.willMoveToParentViewController(parent)
		dimmingView.hidden = true
		isModal = false
	}
	
	internal func handleButtonAction(sender: SheetButton) {
		// This stuff is evil
		typealias ActionHandlerBlock = @convention(block) (UIAlertAction) -> Void
		var handler: ActionHandlerBlock?
		if let handlerBlock = sender.action.valueForKey("handler") {
			handler = unsafeBitCast(handlerBlock, ActionHandlerBlock.self)
		}
		
		if isModal {
			self.presentingViewController!.dismissViewControllerAnimated(true) { () -> Void in
				handler?(sender.action)
			}
		} else {
			handler?(sender.action)
		}
	}
	
	internal func cancel() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	override func prefersStatusBarHidden() -> Bool {
		var presenter = self.presentingViewController!
		if presenter is UINavigationController {
			presenter = (presenter as! UINavigationController).topViewController ?? presenter
		}
		return presenter.prefersStatusBarHidden()
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		var presenter = self.presentingViewController!
		if presenter is UINavigationController {
			presenter = (presenter as! UINavigationController).topViewController ?? presenter
		}
		return presenter.preferredStatusBarStyle()
	}
}


// MARK: - UIViewControllerTransitioningDelegate


extension Sheet {
	func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return self
	}
	
	func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
		return self
	}
}


// MARK: - UIViewControllerAnimatedTransitioning


extension Sheet {
	func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
		return 0.4
	}
	
	func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
		let containerView = transitionContext.containerView()
		let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)
		let toView = transitionContext.viewForKey(UITransitionContextToViewKey)
		
		if toView == self.view {
			// Appearance
			toView!.frame = containerView!.bounds
			toView!.layoutIfNeeded()
			
			dimmingView.alpha = 0
			contentView.transform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(contentView.frame))
			containerView!.addSubview(toView!)
			
			UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
				self.dimmingView.alpha = 1
				self.contentView.transform = CGAffineTransformIdentity
				}, completion: { (finished) -> Void in
					transitionContext.completeTransition(finished)
			})
		} else {
			//Disappearance
			UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
				self.dimmingView.alpha = 0
				self.contentView.transform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(self.contentView.frame))
				}, completion: { (finished) -> Void in
					fromView!.removeFromSuperview()
					transitionContext.completeTransition(finished)
			})
		}
	}
}


// MARK: -


class SheetButton: UIButton {
	var action: UIAlertAction!
}
