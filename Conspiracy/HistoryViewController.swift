//
//  HistoryViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 29.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class HistoryViewController: UIViewController {
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var placeholderLabel: UILabel!
	private var historyItems: [CallHistoryItem]!
	private var pageIndex = 1
	private var isComplete = false
	private var isLoading = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("history.controller title", comment: "History")
		historyItems = Array()
		loadMoreItems()
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
	
	func loadMoreItems() {
		guard self.isComplete == false else { return }
		guard self.isLoading == false else { return }
		
		self.isLoading = true
		if self.historyItems.isEmpty {
			self.tableView.hidden = true
			self.activityIndicator.startAnimating()
		}
		
		API.sharedAPI.getCallHistory(pageIndex) { (calls, complete, error) -> Void in
			self.isLoading = false
			self.activityIndicator.stopAnimating()
			
			if error != nil {
				self.reportError(error)
				return
			}
			
			Log("Loaded \(calls!.count) items, complete: \(complete)")
			if self.historyItems.isEmpty {
				self.historyItems.appendContentsOf(calls!)
				self.tableView.reloadData()
				self.tableView.hidden = self.historyItems.isEmpty
				self.placeholderLabel.hidden = !self.historyItems.isEmpty
			} else {
				var insertedIndexPaths = [NSIndexPath]()
				for idx in 0..<calls!.count {
					insertedIndexPaths.append(NSIndexPath(forRow: self.historyItems.count + idx, inSection: 0))
				}
				self.historyItems.appendContentsOf(calls!)
				self.tableView.insertRowsAtIndexPaths(insertedIndexPaths, withRowAnimation: .Automatic)
			}
			
			self.pageIndex = self.pageIndex + 1
			self.isComplete = complete
		}
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		let vc = segue.destinationViewController as! CallDetailsViewController
		vc.callHistoryItem = historyItems[tableView.indexPathForSelectedRow!.row]
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if let selectedIndexPath = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedIndexPath, animated: animated)
		}
	}
}


// MARK: - UITableViewDataSource


extension HistoryViewController: UITableViewDataSource {
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return historyItems.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let item = historyItems[indexPath.row]
		
		let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! HistoryItemTableCell
		cell.configure(item)
		return cell
	}
}


// MARK: - UITableViewDelegate


extension HistoryViewController: UITableViewDelegate {
	func scrollViewDidScroll(scrollView: UIScrollView) {
		let preloadDistance = 0.5 * CGRectGetHeight(scrollView.bounds)
		if scrollView.contentOffset.y + CGRectGetHeight(scrollView.bounds) + preloadDistance > scrollView.contentSize.height {
			loadMoreItems()
		}
	}
}
