//
//  RadarView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 30.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class RadarView: UIView {
	private var backgroundView: UIImageView!
	private var scannerView: UIView!
	private var avatars: [UIImageView]!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		avatars = Array()
		
		let radarDiameter = min(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds))
		let radarSize = CGSize(width: radarDiameter, height: radarDiameter)
		
		UIGraphicsBeginImageContextWithOptions(radarSize, false, 0)
		UIColor.brandGreenColor().setStroke()
		UIColor.whiteColor().setFill()
		
		func makeCircle(circleDiameter: CGFloat) -> UIBezierPath {
			let path = UIBezierPath(ovalInRect: CGRect(x: 0.5 * (radarDiameter - circleDiameter), y: 0.5 * (radarDiameter - circleDiameter), width: circleDiameter, height: circleDiameter))
			path.lineWidth = 2.0
			return path
		}
		
		makeCircle(16.0).fill()
		var diameter = CGFloat(32.0)
		repeat {
			makeCircle(diameter).stroke()
			diameter = diameter + 16.0
		} while diameter < radarDiameter
		
		backgroundView = UIImageView(image: UIGraphicsGetImageFromCurrentImageContext())
		UIGraphicsEndImageContext()
		
		backgroundView.contentMode = .Center
		self.addSubview(backgroundView)
		
		scannerView = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 2.0, height: 0.5 * radarDiameter)))
		scannerView.backgroundColor = UIColor.whiteColor()
		scannerView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
		self.addSubview(scannerView)
		
		self.layoutIfNeeded()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		backgroundView.frame = self.bounds
		scannerView.center = backgroundView.center
	}
	
	func startAnimating() {
		let animating = !(scannerView.layer.animationKeys() ?? []).isEmpty
		guard animating == false else { return }
		
		self.scannerView.animateRotationWithDuration(4.0, clockwise: true)
	}
	
	func stopAnimating() {
		scannerView.layer.removeAllAnimations()
	}
	
	func addAnimatedAvatar(avatarURL: NSURL) {
		let avatarSize = CGSize(width: 25, height: 25)
		UIImage.loadImageAsynchronously(avatarURL) { (image) -> Void in
			if let unwrappedImage = image {
				let originalImageSize = unwrappedImage.size
				let scale = max(avatarSize.width / originalImageSize.width, avatarSize.height / originalImageSize.height)
				
				var drawingRect = CGRectZero
				drawingRect.size = CGSizeMake(originalImageSize.width * scale, originalImageSize.height * scale)
				drawingRect.origin = CGPointMake(0.5 * (avatarSize.width - drawingRect.size.width), 0.5 * (avatarSize.height - drawingRect.size.height))
				
				UIGraphicsBeginImageContextWithOptions(avatarSize, false, 0)
				let clippingPath = UIBezierPath(ovalInRect: CGRect(origin: CGPointZero, size: avatarSize))
				clippingPath.addClip()
				unwrappedImage.drawInRect(drawingRect)
				let avatarImage = UIGraphicsGetImageFromCurrentImageContext()
				UIGraphicsEndImageContext()
				
				dispatch_async(dispatch_get_main_queue()) {
					self.addAnimatedAvatar(avatarImage)
				}
			}
		}
	}
	
	private func addAnimatedAvatar(avatarImage: UIImage) {
		let avatarView = UIImageView(image: avatarImage)
		avatars.append(avatarView)
		self.insertSubview(avatarView, belowSubview: scannerView)
		addOrbitingAnimation(avatarView)
		
		avatarView.alpha = 0
		UIView.animateWithDuration(0.25) { 
			avatarView.alpha = 1
		}
	}
	
	private func addOrbitingAnimation(view: UIView) {
		let radarDiameter = min(CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds))
		let orbitWidth = randomFloat(0.55 * radarDiameter, max: 0.9 * radarDiameter)
		let orbitSize = CGSize(width: orbitWidth, height: randomFloat(0.2 * radarDiameter, max: orbitWidth))
		let path = UIBezierPath(ovalInRect: CGRect(origin: CGPoint(x: 0.0, y: 0.5 * (radarDiameter - orbitSize.height)), size: orbitSize))
		
		var rotationTransform = CGAffineTransformMakeTranslation(0.5 * radarDiameter, 0.5 * radarDiameter)
		rotationTransform = CGAffineTransformRotate(rotationTransform, randomFloat(0, max: CGFloat(2 * M_PI)))
		rotationTransform = CGAffineTransformTranslate(rotationTransform, -0.5 * radarDiameter, -0.5 * radarDiameter)
		path.applyTransform(rotationTransform)
		
//		let orbitLayer = CAShapeLayer()
//		orbitLayer.path = path.CGPath;
//		orbitLayer.lineWidth = 1
//		orbitLayer.strokeColor = UIColor.redColor().CGColor
//		orbitLayer.fillColor = UIColor.clearColor().CGColor
//		self.layer.addSublayer(orbitLayer)
		
		let pathAnimation = CAKeyframeAnimation(keyPath: "position")
		pathAnimation.calculationMode = kCAAnimationPaced
		pathAnimation.removedOnCompletion = false
		pathAnimation.repeatCount = MAXFLOAT
		pathAnimation.path = path.bezierPathByReversingPath().CGPath
		pathAnimation.duration = CFTimeInterval(randomFloat(12, max: 18))
		view.layer.addAnimation(pathAnimation, forKey: "orbit")
	}
	
	private func randomFloat(min: CGFloat, max: CGFloat) -> CGFloat {
		assert(max > min)
		return min + CGFloat(arc4random_uniform(UInt32(max - min)))
	}
}
