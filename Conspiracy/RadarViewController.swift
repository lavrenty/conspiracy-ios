//
//  RadarViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import AVFoundation


let RadarDidConnectNotification = "RadarDidConnectNotification"
let RadarDidDisconnectNotification = "RadarDidDisconnectNotification"


class RadarViewController: UIViewController {
	private var cameraPreviewLayer: AVCaptureVideoPreviewLayer!
	@IBOutlet weak var radarView: RadarView!
	@IBOutlet weak var searchingLabel: UILabel!
	@IBOutlet weak var cardContainerView: UIView!
	var card: UserCard!
	var answerCallback: ((call: Call, session: Session) -> Void)!
	var testMode = false
	private var webSocket: APIWebSocket!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		searchingLabel.text = NSLocalizedString("radar.looking for matches", comment: "Radar")
		
		let cardViewNib = UINib(nibName: "CardView-user", bundle: nil)
		let cardView = cardViewNib.instantiateWithOwner(nil, options: nil).first as! CardView
		cardView.card = card
		cardView.frame = cardContainerView.bounds
		cardView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		cardContainerView.addSubview(cardView)
		
		NSNotificationCenter.defaultCenter().postNotificationName(RadarDidConnectNotification, object: nil)
		
		var camera: AVCaptureDevice?
		let allCameras = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
		for cameraDevice in allCameras {
			if cameraDevice.position == .Front {
				camera = (cameraDevice as! AVCaptureDevice)
				break
			}
		}
		if camera == nil {
			camera = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
		}
		
		do {
			let cameraInput = try AVCaptureDeviceInput(device: camera)
			
			let session = AVCaptureSession()
			session.beginConfiguration()
			session.addInput(cameraInput)
			session.commitConfiguration()
			
			cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
			cameraPreviewLayer.frame = self.view.bounds
			self.view.layer.insertSublayer(cameraPreviewLayer, atIndex: 0)
		} catch {
			Log("*** AVCaptureDeviceInput() failed: \(error)")
		}
	}
	
	func openWebSocket() {
		guard webSocket == nil else { return }
		guard testMode == false else { return }
		
		webSocket = APIWebSocket(APIEndpoint: "/card/\(card.identifier)/radar")
		
		webSocket.addRoute("update") { (message) -> Void in
			Log("\(message)")
//			let price = Money(cents: message["rate"]! as! Int, currency: nil)
//			self.priceLabel.text = NSString(format: NSLocalizedString("card.price.%@ per minute", comment: "Card"), price!.formattedValue) as String
		}
		
		webSocket.addRoute("view") { (message) -> Void in
			Log("\(message)")
			if let avatar = message["picture"] as? String {
				if let avatarURL = NSURL(string: avatar) {
					self.radarView.addAnimatedAvatar(avatarURL)
				}
			}
		}
		
		webSocket.addRoute("call") { (message) -> Void in
			let call = API.sharedAPI.parseCallObject(message["call"]! as! [String: AnyObject], json: message)
			call.callee = User.currentUser
			call.card = self.card
			let sessionInfo = message["session"]! as! [String: String]
			let session = Session(identifier: sessionInfo["sid"]!, token: sessionInfo["token"]!)
			self.answerCallback(call: call, session: session)
		}
		
		webSocket.addRoute("failed") { (message) -> Void in
			let alert = UIAlertController(title: "Couldn't find a match",
				message: message.description,
				preferredStyle: .Alert
			)
			alert.addAction(UIAlertAction(title: NSLocalizedString("button.ok", comment: "Buttons"), style: .Default, handler: { (UIAlertAction) -> Void in
				self.close()
			}))
			self.presentViewController(alert, animated: true, completion: nil)
		}
		
		webSocket.onErrorRoute = { (error) -> Void in
			self.reportError(error as NSError, actions: [UIAlertAction(title: NSLocalizedString("button.ok", comment: "Buttons"), style: .Default, handler: { (UIAlertAction) -> Void in
				self.close()
			})])
		}
		
		webSocket.onCloseRoute = {
			Log("Web socket is closed")
			if self.webSocket != nil {
				Log("Reconnecting...")
				self.webSocket.open()
			}
		}
	}
	
	func closeWebSocket() {
		if webSocket != nil {
			webSocket.close()
			webSocket = nil
		}
	}
	
	@IBAction func close() {
		self.navigationController?.popViewControllerAnimated(true)
		NSNotificationCenter.defaultCenter().postNotificationName(RadarDidDisconnectNotification, object: nil)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		openWebSocket()
		
		if cameraPreviewLayer != nil {
			cameraPreviewLayer.session.startRunning()
		}
		radarView.startAnimating()
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		closeWebSocket()
		
		if cameraPreviewLayer != nil {
			cameraPreviewLayer.session.stopRunning()
		}
	}
	
	override func viewWillLayoutSubviews() {
		if cameraPreviewLayer != nil {
			cameraPreviewLayer.frame = self.view.bounds
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
}
