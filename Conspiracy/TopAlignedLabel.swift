//
//  TopAlignedLabel.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 28.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class TopAlignedLabel: UILabel {
	override func drawTextInRect(rect: CGRect) {
		var textRect = rect
		textRect.origin.y = 0
		if self.text != nil && !self.text!.isEmpty {
			var textSize = CGSizeZero
			if self.lineBreakMode == .ByTruncatingHead || self.lineBreakMode == .ByTruncatingMiddle || self.lineBreakMode == .ByTruncatingTail {
				// With any of these line break modes boundingRectWithSize() assumes a single line layout
				let text = self.attributedText!.mutableCopy() as! NSMutableAttributedString
				let style = text.attribute(NSParagraphStyleAttributeName, atIndex: 0, effectiveRange: nil)?.mutableCopy() as! NSMutableParagraphStyle
				style.lineBreakMode = .ByCharWrapping
				text.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: text.length))
				textSize = text.boundingRectWithSize(CGSize(width: CGRectGetWidth(rect), height: CGFloat.max), options: [.UsesLineFragmentOrigin], context: nil).size
			} else {
				textSize = self.attributedText!.boundingRectWithSize(CGSize(width: CGRectGetWidth(rect), height: CGFloat.max), options: [.UsesLineFragmentOrigin], context: nil).size
			}
			textRect.size.height = min(ceil(textSize.height), CGRectGetHeight(self.bounds))
		}
		super.drawTextInRect(textRect)
	}
}
