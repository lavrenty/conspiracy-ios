//
//  UIImageExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


extension UIImage {
	private static var token: dispatch_once_t = 0
	private static var queue = NSOperationQueue()
	
	public class func loadImageAsynchronously(url: NSURL?, completion: ((UIImage?) -> Void)) -> NSURLSessionDataTask? {
		// Set up an operation queue and a buggy(!) NSURLCache
		dispatch_once(&token) {
			queue.maxConcurrentOperationCount = 1
			queue.name = "io.conspiracy.image"
			
			// The docs for NSURLSessionDataDelegate method -URLSession:dataTask:willCacheResponse:completionHandler:
			// specifically state that NSURLProtocol may refuse to cache large responses:
			// "if you provide a disk cache, the response must be no larger than about 5% of the disk cache size".
			// 50 MB on disk, 256 KB in RAM
			let cache = NSURLCache(memoryCapacity: 256000, diskCapacity: 50000000, diskPath: nil)
			NSURLCache.setSharedURLCache(cache)
		}
		
		// Sanity checks
		guard let imageURL = url else {
			completion(nil)
			return nil
		}
		
		// File URLs
		if imageURL.fileURL {
			// UIImage(contentsOfFile:) is very fast because UIImage is lazily loaded, but is none the less blocking
			queue.addOperationWithBlock { () -> Void in
				let image = UIImage(contentsOfFile: imageURL.path!)
				completion(image)
			}
			return nil
		}
		
		let request = NSURLRequest(URL: imageURL, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 30)
		let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request)
		if cachedResponse != nil {
			queue.addOperationWithBlock { () -> Void in
				let image = UIImage(data: cachedResponse!.data)
				completion(image)
			}
			return nil
		}
		
		let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) -> Void in
			var image: UIImage?
			if data != nil {
				image = UIImage(data: data!)
			}
			#if DEBUG
				if image == nil && response != nil && (response as! NSHTTPURLResponse).statusCode < 500 {
					if error != nil {
						Log("*** Could not load image at \(imageURL): \(error!)")
					} else {
						Log("*** Not an image at \(imageURL): \(response!)")
					}
				}
			#endif
			completion(image)
		}
		task.resume()
		return task
	}
	
	func imageWithTintColor(tintColor: UIColor) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
		self.drawAtPoint(CGPointZero)
		tintColor.setFill()
		UIRectFillUsingBlendMode(CGRect(origin: CGPointZero, size: self.size), .SourceIn)
		let resultImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return resultImage
	}
}
