//
//  APIServerPicker.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 10.08.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


struct APIServerOption {
	var title: String
	var value: String
}


class APIServerPicker: UITableViewController {
	private var options: [APIServerOption]!
	private var defaultOption: Int = 0
	private var selectedOption: Int = 0
	
	convenience init() {
		self.init(style: .Grouped)
		self.title = "API Server"
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(APIServerPicker.cancel))
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Save, target: self, action: #selector(APIServerPicker.save))
		
		options = [
			APIServerOption(title: "Production", value: "production"),
			APIServerOption(title: "Staging", value: "staging"),
			APIServerOption(title: "Localhost", value: "localhost")
		]
		if let currentDefaultValue = NSUserDefaults.standardUserDefaults().stringForKey("api_server") {
			let optionIndex = options.indexOf({ (option) -> Bool in
				option.value == currentDefaultValue
			})
			if optionIndex != nil {
				defaultOption = optionIndex!
			}
		}
		selectedOption = defaultOption
		self.navigationItem.rightBarButtonItem?.enabled = false
	}
	
	override func viewDidLoad() {
		self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
	}
	
	internal func cancel() {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
	}
	
	internal func save() {
		if selectedOption != defaultOption {
			NSUserDefaults.standardUserDefaults().setObject(options[selectedOption].value, forKey: "api_server")
			exit(0)
		}
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
	}
}


// MARK: - UITableViewDataSource


extension APIServerPicker {
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return options.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
		cell.textLabel?.text = options[indexPath.row].title
		cell.accessoryType = indexPath.row == selectedOption ? .Checkmark : .None
		return cell
	}
}


// MARK: - UITableViewDelegate


extension APIServerPicker {
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if indexPath.row != selectedOption {
			tableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedOption, inSection: 0))?.accessoryType = .None
			tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
			selectedOption = indexPath.row
		}
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		self.navigationItem.rightBarButtonItem?.enabled = selectedOption != defaultOption
	}
}
