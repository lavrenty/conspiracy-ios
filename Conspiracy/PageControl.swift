//
//  PageControl.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 18.04.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


@IBDesignable
class PageControl : UIControl {
	@IBInspectable var pageIndicatorTintColor: UIColor?
	@IBInspectable var currentPageIndicatorTintColor: UIColor?
	
	var numberOfPages: Int = 0 {
		didSet {
			if numberOfPages != oldValue {
				repeat {
					if numberOfPages < self.subviews.count {
						self.subviews.last?.removeFromSuperview()
					} else {
						let dotView = UIImageView(image: PageControl.dotImage)
						self.addSubview(dotView)
					}
				} while numberOfPages != self.subviews.count
				
				self.updateDots()
				self.invalidateIntrinsicContentSize()
				self.setNeedsLayout()
			}
		}
	}
	
	var currentPage: Int = 0 {
		didSet {
			self.updateDots()
		}
	}
	
	// MARK: - Private
	
	private func updateDots() {
		for (index, dotView) in self.subviews.enumerate() {
			if index == currentPage {
				dotView.tintColor = currentPageIndicatorTintColor ?? defaultCurrentPageIndicatorTintColor
			} else {
				dotView.tintColor = pageIndicatorTintColor ?? defaultPageIndicatorTintColor
			}
		}
	}
	
	private var defaultPageIndicatorTintColor: UIColor {
		get {
			return UIColor(white: 1.0, alpha: 0.2)
		}
	}
	
	private var defaultCurrentPageIndicatorTintColor: UIColor {
		get {
			return UIColor.whiteColor()
		}
	}
	
	private static func createDotImage() -> UIImage {
		let dotSize = CGSize(width: 7, height: 7)
		UIGraphicsBeginImageContextWithOptions(dotSize, false, 0)
		UIBezierPath(ovalInRect: CGRect(origin: CGPointZero, size: dotSize)).fill()
		let dotImage = UIGraphicsGetImageFromCurrentImageContext().imageWithRenderingMode(.AlwaysTemplate)
		UIGraphicsEndImageContext()
		return dotImage
	}
	
	private static var dotImage: UIImage = PageControl.createDotImage()
	
	// MARK: - Layout
	
	private var dotSpacing: CGFloat = 9
	
	override func intrinsicContentSize() -> CGSize {
		let dotSize = PageControl.dotImage.size
		return CGSize(width: numberOfPages == 0 ? 0 : dotSize.width * CGFloat(numberOfPages) + dotSpacing * CGFloat(numberOfPages - 1), height: dotSize.height)
	}
	
	override func sizeThatFits(size: CGSize) -> CGSize {
		return self.intrinsicContentSize()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let dotSize = PageControl.dotImage.size
		let intrinsicContentSize = self.intrinsicContentSize()
		var dotOrigin = CGPoint(x: 0.5 * (CGRectGetWidth(self.bounds) - intrinsicContentSize.width), y: 0.5 * (CGRectGetHeight(self.bounds) - intrinsicContentSize.height))
		for dotView in self.subviews {
			dotView.frame.origin = dotOrigin
			dotOrigin.x = dotOrigin.x + dotSize.width + dotSpacing
		}
	}
	
	override func prepareForInterfaceBuilder() {
		numberOfPages = 5
		currentPage = 0
	}
	
	// MARK: - Events
	
	override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		return true
	}
	
	override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		return true
	}
	
	override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
		guard let theTouch = touch else { return }
		let location = theTouch.locationInView(self)
		if CGRectContainsPoint(CGRectInset(self.bounds, -50, -50), location) {
			if location.x < CGRectGetMidX(self.bounds) {
				if currentPage > 0 {
					currentPage = currentPage - 1
					self.sendActionsForControlEvents(.ValueChanged)
				}
			} else {
				if currentPage + 1 < numberOfPages {
					currentPage = currentPage + 1
					self.sendActionsForControlEvents(.ValueChanged)
				}
			}
		}
	}
	
	override func cancelTrackingWithEvent(event: UIEvent?) {
	}
}
