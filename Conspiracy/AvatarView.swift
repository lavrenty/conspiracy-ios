//
//  AvatarView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class AvatarView: UIImageView {
	var user: User? = nil {
		didSet {
			if oldValue != user {
				updateImage()
			}
		}
	}
	
	private var imageTask: NSURLSessionDataTask?
	private func updateImage() {
		self.image = self.placeholderImage
		
		imageTask?.cancel()
		imageTask = nil
		guard let unwrappedUser = user else { return }
		guard let imageURL = unwrappedUser.imageURL else { return }
		
		imageTask = UIImage.loadImageAsynchronously(imageURL) { (image) -> Void in
			if let unwrappedImage = image {
				let diameter = min(self.bounds.size.width, self.bounds.size.height)
				let desiredImageSize = CGSizeMake(diameter, diameter)
				let originalImageSize = unwrappedImage.size
				let scale = max(desiredImageSize.width / originalImageSize.width, desiredImageSize.height / originalImageSize.height)
				
				var drawingRect = CGRectZero
				drawingRect.size = CGSizeMake(originalImageSize.width * scale, originalImageSize.height * scale)
				drawingRect.origin = CGPointMake(0.5 * (desiredImageSize.width - drawingRect.size.width), 0.5 * (desiredImageSize.height - drawingRect.size.height))
				
				UIGraphicsBeginImageContextWithOptions(desiredImageSize, false, 0)
				let clippingPath = UIBezierPath(ovalInRect: CGRect(origin: CGPointZero, size: desiredImageSize))
				clippingPath.addClip()
				unwrappedImage.drawInRect(drawingRect)
				let avatarImage = UIGraphicsGetImageFromCurrentImageContext()
				UIGraphicsEndImageContext()
				
				dispatch_async(dispatch_get_main_queue()) {
					self.image = avatarImage
				}
			}
		}
	}
	
	private var placeholderImage: UIImage {
		get {
			let size = self.bounds.size
			UIGraphicsBeginImageContextWithOptions(size, false, 0)
			UIColor(white: 244.0/255.0, alpha: 1.0).setFill()
			UIBezierPath(ovalInRect: CGRect(origin: CGPointZero, size: size)).fill()
			let image = UIGraphicsGetImageFromCurrentImageContext()
			UIGraphicsEndImageContext()
			
			return image
		}
	}
}
