//
//  Spinner.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 8.04.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


@IBDesignable
class Spinner: UIView {
	private var imageView: UIImageView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		UIGraphicsBeginImageContextWithOptions(CGSize(width: 26, height: 26), false, 0)
		let path = UIBezierPath(arcCenter: CGPoint(x: 13, y: 13), radius: 11, startAngle: 0, endAngle: CGFloat(2 * M_PI * 0.9), clockwise: true)
		path.lineWidth = 2
		UIColor.whiteColor().setStroke()
		path.stroke()
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		imageView = UIImageView(image: image)
		imageView.contentMode = .Center
		self.backgroundColor = UIColor.clearColor()
		self.addSubview(imageView)
	}
	
	override func intrinsicContentSize() -> CGSize {
		return imageView.image!.size
	}
	
	func startAnimating() {
		let animating = !(imageView.layer.animationKeys() ?? []).isEmpty
		guard animating == false else { return }
		
		self.imageView.animateRotationWithDuration(2.0, clockwise: true)
	}
	
	func stopAnimating() {
		imageView.layer.removeAllAnimations()
	}
}
