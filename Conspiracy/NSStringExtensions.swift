//
//  NSStringExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 18.04.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation


extension NSString {
	func attributedTagString(attributes: [String : AnyObject], tagAttributes: [String : AnyObject]) -> NSAttributedString {
		let attributedText = NSMutableAttributedString()
		var searchRange = NSRange(location: 0, length: self.length)
		
		repeat {
			// Find the opening tag and append the remaining text if not found
			let openTagRange = self.rangeOfString("<!", options: [], range: searchRange)
			if openTagRange.location == NSNotFound {
				attributedText.appendAttributedString(NSAttributedString(string: self.substringWithRange(searchRange), attributes: attributes))
				break
			}
			
			// Append the text between tags
			if openTagRange.location > searchRange.location {
				let textRange = NSRange(location: searchRange.location, length: openTagRange.location - searchRange.location)
				attributedText.appendAttributedString(NSAttributedString(string: self.substringWithRange(textRange), attributes: attributes))
			}
			
			// Find the closing tag and append the tagged text
			let closeTagRange = self.rangeOfString("!>", options: [], range: NSRange(location: NSMaxRange(openTagRange), length: NSMaxRange(searchRange) - NSMaxRange(openTagRange)))
			assert(closeTagRange.location != NSNotFound)
			let taggedSubstringRange = NSRange(location: NSMaxRange(openTagRange), length: closeTagRange.location - NSMaxRange(openTagRange))
			attributedText.appendAttributedString(NSAttributedString(string: self.substringWithRange(taggedSubstringRange), attributes: tagAttributes))
			
			// Advance the search range
			searchRange = NSRange(location: NSMaxRange(closeTagRange), length: self.length - NSMaxRange(closeTagRange))
		} while searchRange.length > 0
		
		return attributedText
	}
}
