//
//  HistoryItemTableCell.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 12.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class HistoryItemTableCell: UITableViewCell {
	@IBOutlet weak var callerAvatarView: AvatarView!
	@IBOutlet weak var calleeAvatarView: AvatarView!
	@IBOutlet weak var subjectLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var questionTextLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var topicsView: TopicsView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		topicsView.lineHeight = 18
		topicsView.font = UIFont.systemFontOfSize(10, weight: UIFontWeightMedium)
		topicsView.allowsEditing = false
		topicsView.layoutMode = .SingleLineByTruncatingTail
		
		let bgView = UIView(frame: self.bounds)
		bgView.backgroundColor = UIColor(white: 240.0/255.0, alpha: 1.0)
		self.selectedBackgroundView = bgView
	}
	
	func configure(historyItem: CallHistoryItem) {
		callerAvatarView.user = historyItem.caller
		calleeAvatarView.user = historyItem.callee
		
		let subject = NSMutableAttributedString()
		if historyItem.caller == User.currentUser {
			let whoAnsweredText = NSAttributedString(string: NSLocalizedString("history.you answered", comment: "History").stringByAppendingString("\n"), attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
			let usernameText = NSAttributedString(string: historyItem.callee.fullName, attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor.blackColor()])
			subject.appendAttributedString(whoAnsweredText)
			subject.appendAttributedString(usernameText)
		} else {
			let usernameText = NSAttributedString(string: historyItem.caller.fullName, attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor.blackColor()])
			let whoAnsweredText = NSAttributedString(string: "\n".stringByAppendingString(NSLocalizedString("history.peer answered", comment: "History")), attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
			subject.appendAttributedString(usernameText)
			subject.appendAttributedString(whoAnsweredText)
		}
		subjectLabel.attributedText = subject
		
		questionTextLabel.text = historyItem.text
		
		let dateFormatter = NSDateFormatter()
		dateFormatter.dateStyle = .ShortStyle
		dateFormatter.timeStyle = .NoStyle
		dateFormatter.doesRelativeDateFormatting = true
		dateLabel.text = dateFormatter.stringFromDate(historyItem.date)
		
		if historyItem.rate.doubleValue > 0 {
			if historyItem.caller == User.currentUser {
				priceLabel.text = historyItem.totalCost.formattedValue
				priceLabel.textColor = UIColor.brandGreenColor()
			} else {
				priceLabel.text = historyItem.totalCost.formattedValue
				priceLabel.textColor = UIColor.blackColor()
			}
		} else {
			priceLabel.text = NSLocalizedString("history.free call", comment: "History")
			priceLabel.textColor = UIColor.blackColor()
		}
		topicsView.topics = historyItem.topics
	}
	
	// Fix topicView flickering when animating deselection
	// (UITableViewCell stubbornly sets all labels' background color to clear color)
	
	override func setHighlighted(highlighted: Bool, animated: Bool) {
		let topicViewBackgroundColor = topicsView.bubbleBackgroundColor
		super.setHighlighted(highlighted, animated: animated)
		for tv in topicsView.subviews {
			tv.backgroundColor = topicViewBackgroundColor
		}
	}
	
	override func setSelected(selected: Bool, animated: Bool) {
		let topicViewBackgroundColor = topicsView.bubbleBackgroundColor
		super.setSelected(selected, animated: animated)
		for tv in topicsView.subviews {
			tv.backgroundColor = topicViewBackgroundColor
		}
	}
}
