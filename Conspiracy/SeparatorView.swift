//
//  SeparatorView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 29.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class SeparatorView: UIView {
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		self.backgroundColor = UIColor(white: 237.0/255.0, alpha: 1.0)
		self.userInteractionEnabled = false
		self.translatesAutoresizingMaskIntoConstraints = false
		
		let isVertical = CGRectGetWidth(self.bounds) < CGRectGetHeight(self.bounds)
		let lengthAxis: UILayoutConstraintAxis = isVertical ? .Vertical : .Horizontal
		let widthAxis: UILayoutConstraintAxis = isVertical ? .Horizontal : .Vertical
		
		self.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: widthAxis)
		self.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: widthAxis)
		self.setContentHuggingPriority(UILayoutPriorityDefaultLow, forAxis: lengthAxis)
		self.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, forAxis: lengthAxis)
	}
	
	override func intrinsicContentSize() -> CGSize {
		let size = self.bounds.size
		let isVertical = size.width < size.height
		return CGSize(width: isVertical ? 0.5 : size.width, height: isVertical ? size.height : 0.5)
	}
}
