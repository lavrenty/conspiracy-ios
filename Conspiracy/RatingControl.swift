//
//  RatingControl.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 17.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


@IBDesignable
class RatingControl: UIControl {
	private var starSpacing: CGFloat = 20
	@IBInspectable var smallStyle: Bool = false {
		didSet {
			updateImages()
		}
	}
	
	private var stars: [UIImageView]!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setupInternals()
	}
	
	private func setupInternals() {
		stars = []
		for _ in 1...5 {
			let starView = UIImageView(image: nil)
			stars.append(starView)
			self.addSubview(starView)
		}
		
		updateImages()
	}
	
	private func updateImages() {
		starSpacing = smallStyle ? 7 : 20
		let hollowStarImage = UIImage(named: smallStyle ? "rating-star-hollow-small" : "rating-star-hollow", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
		let filledStarImage = UIImage(named: smallStyle ? "rating-star-filled-small" : "rating-star-filled", inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
		for starView in stars {
			starView.image = hollowStarImage
			starView.highlightedImage = filledStarImage
			starView.sizeToFit()
		}
		self.invalidateIntrinsicContentSize()
		self.setNeedsLayout()
	}
	
	var rating: Int = 0 {
		didSet {
			for i in 0...4 {
				stars[i].highlighted = i < rating
			}
		}
	}
	
	override func tintColorDidChange() {
		for imageView in stars {
			imageView.tintColor = self.tintColor
		}
	}
	
	override func intrinsicContentSize() -> CGSize {
		let starSize = stars[0].image!.size
		return CGSizeMake(starSize.width * 5 + CGFloat(starSpacing) * 4, starSize.height)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let starSize = stars[0].image!.size
		var offset: CGFloat = 0.0
		for i in 0...4 {
			var frame = stars[i].frame
			frame.origin = CGPointMake(offset, 0)
			stars[i].frame = frame
			offset = offset + starSize.width + starSpacing
		}
	}
	
	private func setRating(touch: UITouch) {
		let newRating = CGFloat(5.0) * touch.locationInView(self).x / CGRectGetWidth(self.bounds)
		let starCount = Int(round(max(min(newRating, CGFloat(5.0)), CGFloat(0.0))))
		
		if rating != starCount {
			self.rating = starCount
			self.sendActionsForControlEvents(.ValueChanged)
		}
	}
	
	override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		self.setRating(touch)
		return true
	}
	
	override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		self.setRating(touch)
		return true
	}
	
	override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
		if touch != nil {
			self.setRating(touch!)
		}
	}
	
	override func cancelTrackingWithEvent(event: UIEvent?) {
		
	}
}
