//
//  NSDataExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 26.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation


extension NSData {
	var hexStringRepresentation: String {
		get {
			let numberOfBytes = self.length
			let string = NSMutableString(capacity: numberOfBytes)
			var bytePointer = UnsafePointer<UInt8>(self.bytes)
			for _ in 0..<numberOfBytes {
				string.appendFormat("%02x", bytePointer.memory)
				bytePointer = bytePointer + 1
			}
			return string as String
		}
	}
}
