//
//  AuthenticationController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import FBSDKLoginKit


class AuthenticationController: UIViewController {
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var tosButton: UIButton!
	private var completion: ((authorized: Bool) -> Void)!
	private var state: String?
	
	convenience init(completion: (authorized: Bool) -> Void) {
		self.init(nibName: "Authentication", bundle: nil)
		self.completion = completion
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		loginButton.setTitle(NSLocalizedString("launch screen.button.login", comment: "Launch Screen"), forState: .Normal)
		
		let font = tosButton.titleLabel!.font
		let text = NSLocalizedString("launch screen.button.tos", comment: "Launch Screen")
		var normalAttrs = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.7)]
		var linkAttrs = [NSFontAttributeName: UIFont.systemFontOfSize(font.pointSize, weight: UIFontWeightSemibold), NSForegroundColorAttributeName: UIColor(white: 1.0, alpha: 0.7), NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue]
		tosButton.setAttributedTitle(text.attributedTagString(normalAttrs, tagAttributes: linkAttrs), forState: .Normal)
		normalAttrs[NSForegroundColorAttributeName] = UIColor.whiteColor()
		linkAttrs[NSForegroundColorAttributeName] = UIColor.whiteColor()
		tosButton.setAttributedTitle(text.attributedTagString(normalAttrs, tagAttributes: linkAttrs), forState: .Highlighted)
	}
	
	@IBAction func login(sender: AnyObject) {
		let theWindow = self.view.window!
		theWindow.userInteractionEnabled = false
		let loginManager = FBSDKLoginManager()
		loginManager.logInWithReadPermissions(["user_likes", "user_friends", "email", "user_birthday"], fromViewController: self) { (result: FBSDKLoginManagerLoginResult!, error: NSError!) in
			if let fbToken = result.token {
				API.sharedAPI.authorizeWithFacebook(fbToken.tokenString, completion: { (user, error) in
					theWindow.userInteractionEnabled = true
					if error == nil {
						self.completion(authorized: true)
					} else {
						self.reportError(error)
					}
				})
			} else {
				self.completion(authorized: false)
				theWindow.userInteractionEnabled = true
			}
		}
	}
	
	@IBAction func showTermsOfService(sender: AnyObject) {
		let url = NSURL(string: "https://notconspiracy.com/terms.html")!
		let webViewController = WebViewController.navigationController(url, title: nil, excludedDomains: nil, callback: nil)
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
}
