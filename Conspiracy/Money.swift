//
//  Money.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 4.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


struct Money: Equatable, Comparable {
	#if DEBUG
	static let allCurrencies = NSLocale.commonISOCurrencyCodes()
	#endif
	
	var doubleValue: Double {
		get {
			return Double(cents) * 0.01
		}
	}
	var currency: String
	var cents: Int
	
	init?(cents: Int, currency: String?) {
		if currency != nil {
			let wellFormedCurrency = currency!.uppercaseString
			#if DEBUG
			if !Money.allCurrencies.contains(wellFormedCurrency) { return nil }
			#endif
			self.currency = wellFormedCurrency
		} else {
			self.currency = "USD"
		}
		self.cents = cents
	}
	
	init?(doubleValue: Double, currency: String?) {
		self.init(cents: Int(round(doubleValue * 100.0)), currency: currency)
	}
	
	var description: String {
		return "\(doubleValue) \(currency)"
	}
	
	var formattedValue: String {
		let priceFormatter = NSNumberFormatter()
		priceFormatter.numberStyle = .CurrencyStyle
		priceFormatter.currencyCode = currency
		priceFormatter.locale = NSLocale(localeIdentifier: "en_US")
		if doubleValue == round(doubleValue) {
			priceFormatter.maximumFractionDigits = 0
		}
		return priceFormatter.stringFromNumber(doubleValue)!
	}
}

// MARK: - Equatable

func ==(lhs: Money, rhs: Money) -> Bool { return lhs.cents == rhs.cents && lhs.currency == rhs.currency }

// MARK: - Comparable

func <(lhs: Money, rhs: Money) -> Bool { return lhs.cents < rhs.cents && lhs.currency == rhs.currency }

func <=(lhs: Money, rhs: Money) -> Bool { return lhs.cents <= rhs.cents && lhs.currency == rhs.currency }

func >=(lhs: Money, rhs: Money) -> Bool { return lhs.cents >= rhs.cents && lhs.currency == rhs.currency }

func >(lhs: Money, rhs: Money) -> Bool { return lhs.cents > rhs.cents && lhs.currency == rhs.currency }

func *(lhs: Money, rhs: Int) -> Money { return Money(cents: lhs.cents * rhs, currency: lhs.currency)! }

func *(lhs: Int, rhs: Money) -> Money { return Money(cents: rhs.cents * lhs, currency: rhs.currency)! }

func *(lhs: Money, rhs: NSTimeInterval) -> Money { return Money(cents: Int(Double(lhs.cents) * rhs / 60.0), currency: lhs.currency)! }

func *(lhs: NSTimeInterval, rhs: Money) -> Money { return Money(cents: Int(Double(rhs.cents) * lhs / 60.0), currency: rhs.currency)! }

//func +(lhs: Money, rhs: Money) -> Money {
//	if lhs.currency == rhs.currency {
//		return Money(value: lhs.value + rhs.value, currency: lhs.currency)!
//	}
//	return lhs
//}
//
//func -(lhs: Money, rhs: Money) -> Money {
//	if lhs.currency == rhs.currency {
//		return Money(value: lhs.value - rhs.value, currency: lhs.currency)!
//	}
//	return lhs
//}
