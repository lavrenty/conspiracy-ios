//
//  UIViewControllerExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


extension UIViewController {
	func reportError(error: NSError?, actions: [UIAlertAction]?) {
		guard error != nil else { return }
		
		if let presentedViewController = self.presentedViewController {
			// Cannot present a UIAlertController while presenting another view controller
			if presentedViewController.isBeingDismissed() {
				// Try to piggyback presenting a UIAlertController on to the transition coordinator's completion
				if let transitionCoordinator = self.transitionCoordinator() {
					transitionCoordinator.animateAlongsideTransition(nil, completion: { [weak self] (context) in
						self?.reportError(error)
					})
				}
			}
			return
		}
		
		let alert = UIAlertController(title: error!.localizedDescription,
			message: error!.localizedFailureReason ?? "",
			preferredStyle: .Alert
		)
		if let alertActions = actions {
			for action in alertActions {
				alert.addAction(action)
			}
		} else {
			alert.addAction(UIAlertAction(title: NSLocalizedString("button.ok", comment: "Buttons"), style: .Default, handler: nil))
		}
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	func reportError(error: NSError?) {
		self.reportError(error, actions: nil)
	}
	
	// MARK: - Pseudo Action Sheets
	
	private func presentedConspiracySheet() -> Sheet? {
		for childVC in self.childViewControllers {
			if childVC is Sheet {
				return childVC as? Sheet
			}
		}
		return nil
	}
	
	func isPresentingConspiracySheet() -> Bool {
		return self.presentedConspiracySheet() != nil
	}
	
	func presentConspiracySheet(sheet: Sheet, animated: Bool) {
		guard !self.isPresentingConspiracySheet() else { return }
		
		sheet.view.frame = CGRect(x: 0, y: CGRectGetHeight(self.view.bounds) - sheet.sheetContentHeight, width: CGRectGetWidth(self.view.bounds), height: sheet.sheetContentHeight)
		self.addChildViewController(sheet)
		self.view.addSubview(sheet.view)
		sheet.didMoveToParentViewController(self)
		
		if animated {
			sheet.view.layoutIfNeeded()
			sheet.view.transform = CGAffineTransformMakeTranslation(0, sheet.sheetContentHeight)
			UIView.animateWithDuration(sheet.transitionDuration(nil), animations: { () -> Void in
				sheet.view.transform = CGAffineTransformIdentity
			})
		}
	}
	
	func dismissConspiracySheet(animated: Bool) {
		if let sheet = self.presentedConspiracySheet() {
			let completion = {
				sheet.willMoveToParentViewController(nil)
				sheet.view.removeFromSuperview()
				sheet.removeFromParentViewController()
			}
			if animated {
				UIView.animateWithDuration(sheet.transitionDuration(nil), animations: { () -> Void in
					sheet.view.transform = CGAffineTransformMakeTranslation(0, sheet.sheetContentHeight)
				}, completion: { (finished) -> Void in
					completion()
				})
			} else {
				completion()
			}
		}
	}
}
