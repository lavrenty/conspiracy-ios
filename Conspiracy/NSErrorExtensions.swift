//
//  NSErrorExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 10.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


extension NSError {
	convenience init(HTTPStatusCode: Int) {
		let reason = NSHTTPURLResponse.localizedStringForStatusCode(HTTPStatusCode)
		self.init(domain: "com.notconspiracy.error.HTTP", code: HTTPStatusCode, userInfo: [NSLocalizedDescriptionKey: reason, NSLocalizedFailureReasonErrorKey: "HTTP status code: \(HTTPStatusCode)"])
	}
	
	convenience init(APIErrorInfo: [String: AnyObject]) {
		let errorCode = APIErrorInfo["code"] != nil ? APIErrorInfo["code"]! as! Int : 0
		let errorType = APIErrorInfo["type"] != nil ? APIErrorInfo["type"]! as! String : "NO_TYPE"
		let errorMessage = APIErrorInfo["message"] != nil ? APIErrorInfo["message"]! as! String : "NO_MESSAGE"
		
		let localizableDescription = "error.\(errorCode).\(errorType).\(errorMessage)"
		let localizedDescription = NSLocalizedString(localizableDescription, comment: "Errors")
		if localizableDescription.lowercaseString == localizedDescription.lowercaseString {
			// No localization for this error
			Log("*** No localization for \"\(localizableDescription)\"")
			self.init(domain: kAPIErrorDomain, code: errorCode, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("error.api.general", comment: "Errors"), NSLocalizedFailureReasonErrorKey: "\(errorMessage)\n(\(errorType) code \(errorCode))"])
		} else {
			self.init(domain: kAPIErrorDomain, code: errorCode, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
		}
	}
}
