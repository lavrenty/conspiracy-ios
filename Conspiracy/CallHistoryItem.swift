//
//  CallHistoryItem.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation
import Stripe


class CallHistoryItem {
	var identifier: Int = 0
	var callIdentifier: Int = 0
	var caller: User!
	var callee: User!
	var rate: Money!
	var effectiveRate: Money?
	var duration: Int = 0
	var callCost: Money! // without fees
	var totalCost: Money! // including fees
	var text: String!
	var date: NSDate!
	var topics: [Topic]!
	var rating: Int = 0
	var details: CallHistoryItemDetails?
}


struct PaymentCardInfo {
	var cardBrand: STPCardBrand
	var last4Digits: String
	
	init(cardBrand: STPCardBrand, last4Digits: String) {
		self.cardBrand = cardBrand
		self.last4Digits = last4Digits
	}
	
	init(cardBrandString: String, last4: String) {
		let cardBrands = [
			"Visa": STPCardBrand.Visa,
			"American Express": STPCardBrand.Amex,
			"MasterCard": STPCardBrand.MasterCard,
			"Discover": STPCardBrand.Discover,
			"JCB": STPCardBrand.JCB,
			"Diners Club": STPCardBrand.DinersClub,
			"Unknown": STPCardBrand.Unknown
		]
		self.cardBrand = cardBrands[cardBrandString] ?? STPCardBrand.Unknown
		self.last4Digits = last4
	}
}


struct ClientChargeData {
	var baseClientFee: Money
	var paid: Bool
	var amount: Money?
	var paymentCard: PaymentCardInfo?
}


struct ConsultantChargeData {
	var serviceFee: Money
}


class CallHistoryItemDetails {
	var consultantCharge: ConsultantChargeData?
	var clientCharge: ClientChargeData?
}
