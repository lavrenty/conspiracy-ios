//
//  PaymentCardButton.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 12.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit
import Stripe


class PaymentCardButton: UIButton {
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		self.titleLabel?.font = UIFont.systemFontOfSize(16)
		self.cardInfo = nil
	}
	
	var cardInfo: PaymentCardInfo? {
		didSet {
			if cardInfo != nil {
				self.setTitleColor(UIColor(white: 38.0/255.0, alpha: 1.0), forState: .Normal)
				self.setTitle("•••• " + cardInfo!.last4Digits, forState: .Normal)
				
				let imageName: String
				switch (cardInfo!.cardBrand) {
					case .Amex:
						imageName = "stp_card_amex"
						break
					case .DinersClub:
						imageName = "stp_card_diners"
						break
					case .Discover:
						imageName = "stp_card_discover"
						break
					case .JCB:
						imageName = "stp_card_jcb"
						break
					case .MasterCard:
						imageName = "stp_card_mastercard"
						break
					case .Visa:
						imageName = "stp_card_visa"
						break
					default:
						imageName = "stp_card_placeholder"
						break
				}
				self.setImage(UIImage(named: imageName, inBundle: NSBundle(forClass: STPCard.self), compatibleWithTraitCollection: nil), forState: .Normal)
			} else {
				self.setTitleColor(UIColor(white: 155.0/255.0, alpha: 1.0), forState: .Normal)
				self.setTitle(NSLocalizedString("payment card.button.add payment card", comment: "Payment Card"), forState: .Normal)
				self.setImage(UIImage(named: "btn-add-payment-card"), forState: .Normal)
			}
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if let imageView = self.imageView {
			imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0.5 * (CGRectGetHeight(self.bounds) - CGRectGetHeight(imageView.frame))), size: imageView.frame.size)
		}
		if let titleLabel = self.titleLabel {
			titleLabel.frame = CGRect(x: self.imageView != nil ? CGRectGetMaxX(self.imageView!.frame) + 20 : 0, y: 0, width: CGRectGetWidth(titleLabel.frame), height: CGRectGetHeight(self.bounds))
		}
	}
}
