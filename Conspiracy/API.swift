//
//  API.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Stripe
import Crashlytics
import Amplitude_iOS


let kAPIErrorDomain = "com.notconspiracy.error.API"
enum APIErrorCode: Int {
	case ResponseIsNotJSON = -1
	case ResponseIsMalformed = -2
}


class API {
	static let sharedAPI = API()
	
	private(set) var APIBaseURLString: String
	private(set) var APIWebSocketURLString: String
	
	var userAgentString: String
	private var session: NSURLSession
	let eventQueue = dispatch_queue_create("API.WS", DISPATCH_QUEUE_SERIAL)
	private var failedRequests = [NSURLRequest]()
	private(set) var accessToken: String? {
		didSet {
			let defaults = NSUserDefaults.standardUserDefaults()
			var accessTokens = defaults.dictionaryForKey("access-token") ?? [String: AnyObject]()
			let apiHostName = NSURL(string: APIBaseURLString)!.host!
			if accessToken != nil {
				accessTokens[apiHostName] = accessToken
			} else {
				accessTokens.removeValueForKey(apiHostName)
				var myInfo = NSUserDefaults.standardUserDefaults().dictionaryForKey("me")!
				myInfo.removeValueForKey(apiHostName)
				defaults.setObject(myInfo, forKey: "me")
				User.currentUser = nil
			}
			defaults.setObject(accessTokens, forKey: "access-token")
		}
	}
	
	private(set) var APIServerPreference: String
	
	private init () {
		// API base URLs
		#if INTERNAL_TEST_BUILD
		APIServerPreference = NSUserDefaults.standardUserDefaults().stringForKey("api_server") ?? "production"
		#else
		APIServerPreference = "production"
		#endif
		switch APIServerPreference {
			case "production":
				APIBaseURLString = "https://api.notconspiracy.com"
				APIWebSocketURLString = "wss://api.notconspiracy.com"
				break
			case "staging":
				APIBaseURLString = "https://api-staging.notconspiracy.com"
				APIWebSocketURLString = "wss://api-staging.notconspiracy.com"
				break
			case "localhost":
				APIBaseURLString = "http://localhost:12312"
				APIWebSocketURLString = "ws://localhost:12312"
				break
			default:
				APIBaseURLString = "https://api.notconspiracy.com"
				APIWebSocketURLString = "wss://api.notconspiracy.com"
				break
		}
		Log("API base URL: \(APIBaseURLString), web socket: \(APIWebSocketURLString)")
		
		let apiHostName = NSURL(string: APIBaseURLString)!.host!
		accessToken = NSUserDefaults.standardUserDefaults().dictionaryForKey("access-token")?[apiHostName] as? String
		
		let appInfoDictionary = NSBundle.mainBundle().infoDictionary!
		let appName = appInfoDictionary[kCFBundleNameKey as String]!
		let appVersion = appInfoDictionary["CFBundleShortVersionString"]!
		let platform = UIDevice.currentDevice().platformType
		let systemVersion = UIDevice.currentDevice().systemVersion
		userAgentString = "\(platform) iOS/\(systemVersion) \(appName)/\(appVersion)"
		
		let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
		config.requestCachePolicy = .ReloadIgnoringLocalCacheData
		config.timeoutIntervalForRequest = 30
		config.timeoutIntervalForResource = 300
		config.networkServiceType = .NetworkServiceTypeDefault
		config.allowsCellularAccess = true
		config.connectionProxyDictionary = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? [NSObject : AnyObject]
		config.TLSMinimumSupportedProtocol = .TLSProtocol12
		config.HTTPShouldUsePipelining = true
		config.HTTPShouldSetCookies = false
		config.HTTPCookieAcceptPolicy = .Never
		config.HTTPAdditionalHeaders = ["User-Agent": userAgentString, "Accept": "application/json"]
		config.HTTPMaximumConnectionsPerHost = 1
		config.HTTPCookieStorage = nil
		config.URLCache = nil
		
		let sessionDelegateQueue = NSOperationQueue()
		sessionDelegateQueue.name = "API.HTTP"
		sessionDelegateQueue.maxConcurrentOperationCount = 1
		
		session = NSURLSession(configuration: config, delegate: nil, delegateQueue: sessionDelegateQueue)
		
		if var myInfo = NSUserDefaults.standardUserDefaults().dictionaryForKey("me") {
			if myInfo["id"] != nil {
				myInfo = [apiHostName: myInfo]
				NSUserDefaults.standardUserDefaults().setObject([apiHostName: myInfo], forKey: "me")
			}
			if let myInfoInCurrentDomain = myInfo[apiHostName] as? [String: AnyObject] {
				let currentUser = parseUserObject(myInfoInCurrentDomain, json: [:])
				User.currentUser = currentUser
				Crashlytics.sharedInstance().setUserIdentifier(String(currentUser.identifier))
				Crashlytics.sharedInstance().setUserName(currentUser.fullName)
			}
		}
		if User.currentUser == nil {
			accessToken = nil
		}
	}
	
	private func cacheCurrentUser(json: AnyObject) {
		if mockServerResponses == nil {
			let apiHostName = NSURL(string: APIBaseURLString)!.host!
			var myInfo = NSUserDefaults.standardUserDefaults().dictionaryForKey("me") ?? [:]
			myInfo[apiHostName] = json
			NSUserDefaults.standardUserDefaults().setObject(myInfo, forKey: "me")
			
			let currentUser = User.currentUser!
			Crashlytics.sharedInstance().setUserIdentifier(String(currentUser.identifier))
			Crashlytics.sharedInstance().setUserName(currentUser.fullName)
		}
	}
	
	var pushNotificationsDeviceToken: NSData? {
		get { return NSUserDefaults.standardUserDefaults().dataForKey("apns-token") }
		set { NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "apns-token") }
	}
	
	private var mockServerResponses: [String: [String: AnyObject]]?
	func addMockServerResponse(apiPath: String, response: [String: AnyObject]) {
		if mockServerResponses == nil {
			mockServerResponses = [:]
		}
		mockServerResponses!["/v1" + apiPath] = response
	}
	
	func resetMockServerResponses() {
		mockServerResponses = nil
	}
	
	private func makeJSONRequest(HTTPMethod: String, endpoint: String, authorized: Bool, parametersJSON: [String: AnyObject]?) -> NSMutableURLRequest {
		guard let url = NSURL(string: APIBaseURLString + "/v1" + endpoint) else {
			fatalError("Invalid endpoint: \(endpoint)")
		}
		
		let request = NSMutableURLRequest(URL: url)
		request.HTTPMethod = HTTPMethod
		if let json = parametersJSON {
			if HTTPMethod.uppercaseString != "GET" {
				do {
					let bodyData = try NSJSONSerialization.dataWithJSONObject(json, options: [])
					request.HTTPBody = bodyData
					request.setValue("application/json", forHTTPHeaderField: "Content-Type")
				} catch let error as NSError {
					fatalError("Invalid JSON: \(json.description)\n  (\(error))")
				}
			}
		}
		if authorized {
			precondition(accessToken != nil, "Request \(endpoint) is not authorized")
			request.setValue("BEARER \(accessToken!)", forHTTPHeaderField: "Authorization")
		}
//		Log("\(request.cURLCommand)")
		return request
	}
	
	private func performJSONRequest(request: NSURLRequest, completion: ((result: [String: AnyObject]?, response: NSHTTPURLResponse?, error: NSError?) -> Void)) {
		if mockServerResponses != nil {
			let apiPath = request.URL!.path
			assert(apiPath != nil)
			if let testResponse = mockServerResponses![apiPath!] {
				completion(result: testResponse, response: nil, error: nil)
			} else {
				Log("WARNING: no mock response for request \(apiPath!)")
				completion(result: nil, response: nil, error: NSError(HTTPStatusCode: 404))
			}
			return
		}
		
		let task = session.dataTaskWithRequest(request) {
			(data: NSData?, response: NSURLResponse?, requestError: NSError?) -> Void in
			
			var error = requestError
			var json: [String: AnyObject]?
			let httpResponse = response as? NSHTTPURLResponse
			if httpResponse != nil {
				if httpResponse!.statusCode == 200 {
					if error == nil {
//						if data != nil {
//							Log("\(request.cURLCommand) => \(NSString(data: data!, encoding: NSUTF8StringEncoding)!)")
//						}
						
						json = JSON(data) as? [String: AnyObject]
						if json != nil {
							let answer = json!
							if let success = answer["success"] {
								if success as! NSNumber == true {
									if answer["error"] != nil {
										error = NSError(domain: kAPIErrorDomain, code: APIErrorCode.ResponseIsMalformed.rawValue, userInfo: nil)
									}
								} else {
									if answer["error"] == nil {
										error = NSError(domain: kAPIErrorDomain, code: APIErrorCode.ResponseIsMalformed.rawValue, userInfo: nil)
									} else {
										error = NSError(APIErrorInfo: answer["error"]! as! [String : AnyObject])
									}
								}
							} else {
								error = NSError(domain: kAPIErrorDomain, code: APIErrorCode.ResponseIsMalformed.rawValue, userInfo: nil)
							}
						} else {
							error = NSError(domain: kAPIErrorDomain, code: APIErrorCode.ResponseIsNotJSON.rawValue, userInfo: nil)
						}
					}
				} else {
					if httpResponse!.statusCode == 401 {
						self.accessToken = nil
						error = NSError(HTTPStatusCode: httpResponse!.statusCode)
//						Log("*** \(request.URL!), status: \(statusCodeString), data:\n\(dataDescription)\n\(request.cURLCommand)")
					} else {
						error = NSError(HTTPStatusCode: httpResponse!.statusCode)
					}
					FunctionNotImplemented("Retrying is not implemented")
				}
				
				// Once we get a server response, retry all the requests which failed before
				if self.failedRequests.count > 0 {
					self.retryFailedRequests()
				}
			} else {
				Log("*** No response: \(request.URL!) \(error!)")
			}
			
			#if DEBUG
			if error != nil {
				let statusCodeString: String
				if httpResponse != nil {
					statusCodeString = "\(httpResponse!.statusCode)"
				} else {
					statusCodeString = "none"
				}
				let dataDescription: NSString
				if data != nil {
					dataDescription = NSString(data: data!, encoding: NSUTF8StringEncoding)!
				} else {
					dataDescription = "none"
				}
				Log("*** \(request.HTTPMethod!) \(request.URL!), status: \(statusCodeString), data:\n\(dataDescription)\n\(request.cURLCommand)")
			}
			#endif
			
			if error != nil {
				json = nil
				if error!.domain != NSURLErrorDomain {
					Crashlytics.sharedInstance().recordError(error!, withAdditionalUserInfo: ["Request": "\(request.HTTPMethod!) \(request.URL!.path!)"])
				}
			}
			
			dispatch_async(dispatch_get_main_queue()) {
				completion(result: json, response: httpResponse, error: error)
			}
		}
		task.resume()
	}
	
	private func retryFailedRequests() {
		for failedRequest in failedRequests {
			Log("Retrying request: \(failedRequest.URL!)")
			performJSONRequest(failedRequest, completion: { (result, response, error) in
				if error != nil && error!.domain == NSURLErrorDomain && error!.code != NSURLErrorCancelled {
					self.failedRequests.append(failedRequest)
				}
			})
		}
		failedRequests.removeAll()
	}
	
	// MARK: - Conspiracy API
	
	func authorizeWithFacebook(accessToken: String, completion: ((user: User?, error: NSError?) -> Void)) {
		var languageCodes = [String]()
		for localeIdentifier in NSLocale.preferredLanguages() {
			let languageCode: String
			if let dashRange = localeIdentifier.rangeOfString("-") { // "en-US"
				languageCode = localeIdentifier.substringToIndex(dashRange.startIndex)
			} else { // "en"
				languageCode = localeIdentifier
			}
			if !languageCodes.contains(languageCode) {
				languageCodes.append(languageCode)
			}
		}
		
		let json: [String: AnyObject] = [
			"token": accessToken,
			"locale": NSLocale.currentLocale().localeIdentifier,
			"languages": languageCodes.isEmpty ? ["en"] : languageCodes
		]
		
		let request = makeJSONRequest("POST", endpoint: "/auth/facebook/connect", authorized: false, parametersJSON: json)
		performJSONRequest(request) { (result, response, error) -> Void in
			var user: User?
			if result != nil {
				Log("\(#function) \(result!)")
				self.accessToken = result!["access_token"] as? String
				let userInfo = result!["user"] as! [String: AnyObject]
				user = self.parseUserObject(userInfo, json: result!)
				if user != nil {
					User.currentUser = user
					self.cacheCurrentUser(userInfo)
					Amplitude.instance().setUserId("\(user!.identifier)")
					Amplitude.instance().logEvent("Login")
				}
				if self.accessToken != nil && self.pushNotificationsDeviceToken != nil {
					Log("\(#function) Uploading APNS device token")
					self.registerDeviceForPushNotifications(nil)
				}
			}
			completion(user: user, error: error)
		}
	}
	
	func logOut() {
		Amplitude.instance().logEvent("Logout")
		FBSDKLoginManager().logOut()
		self.session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
			for task in dataTasks {
				task.cancel()
			}
		}
		self.failedRequests.removeAll()
		self.accessToken = nil
		UIApplication.sharedApplication().unregisterForRemoteNotifications()
	}
	
//	func disconnectFacebook(completion: ((error: NSError?) -> Void)) {
//		let request = makeJSONRequest("POST", endpoint: "/auth/facebook/disconnect", authorized: true, parametersJSON: nil)
//		performJSONRequest(request) { (result, response, error) -> Void in
//			if result != nil {
//				Log("\(#function) \(result!)")
//				if let currentUser = User.currentUser {
//					currentUser.hasConnectedFacebook = false
//				}
//			}
//			completion(error: error)
//		}
//	}
	
	func registerDeviceForPushNotifications(completion: ((error: NSError?) -> Void)?) {
		if self.pushNotificationsDeviceToken == nil {
			// Must request APNS device token first
			Log("\(#function) No APNS device token found. Registering...")
			(UIApplication.sharedApplication().delegate as! AppDelegate).registerForPushNotifications { (error) in
				if error != nil {
					completion?(error: error)
				} else {
					// Now that pushNotificationsDeviceToken is set by AppDelegate, retry
					assert(self.pushNotificationsDeviceToken != nil)
					self.registerDeviceForPushNotifications(completion)
				}
			}
		} else {
			let request = makeJSONRequest("POST", endpoint: "/notifications", authorized: true, parametersJSON: ["token": self.pushNotificationsDeviceToken!.hexStringRepresentation])
			performJSONRequest(request) { (result, response, error) -> Void in
				Log("\(#function) \(result ?? error)")
				completion?(error: error)
			}
		}
	}
	
	func unregisterDeviceForPushNotifications(completion: ((error: NSError?) -> Void)) {
		guard self.pushNotificationsDeviceToken != nil else {
			completion(error: nil)
			return
		}
		
		let request = makeJSONRequest("DELETE", endpoint: "/notifications", authorized: true, parametersJSON: ["token": self.pushNotificationsDeviceToken!.hexStringRepresentation])
		performJSONRequest(request) { (result, response, error) -> Void in
			Log("\(#function) \(result ?? error)")
			completion(error: error)
		}
	}
	
	func getUser(id: Int, completion: ((user: User?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/user/\(id)/profile", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var user: User?
			if result != nil {
//				Log("\(#function) \(result!)")
				user = self.parseJSON(result!, key: "user") as? User
			}
			completion(user: user, error: error)
		}
	}
	
	func getCurrentUser(completion: ((user: User?, error: NSError?) -> Void)?) {
		let currentUser = User.currentUser
		assert(currentUser != nil)
		let request = makeJSONRequest("GET", endpoint: "/user/\(currentUser!.identifier)/profile", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var user: User?
			if result != nil {
//				Log("\(#function) \(result!)")
				user = self.parseJSON(result!, key: "user") as? User
				if user != nil {
					User.currentUser = user
					if let userInfo = result!["user"] {
						self.cacheCurrentUser(userInfo)
					}
				}
			}
			completion?(user: user, error: error)
		}
	}
	
	func getSuggestedTopicsForUserProfile(completion: ((topics: [Topic]?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/topics/suggest", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var topics: [Topic]?
			if result != nil {
//				Log("\(#function) \(result!)")
				topics = self.parseJSON(result!, key: "topics") as? [Topic]
				precondition(topics != nil, "Failed to parse: \(result!)")
			}
			completion(topics: topics, error: error)
		}
	}
	
	func getSuggestedTopics(completion: ((recentTopics: [Topic]?, popularTopics: [Topic]?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/topics/recent", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var recentTopics: [Topic]?
			var popularTopics: [Topic]?
			if result != nil {
				if let allTopics = result!["topics"] as? [String: AnyObject] {
//					Log("\(#function) \((allTopics as NSDictionary).allKeys)")
					recentTopics = self.parseJSON(allTopics, key: "recent") as? [Topic]
					popularTopics = self.parseJSON(allTopics, key: "popular") as? [Topic]
					precondition(popularTopics != nil, "Failed to parse: \(result!)")
				}
			}
			completion(recentTopics: recentTopics, popularTopics: popularTopics, error: error)
		}
	}
	
	func getMatchingTopics(query: String, completion: ((topics: [Topic]?, error: NSError?) -> Void)) {
		let escapedQuery = query.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
		assert(escapedQuery != nil)
		let request = makeJSONRequest("GET", endpoint: "/topics/autocomplete?q=\(escapedQuery!)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var topics: [Topic]?
			if result != nil {
//				Log("\(#function) \(result!)")
				topics = self.parseJSON(result!, key: "topics") as? [Topic]
				precondition(topics != nil, "Failed to parse: \(result!)")
			}
			completion(topics: topics, error: error)
		}
	}
	
	func subscribeToTopic(topic: Topic, completion: ((topic: Topic?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("POST", endpoint: "/topics/subscribe", authorized: true, parametersJSON: ["name": topic.name])
		performJSONRequest(request) { (result, response, error) -> Void in
			var theTopic: Topic?
			if result != nil {
				Log("\(#function) \(result!)")
				theTopic = self.parseJSON(result!, key: "topic") as? Topic
			}
			completion(topic: theTopic, error: error)
		}
	}
	
	func unsubscribeFromTopic(topic: Topic, completion: ((error: NSError?) -> Void)) {
		let request = makeJSONRequest("DELETE", endpoint: "/topic/\(topic.identifier)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			Log("\(#function) \(result!)")
			completion(error: error)
		}
	}
	
	func updateUserLanguages(languageCodes: [String], completion: ((error: NSError?) -> Void)) {
		let currentUser = User.currentUser
		assert(currentUser != nil)
		let request = makeJSONRequest("POST", endpoint: "/user/\(currentUser!.identifier)/languages", authorized: true, parametersJSON: ["languages": languageCodes])
		performJSONRequest(request) { (result, response, error) -> Void in
			if result != nil {
				Log("\(#function) \(result!)")
				if error == nil {
					currentUser!.languages = result!["languages"] as! [String]
				}
			}
			completion(error: error)
		}
	}
	
	func addPaymentCard(stripeToken: String, completion: ((error: NSError?) -> Void)) {
		let request = makeJSONRequest("POST", endpoint: "/billing/card", authorized: true, parametersJSON: ["token": stripeToken])
		performJSONRequest(request) { (result, response, error) -> Void in
			if result != nil {
				Log("\(#function) \(result!)")
				let user = self.parseJSON(result!, key: "user") as? User
				User.currentUser = user
			}
			completion(error: error)
		}
	}
	
	func deletePaymentCard(completion: ((error: NSError?) -> Void)) {
		let request = makeJSONRequest("DELETE", endpoint: "/billing/card", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			if result != nil {
				Log("\(#function) \(result!)")
				let user = self.parseJSON(result!, key: "user") as? User
				User.currentUser = user
			}
			completion(error: error)
		}
	}
	
	func getEstimates(question: String, topics: [Topic], completion: ((estimates: [Estimate]?, error: NSError?) -> Void)) {
		var languageCode = "en"
		if let localeIdentifier = NSLocale.preferredLanguages().first {
			if let dashRange = localeIdentifier.rangeOfString("-") { // "en-US"
				languageCode = localeIdentifier.substringToIndex(dashRange.startIndex)
			} else { // "en"
				languageCode = localeIdentifier
			}
		}
		
		let request = makeJSONRequest("POST", endpoint: "/cards/rates", authorized: true, parametersJSON: ["topic_ids": topics.map({$0.identifier}), "language": languageCode, "text": question])
		performJSONRequest(request) { (result, response, error) -> Void in
			var estimates: [Estimate]?
			if result != nil {
//				Log("\(#function) \(result!)")
				if let estimateInfos = result!["rates"] as? [String: [String: AnyObject]] {
					estimates = [Estimate]()
					for (_, estimateInfo) in estimateInfos {
						let e = Estimate(rate: Money(cents: estimateInfo["rate"] as! Int, currency: estimateInfo["currency"] as? String)!, userCount: estimateInfo["users_count"] as! Int, score: estimateInfo["score"] as! Int)
						estimates!.append(e)
					}
					// Sort estimates from higher prices to lower
					estimates!.sortInPlace({ (e1, e2) -> Bool in
						return e1.rate > e2.rate
					})
				}
			}
			completion(estimates: estimates, error: error)
		}
	}
	
	func getCard(id: Int, completion: ((card: UserCard?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/card/\(id)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var card: UserCard?
			if result != nil {
				Log("\(#function) \(result!)")
				card = self.parseJSON(result!, key: "card") as? UserCard
			}
			completion(card: card, error: error)
		}
	}
	
	func createCard(question: String, topics: [Topic], price: Money, completion: ((cardIdentifier: Int?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("POST", endpoint: "/cards", authorized: true, parametersJSON: [
			"text": question,
			"rate": price.cents,
			"topic_ids": topics.map { $0.identifier }
			])
		performJSONRequest(request) { (result, response, error) -> Void in
			var cardID: Int?
			if result != nil {
				Log("\(#function) \(result!)")
				cardID = result!["card_id"] as? Int
			}
			completion(cardIdentifier: cardID, error: error)
		}
	}
	
	func answer(card: UserCard, completion: ((call: Call?, session: Session?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("POST", endpoint: "/card/\(card.identifier)/dial", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, requestError) -> Void in
			var error = requestError
			var call: Call?
			var session: Session?
			if result != nil {
				Log("\(#function) \(result!)")
				if let callInfo = result!["call"] {
					call = self.parseCallObject(callInfo as! [String: AnyObject], json: [:])
					call?.caller = User.currentUser
					call?.callee = card.user
					call?.card = card
					if let sessionInfo = result!["session"] {
						let sessionInfo2 = sessionInfo as! [String: String]
						session = Session(identifier: sessionInfo2["sid"]!, token: sessionInfo2["token"]!)
					}
				} else {
					if error == nil {
						error = NSError(domain: kAPIErrorDomain, code: 0, userInfo: nil)
					}
				}
			}
			completion(call: call, session: session, error: error)
		}
	}
	
	func rateCall(callID: Int, userRating: Int, connectionQuality: Int, completion: ((error: NSError?) -> Void)?) {
		let json = ["rating": userRating, "conn_quality": connectionQuality]
		let request = makeJSONRequest("POST", endpoint: "/call/\(callID)/rate", authorized: true, parametersJSON: json)
		performJSONRequest(request) { (result, response, error) -> Void in
			#if DEBUG
			if result != nil {
				Log("\(#function) \(result!)")
			}
			#endif
			if completion != nil {
				completion!(error: error)
			} else
			if error != nil && error!.domain == NSURLErrorDomain && error!.code != NSURLErrorCancelled {
				self.failedRequests.append(request)
			}
		}
	}
	
	func endorseUser(user: User, topic: Topic, completion: ((error: NSError?) -> Void)) {
		let request = makeJSONRequest("POST", endpoint: "/user/\(user.identifier)/endorse/\(topic.identifier)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			if result != nil {
				Log("\(#function) \(result!)")
			}
			completion(error: error)
		}
	}
	
	func getCallHistory(pageIndex: Int, completion: ((calls: [CallHistoryItem]?, complete: Bool, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/history?page=\(pageIndex)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var calls: [CallHistoryItem]?
			var isComplete = false
			if result != nil {
//				Log("\(#function) \(result!)")
				calls = self.parseJSON(result!, key: "items") as? [CallHistoryItem]
				precondition(calls != nil, "Failed to parse: \(result!)")
				calls?.sortInPlace({ (one, two) -> Bool in
					one.date.compare(two.date) == NSComparisonResult.OrderedDescending
				})
				isComplete = !(result!["page"]!["has_more"] as! Bool)
			}
			completion(calls: calls, complete: isComplete, error: error)
		}
	}
	
	func getCallDetails(callHistoryItem: CallHistoryItem, completion: ((callDetails: CallHistoryItemDetails?, error: NSError?) -> Void)) {
		let request = makeJSONRequest("GET", endpoint: "/history/\(callHistoryItem.identifier)", authorized: true, parametersJSON: nil)
		performJSONRequest(request) { (result, response, error) -> Void in
			var callDetails: CallHistoryItemDetails?
			if result != nil {
				Log("\(#function) \(result!)")
				if let details = result!["item"] as? [String: AnyObject] {
					let currency = details["currency"] as? String
					
					callDetails = CallHistoryItemDetails()
					if let serviceFee = details["conspiracy_fee"] as? Int {
						callDetails!.consultantCharge = ConsultantChargeData(serviceFee: Money(cents: serviceFee, currency: currency)!)
					}
					if let baseFeeInCents = details["base_fee"] as? Int {
						let baseClientFee = Money(cents: baseFeeInCents, currency: currency)!
						if let clientData = details["charge"] as? [String: AnyObject] {
							let paid = clientData["paid"] as! Bool
							let amount = Money(cents: clientData["amount"] as! Int, currency: clientData["currency"] as? String)!
							callDetails!.clientCharge = ClientChargeData(baseClientFee: baseClientFee, paid: paid, amount: amount, paymentCard: paid ? PaymentCardInfo(cardBrandString: clientData["card_brand"] as! String, last4: clientData["card_last_four"] as! String) : nil)
						} else {
							callDetails!.clientCharge = ClientChargeData(baseClientFee: baseClientFee, paid: false, amount: nil, paymentCard: nil)
						}
					}
				}
			}
			completion(callDetails: callDetails, error: error)
		}
	}
	
//	func getCardDetails(cardIdentifier: Int, completion: ((card: UserCard?, error: NSError?) -> Void)) {
//		let request = makeJSONRequest("GET", endpoint: "/card/\(cardIdentifier)", authorized: true, parametersJSON: nil)
//		performJSONRequest(request) { (result, response, error) -> Void in
//			var theCard: UserCard?
//			if result != nil {
//				Log("\(#function) \(result!)")
//				theCard = self.parseJSON(result!, key: "card") as? UserCard
//			}
//			completion(card: theCard, error: error)
//		}
//	}
	
	// MARK: -
	
	private func parseJSON(json: [String: AnyObject], key: String) -> AnyObject? {
		guard let objectInfo = json[key] else {
			return nil
		}
		
		if objectInfo is NSArray {
			return (objectInfo as! NSArray).map { parseAPIObject($0 as! [String: AnyObject], json: json)! }
		}
		
		return parseAPIObject(objectInfo as! [String: AnyObject], json: json)
	}
	
	private func parseAPIObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> AnyObject? {
		guard let type = objectJSON["type"] else {
			Log("*** Failed to parse \(json)\nObject type is unknown: \(objectJSON)")
			return nil
		}
		guard type is String else {
			Log("*** Failed to parse \(json)\nObject type is not a String: \(objectJSON)")
			return nil
		}
		switch type as! String {
			case "user":
				return parseUserObject(objectJSON, json: json)
			case "topic":
				return parseTopicObject(objectJSON, json: json)
			case "card":
				return parseCardObject(objectJSON, json: json)
			case "call":
				return parseCallObject(objectJSON, json: json)
			case "history_item":
				return parseHistoryItemObject(objectJSON, json: json)
			default:
				return nil
		}
	}
	
	func parseUserObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> User {
//		Log("\(objectJSON)")
		let object = User(identifier: objectJSON["id"] as! Int)
		
//		object.firstName = objectJSON["first_name"] as! String
//		object.middleName = objectJSON["middle_name"] as! String
//		object.lastName = objectJSON["last_name"] as! String
		object.fullName = objectJSON["name"] as? String
		if let genderString = objectJSON["gender"] as? String {
			let genders = ["male": Gender.Male, "female": Gender.Female]
			object.gender = genders[genderString]
		}
//		object.locale = objectJSON["locale"] as? String
		object.languages = objectJSON["languages"] as? [String]
		if let verified = objectJSON["is_verified"] as? Bool {
			object.isVerified = verified
		}
		if let notifications = objectJSON["notifications"] as? Bool {
			object.hasPushNotificationsEnabled = notifications
		}
		if let questionCount = objectJSON["questions_count"] as? Int {
			object.questionCount = questionCount
		}
		if let answerCount = objectJSON["answers_count"] as? Int {
			object.answerCount = answerCount
		}
		if let hasFacebook = objectJSON["has_facebook"] as? Bool {
			object.hasConnectedFacebook = hasFacebook
		}
		if let hasLinkedIn = objectJSON["has_linkedin"] as? Bool {
			object.hasConnectedLinkedIn = hasLinkedIn
		}
		if let facebookPermissions = objectJSON["fb_permissions"] as? String {
			object.facebookPermissions = facebookPermissions.componentsSeparatedByString(",")
		}
		if let avatarURI = objectJSON["picture"] as? String {
			object.imageURL = NSURL(string: avatarURI)
		}
		if let topicIDs = objectJSON["topic_ids"] {
			object.topics = parseRelatedObjects(topicIDs, json: json, lookupKey: "topics") as? [Topic]
		}
		var paymentCard: PaymentCardInfo?
		if let isBillable = objectJSON["is_billable"] as? Bool {
			if isBillable && objectJSON["card_brand"] != nil && objectJSON["card_last4"] != nil {
				paymentCard = PaymentCardInfo(cardBrandString: objectJSON["card_brand"] as! String, last4: objectJSON["card_last4"] as! String)
			}
		}
		object.paymentCard = paymentCard
		if let isPayable = objectJSON["is_payable"] as? Bool {
			object.isPayable = isPayable
		}
		if let unpaidInvoice = objectJSON["has_unpaid_invoice"] as? Bool {
			object.hasUnpaidInvoice = unpaidInvoice
		}
		if let balanceAmount = objectJSON["balance"] as? Int {
			object.balance = Money(cents: balanceAmount, currency: objectJSON["balance_currency"] as? String)!
		}
		return object
	}
	
	private func parseTopicObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> Topic {
		let object = Topic(identifier: objectJSON["id"] as! Int)
		
		object.name = objectJSON["name"] as! String
		if let callCount = objectJSON["calls_count"] as? Int {
			object.callCount = callCount
		}
		if let suggested = objectJSON["is_suggested"] as? Bool {
			object.isSuggested = suggested
		}
		return object
	}
	
	func parseCardObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> UserCard {
		let object = UserCard(identifier: objectJSON["id"] as! Int)
		
		object.text = objectJSON["text"] as! String
		object.language = objectJSON["language"] as! String
		object.price = Money(cents: objectJSON["rate"] as! Int, currency: nil)
		object.creationDate = NSDate(timeIntervalSince1970: objectJSON["created"] as! NSTimeInterval)
		object.user = parseRelatedObjects(objectJSON["user_id"]!, json: json, lookupKey: "users") as! User
		object.topics = parseRelatedObjects(objectJSON["topic_ids"]!, json: json, lookupKey: "topics") as! [Topic]
		object.live = objectJSON["live"] as! Bool
		object.numberOfViews = objectJSON["views"] as! Int
		return object
	}
	
	func parseSystemCardObject(objectJSON: [String: AnyObject]) -> Card? {
		let cardIdentifier = objectJSON["id"] as! Int
		
		let data = objectJSON["data"] as! [String: AnyObject]
		switch(objectJSON["type"] as! String) {
			case "endorsement":
				let card = SystemEndorsementCard(identifier: cardIdentifier)
				card.user = parseUserObject(data["user"] as! [String: AnyObject], json: [:])
				card.topic = parseTopicObject(data["topic"] as! [String: AnyObject], json: [:])
				return card
			
			case "topic":
				let card = TopicCard(identifier: cardIdentifier)
				card.topic = parseTopicObject(data["topic"] as! [String: AnyObject], json: [:])
				return card
			
			case "notifications":
				return PushNotificationsCard(identifier: cardIdentifier)
			
			default:
				Log("*** Unsupported system card type: \"\(objectJSON["type"] as! String)\"\n\(objectJSON)")
				break
		}
		return nil
	}
	
	func parseCallObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> Call {
		let object = Call(identifier: objectJSON["id"] as! Int)
		
		object.rate = Money(cents: (objectJSON["rate"] as? Int)!, currency: nil)
		if let caller = json["caller"] {
			object.caller = parseUserObject(caller as! [String: AnyObject], json: [:])
		}
		if let callee = json["callee"] {
			object.callee = parseUserObject(callee as! [String: AnyObject], json: [:])
		}
		return object
	}
	
	private func parseHistoryItemObject(objectJSON: [String: AnyObject], json: [String: AnyObject]) -> CallHistoryItem? {
		let object = CallHistoryItem()
		object.identifier = objectJSON["id"] as! Int
		object.callIdentifier = objectJSON["call_id"] as! Int
		object.caller = parseRelatedObjects(objectJSON["caller_id"]!, json: json, lookupKey: "users") as? User
		if object.caller == nil {
			object.caller = User.currentUser
		}
		object.callee = parseRelatedObjects(objectJSON["callee_id"]!, json: json, lookupKey: "users") as? User
		if object.callee == nil {
			object.callee = User.currentUser
		}
		let currency = objectJSON["currency"] as? String
		object.rate = Money(cents: (objectJSON["rate"] as? Int)!, currency: currency)
		if let effectiveRate = objectJSON["effective_rate"] as? Int {
			object.effectiveRate = Money(cents: effectiveRate, currency: currency)
		}
		object.duration = objectJSON["duration"] as! Int
		object.callCost = Money(cents: objectJSON["call_cost"] as! Int, currency: currency)!
		object.totalCost = Money(cents: objectJSON["cost"] as! Int, currency: currency)!
		object.text = objectJSON["text"] as! String
		object.date = NSDate(timeIntervalSince1970: objectJSON["started"] as! NSTimeInterval)
		object.topics = parseRelatedObjects(objectJSON["topic_ids"]!, json: json, lookupKey: "topics") as! [Topic]
		object.rating = objectJSON["rating"] as! Int
		
		return object
	}
	
	private func parseRelatedObjects(objectIDs: AnyObject, json: [String: AnyObject], lookupKey: String) -> AnyObject? {
		guard let lookupTable = json[lookupKey] else {
			if json.count != 0 {
				Log("*** Failed to parse \(json)\nNo included objects for key \"\(lookupKey)\"")
			}
			return nil
		}
		guard lookupTable is [String: AnyObject] else {
			Log("*** Failed to parse \(json)\nMalformed included objects for key \"\(lookupKey)\"")
			return nil
		}
		let theLookupTable = lookupTable as! [String: AnyObject]
		if objectIDs is Int {
			if let objectJSON = theLookupTable["\(objectIDs)"] {
				return parseAPIObject(objectJSON as! [String: AnyObject], json: json)
			} else {
				return nil
			}
		}
		if objectIDs is [Int] {
			let objIDs = objectIDs as! [Int]
			return objIDs.map { parseAPIObject(theLookupTable["\($0)"] as! [String: AnyObject], json: json)! }
		}
		Log("*** Failed to parse \(json)\nMalformed included object reference: \"\(objectIDs)\"")
		return nil
	}
}
