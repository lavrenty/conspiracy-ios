//
//  SpecialPresentationController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 3.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class SpecialSegue: UIStoryboardSegue {
	override init(identifier: String?, source: UIViewController, destination: UIViewController) {
		super.init(identifier: identifier, source: source, destination: SpecialPresentationController(viewController: destination))
	}
}


class SpecialPresentationController: UIViewController, UIGestureRecognizerDelegate {
	var contentViewController: UIViewController
	private var dimmingView: UIView!
	private var statusBarHeight: CGFloat = 0
	private weak var scrollView: UIScrollView?
	private var initialOffset: CGFloat?
	
	init(viewController: UIViewController) {
		contentViewController = viewController
		super.init(nibName: nil, bundle: nil)
		self.modalPresentationStyle = .Custom
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SpecialPresentationController.statusBarFrameWillChange(_:)), name: UIApplicationWillChangeStatusBarFrameNotification, object: nil)
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let statusBarFrame = UIApplication.sharedApplication().statusBarFrame
		statusBarHeight = min(CGRectGetWidth(statusBarFrame), CGRectGetHeight(statusBarFrame))
		
		dimmingView = UIView(frame: self.view.bounds)
		dimmingView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		dimmingView.backgroundColor = UIColor(white: 0, alpha: 0.8)
		dimmingView.alpha = 0
		self.view.insertSubview(dimmingView, atIndex: 0)
		
		contentViewController.view.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0))
		contentViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		contentViewController.view.layer.cornerRadius = 6
		contentViewController.view.clipsToBounds = true
		
		self.addChildViewController(contentViewController)
		self.view.addSubview(contentViewController.view)
		contentViewController.didMoveToParentViewController(self)
		
		let dismissGestureRecognizer = DirectionPanGestureRecognizer(direction: .Vertical, target: self, action: #selector(SpecialPresentationController.handleDismissGesture(_:)))
		dismissGestureRecognizer.delegate = self
		self.view.addGestureRecognizer(dismissGestureRecognizer)
	}
	
	override func prefersStatusBarHidden() -> Bool {
		let vc: UIViewController?
		if contentViewController is UINavigationController {
			vc = (contentViewController as! UINavigationController).topViewController
		} else {
			vc = contentViewController
		}
		return vc != nil ? vc!.prefersStatusBarHidden() : false
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return self.presentingViewController!.preferredStatusBarStyle()
	}
	
	func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
		var currentViewController = contentViewController
		if let navigationController = currentViewController as? UINavigationController {
			if navigationController.topViewController != nil {
				currentViewController = navigationController.topViewController!
			}
		}
		return !currentViewController.ignoresPullToDismissGesture()
	}
	
	func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		// The idea is to identify a UIScrollView.panGestureRecognizer and get the UIScrollView being scrolled if any
		if let theScrollView = otherGestureRecognizer.view as? UIScrollView {
			if theScrollView.panGestureRecognizer == otherGestureRecognizer {
				let bounces = theScrollView.bounces && theScrollView.alwaysBounceVertical
				if theScrollView.scrollEnabled && bounces {
					// Ignore UITableViewWrapperView. Is there a better way?
					if !(theScrollView.superview is UIScrollView) {
						scrollView = theScrollView
//						Log("\(theScrollView.dynamicType)")
						return true
					}
				}
			}
		}
		return false
	}
	
	internal func handleDismissGesture(gestureRecognizer: UIPanGestureRecognizer) {
		switch gestureRecognizer.state {
			case .Began:
				initialOffset = nil
				break
				
			case .Changed:
				if let theScrollView = scrollView {
					if theScrollView.contentOffset.y < 0 {
						// Set initialOffset to the pan gesture recognizer's translation at a point where bouncing starts
						if initialOffset == nil {
							initialOffset = gestureRecognizer.translationInView(self.view).y + theScrollView.contentOffset.y
						}
						// Prevent bouncing
						theScrollView.contentOffset.y = 0
					} else {
						// Once bounced, prevent scrolling until dragged the view to the top
						if initialOffset != nil {
							theScrollView.contentOffset.y = max(0, initialOffset! - gestureRecognizer.translationInView(self.view).y)
						}
					}
					// Once bounced, drag the view relative to the bouncing start translation
					if initialOffset != nil {
						fadePresentedViewController(gestureRecognizer.translationInView(self.view).y - initialOffset!)
					}
				} else {
					// Dragging a non-scroll view is simple
					fadePresentedViewController(gestureRecognizer.translationInView(self.view).y)
				}
				break
				
			case .Ended:
				let cancelled: Bool
				if scrollView != nil {
					if initialOffset != nil {
						cancelled = gestureRecognizer.velocityInView(self.view).y < 0 || gestureRecognizer.translationInView(self.view).y < initialOffset!
					} else {
						cancelled = true
					}
				} else {
					cancelled = gestureRecognizer.velocityInView(self.view).y < 0 || gestureRecognizer.translationInView(self.view).y <= 0
				}
				
				if cancelled {
					UIView.animateWithDuration(0.4, animations: { () -> Void in
						self.fadePresentedViewController(0)
						},
						completion: nil)
				} else {
					if let presenter = self.presentingViewController! as? UIViewControllerTransitioningDelegate {
						if let animator = presenter.animationControllerForDismissedController!(self) {
							UIView.animateWithDuration(animator.transitionDuration(nil), animations: { () -> Void in
								self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
								self.dimmingView.alpha = 0
							})
							return
						}
					}
					
					let duration = 0.4 * (1.0 - CGRectGetMinY(contentViewController.view.frame) / CGRectGetHeight(self.view.bounds))
					UIView.animateWithDuration(NSTimeInterval(duration), animations: { () -> Void in
						self.contentViewController.view.transform.ty = CGRectGetHeight(self.view.bounds) - self.statusBarHeight
						self.dimmingView.alpha = 0
						},
						completion: { (finished) -> Void in
							self.presentingViewController!.dismissViewControllerAnimated(false, completion: nil)
					})
				}
				
				scrollView = nil
				break
				
			case .Cancelled:
				UIView.animateWithDuration(0.4, animations: { () -> Void in
					self.fadePresentedViewController(0)
					},
					completion: nil)
				
				scrollView = nil
				break
				
			default:
				break
		}
	}
	
	private func fadePresentedViewController(panDistance: CGFloat) {
		let scalePanDistanceThreshold: CGFloat = 75
		let view = contentViewController.view
		if panDistance > 0 {
			var transform = CGAffineTransformMakeTranslation(0, panDistance)
			if panDistance > scalePanDistanceThreshold {
				let fraction = 1.0 - 0.5 * min((panDistance - scalePanDistanceThreshold) / (0.5 * CGRectGetHeight(UIScreen.mainScreen().bounds)), 1.0)
				transform = CGAffineTransformScale(transform, fraction, fraction)
			}
			view.transform = transform
			let fraction = min(panDistance / (0.5 * CGRectGetHeight(UIScreen.mainScreen().bounds)), 1.0)
			dimmingView.alpha = 1.0 - 0.5 * fraction
		} else {
			view.transform = CGAffineTransformIdentity
			dimmingView.alpha = 1
		}
	}
	
	internal func statusBarFrameWillChange(notification: NSNotification) {
		if let statusBarFrameValue = notification.userInfo?[UIApplicationStatusBarFrameUserInfoKey] as? NSValue {
			let inset = CGRectGetHeight(statusBarFrameValue.CGRectValue()) - 20
			UIView.animateWithDuration(0.35, animations: { () -> Void in
				self.contentViewController.view.frame = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsets(top: inset, left: 0, bottom: 0, right: 0))
				self.contentViewController.view.layoutIfNeeded()
			})
		}
	}
}


extension UIViewController {
	func ignoresPullToDismissGesture() -> Bool {
		return false
	}
}
