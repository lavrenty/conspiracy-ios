//
//  TestsViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 19.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


struct Test {
	var title: String
	var action: ((Void) -> Void)?
}


struct TestSection {
	var sectionTitle: String
	var controller: ((Void) -> UIViewController)
	var commonAction: ((Void) -> Void)?
	var tests = [Test]()
}


class TestsViewController: UITableViewController {
	private var testSections: [TestSection]!
	private var savedCurrentUser: User!
	private var testedViewController: UIViewController?
	private var call: Call?
	private var callHistoryItem: CallHistoryItem?
	
	convenience init() {
		self.init(style: .Grouped)
		self.title = "Tests"
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav-btn-close"), style: .Plain, target: self, action: #selector(TestsViewController.close))
		
		self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
		
		let fakeUser = User(identifier: 1000000000)
		savedCurrentUser = User.currentUser
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		testSections = [
			TestSection(sectionTitle: "Profile",
				controller: {
					return storyboard.instantiateViewControllerWithIdentifier("profile-vc")
				},
				commonAction: {
					API.sharedAPI.addMockServerResponse("/topics/suggest", response: ["topics": [
						[
							"type": "topic",
							"id": 49,
							"name": "Answer",
							"calls_count": 0,
							"is_suggested": true,
							"score": 50,
						],
						[
							"type": "topic",
							"id": 44,
							"name": "Books",
							"calls_count": 0,
							"is_suggested": true,
							"score": 20,
						],
						[
							"type": "topic",
							"id": 45,
							"name": "Startups",
							"calls_count": 0,
							"is_suggested": true,
							"score": -3,
						],
					]])
					User.currentUser = fakeUser
				},
				tests: [
					Test(title: "No problems", action: {
						API.sharedAPI.addMockServerResponse("/user/\(fakeUser.identifier)/profile", response: ["user": [
							"type": "user",
							"id": fakeUser.identifier,
							"first_name": "Даздраперма",
							"middle_name": "",
							"last_name": "Ногизадерищенко",
							"name": "Даздраперма Ногизадерищенко",
							"gender": "female",
							"has_facebook": true,
							"has_linkedin": false,
							"has_twitter": false,
							"has_mobile": true,
							"has_unpaid_invoice": false,
							"is_payable": true,
							"is_billable": true,
							"card_brand": "Visa",
							"card_last4": "6789",
							"is_verified": true,
							"languages": ["zh", "cr", "en", "es", "pt", "se"],
							"picture": "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352",
							"answers_count": 1000,
							"questions_count": 1000,
						]])
					}),
					
					Test(title: "Unverified user", action: {
						API.sharedAPI.addMockServerResponse("/user/\(fakeUser.identifier)/profile", response: ["user": [
							"type": "user",
							"id": fakeUser.identifier,
							"first_name": "Даздраперма",
							"middle_name": "",
							"last_name": "Ногизадерищенко",
							"name": "Даздраперма Ногизадерищенко",
							"gender": "female",
							"has_facebook": false,
							"has_linkedin": false,
							"has_twitter": false,
							"has_mobile": true,
							"has_unpaid_invoice": false,
							"is_payable": false,
							"is_billable": false,
							"is_verified": false,
							"languages": [],
							"picture": "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352",
							"answers_count": 0,
							"questions_count": 0,
						]])
					}),
					
					Test(title: "Has unpaid invoice", action: {
						API.sharedAPI.addMockServerResponse("/user/\(fakeUser.identifier)/profile", response: ["user": [
							"type": "user",
							"id": fakeUser.identifier,
							"first_name": "Даздраперма",
							"middle_name": "",
							"last_name": "Ногизадерищенко",
							"name": "Даздраперма Ногизадерищенко",
							"gender": "female",
							"has_facebook": true,
							"has_linkedin": true,
							"has_twitter": true,
							"has_mobile": true,
							"has_unpaid_invoice": true,
							"is_payable": true,
							"is_billable": true,
							"card_brand": "Visa",
							"card_last4": "6789",
							"is_verified": true,
							"languages": ["zh", "cr"],
							"picture": "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352",
							"answers_count": 10,
							"questions_count": 1,
						]])
					}),
					
				]
			),
			
			// Open cards
			
			TestSection(sectionTitle: "Open System Cards",
				controller: {
					return self.testedViewController!
				},
				commonAction: {
					self.testedViewController = storyboard.instantiateViewControllerWithIdentifier("opencard-vc")
					
					let user = User(identifier: -2)
					user.fullName = "Jennifer Maldonado-Christensen"
					user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
					user.isVerified = true
					user.isPayable = true
//					user.paymentCard = PaymentCardInfo(cardBrandString: "Visa", last4: "6789")
					User.currentUser = user
				},
				tests: [
					Test(title: "Push notifications", action: {
						(self.testedViewController as? OpenCardViewController)?.card = PushNotificationsCard(identifier: -1)
					}),
					
					Test(title: "Endorsement", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let topic = Topic(identifier: -1)
						topic.name = "QA (UI Testing)"
						
						let card = SystemEndorsementCard(identifier: -1)
						card.user = user
						card.topic = topic
						(self.testedViewController as? OpenCardViewController)?.card = card
					}),
					
					Test(title: "Topic", action: {
						let topic = Topic(identifier: -1)
						topic.name = "QA (UI Testing)"
						
						let card = TopicCard(identifier: -1)
						card.topic = topic
						(self.testedViewController as? OpenCardViewController)?.card = card
					}),
				]
			),
			
			TestSection(sectionTitle: "Open User Cards",
				controller: {
					let user = User(identifier: -1)
					user.fullName = "Даздраперма Ногизадерищенко"
					user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
					
					let topic1 = Topic(identifier: -1)
					topic1.name = "QA (UI Testing)"
					let topic2 = Topic(identifier: 49)
					topic2.name = "Answer"
					let topic3 = Topic(identifier: 44)
					topic3.name = "Books"
					let topic4 = Topic(identifier: -2)
					topic4.name = "Here comes a topic with a ridiculously long name that won't fit anywhere"
					
					let card = UserCard(identifier: -1)
					card.text = "This is a sample question text, sort of «Lorem ipsum». Can it contain hyperlinks? Should we filter user-entered stuff in any way?"
					card.language = "en"
					card.price = Money(cents: 100, currency: "GBP")
					card.creationDate = NSDate()
					card.user = user
					card.topics = [topic1, topic4, topic3, topic2]
					card.live = true
					
					let vc = storyboard.instantiateViewControllerWithIdentifier("opencard-vc") as! OpenCardViewController
					vc.card = card
					return vc
				},
				commonAction: {
					let currentUser = User(identifier: -2)
					currentUser.fullName = "Jennifer Maldonado-Christensen"
					currentUser.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
//					currentUser.paymentCard = PaymentCardInfo(cardBrandString: "Visa", last4: "6789")
					User.currentUser = currentUser
				},
				tests: [
					Test(title: "No problems", action: {
						User.currentUser?.isVerified = true
						User.currentUser?.isPayable = true
					}),
					
					Test(title: "Unverified user", action: {
						User.currentUser?.isVerified = false
						User.currentUser?.isPayable = true
					}),
					
					Test(title: "Payout not configured", action: {
						User.currentUser?.isVerified = true
						User.currentUser?.isPayable = false
					}),
				]
			),
			
			// Call UI
			
			TestSection(sectionTitle: "Radar",
				controller: {
					return self.testedViewController!
				},
				commonAction: {
					let vc = storyboard.instantiateViewControllerWithIdentifier("call-vc") as! VideoViewController
					vc.testMode = true
					self.testedViewController = vc
				},
				tests: [
					Test(title: "Paid question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample paid question text, blah-blah-blah"
						card.language = "en"
						card.price = Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
					}),
					
					Test(title: "Free question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample free question text, blah-blah-blah"
						card.language = "en"
						card.price = nil //Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
					}),
				]
			),
			
			TestSection(sectionTitle: "In-call UI",
				controller: {
					return self.testedViewController!
				},
				commonAction: {
					let vc = storyboard.instantiateViewControllerWithIdentifier("call-vc") as! VideoViewController
					vc.testMode = true
					vc.call = Call(identifier: -1)
					vc.session = Session(identifier: "", token: "")
					self.testedViewController = vc
				},
				tests: [
					Test(title: "Paid answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let userTopic1 = Topic(identifier: -100)
						userTopic1.name = "UI Testing"
						let userTopic2 = Topic(identifier: -101)
						userTopic2.name = "Breaking Things"
						user.topics = [userTopic1, userTopic2]
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample paid question text, blah-blah-blah"
						card.language = "en"
						card.price = Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
						(self.testedViewController as! VideoViewController).call.rate = card.price
						(self.testedViewController as! VideoViewController).call.caller = User.currentUser
						(self.testedViewController as! VideoViewController).call.callee = user
					}),
					
					Test(title: "Free answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let userTopic1 = Topic(identifier: -100)
						userTopic1.name = "UI Testing"
						let userTopic2 = Topic(identifier: -101)
						userTopic2.name = "Breaking Things"
						user.topics = [userTopic1, userTopic2]
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						let topic3 = Topic(identifier: -1)
						topic3.name = "Topic with a ridiculously long name"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample free question text, blah-blah-blah"
						card.language = "en"
						card.price = nil //Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2, topic3]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
						(self.testedViewController as! VideoViewController).call.rate = Money(cents: 0, currency: nil)
						(self.testedViewController as! VideoViewController).call.caller = User.currentUser
						(self.testedViewController as! VideoViewController).call.callee = user
					}),
					
					Test(title: "Paid question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let userTopic1 = Topic(identifier: -100)
						userTopic1.name = "UI Testing"
						let userTopic2 = Topic(identifier: -101)
						userTopic2.name = "Breaking Things"
						user.topics = [userTopic1, userTopic2]
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample paid question text, blah-blah-blah"
						card.language = "en"
						card.price = Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
						(self.testedViewController as! VideoViewController).call.rate = card.price
						(self.testedViewController as! VideoViewController).call.caller = user
						(self.testedViewController as! VideoViewController).call.callee = User.currentUser
					}),
					
					Test(title: "Free question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						let userTopic1 = Topic(identifier: -100)
						userTopic1.name = "UI Testing"
						let userTopic2 = Topic(identifier: -101)
						userTopic2.name = "Breaking Things"
						user.topics = [userTopic1, userTopic2]
						
						let topic1 = Topic(identifier: 49)
						topic1.name = "Answer"
						let topic2 = Topic(identifier: 44)
						topic2.name = "Books"
						let topic3 = Topic(identifier: -1)
						topic3.name = "Topic with a ridiculously long name"
						
						let card = UserCard(identifier: -1)
						card.text = "This is a sample free question text, blah-blah-blah"
						card.language = "en"
						card.price = nil //Money(cents: 100, currency: "USD")
						card.creationDate = NSDate()
						card.user = user
						card.topics = [topic1, topic2, topic3]
						card.live = true
						
						(self.testedViewController as! VideoViewController).card = card
						(self.testedViewController as! VideoViewController).call.rate = Money(cents: 0, currency: nil)
						(self.testedViewController as! VideoViewController).call.caller = user
						(self.testedViewController as! VideoViewController).call.callee = User.currentUser
					}),
				]
			),
			
			TestSection(sectionTitle: "Call Rating",
				controller: {
					let vc = storyboard.instantiateViewControllerWithIdentifier("rate-vc") as! RatingViewController
					vc.call = self.call
					return vc
				},
				commonAction: {
					if self.call == nil {
						self.call = Call(identifier: -1)
					}
					
					let user = User(identifier: -2)
					user.fullName = "Jennifer Maldonado-Christensen"
					user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
					User.currentUser = user
				},
				tests: [
					Test(title: "Paid answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.call!.rate = Money(cents: 125, currency: "USD")
						self.call!.duration = 659
						self.call!.totalCost = Money(cents: 125 * 10, currency: "USD")
						self.call!.callee = user
					}),
					
					Test(title: "Free answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.call!.rate = nil //Money(cents: 0, currency: "USD")
						self.call!.duration = 659
						self.call!.totalCost = nil //Money(cents: 0, currency: "USD")
						self.call!.callee = user
					}),
					
					Test(title: "Paid question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.call!.rate = Money(cents: 75, currency: "GBP")
						self.call!.duration = 659
						self.call!.totalCost = Money(cents: 75 * 10, currency: "GBP")
						self.call!.callee = nil
						self.call!.caller = user
					}),
					
					Test(title: "Free question", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.call!.rate = nil //Money(cents: 0, currency: "USD")
						self.call!.duration = 659
						self.call!.totalCost = Money(cents: 0, currency: "USD")
						self.call!.callee = nil
						self.call!.caller = user
					}),
				]
			),
			
			// Call History
			
			TestSection(sectionTitle: "Call History",
				controller: {
					let vc = storyboard.instantiateViewControllerWithIdentifier("call-details-vc") as! CallDetailsViewController
					vc.callHistoryItem = self.callHistoryItem
					return vc
				},
				commonAction: {
					if self.callHistoryItem == nil {
						self.callHistoryItem = CallHistoryItem()
						self.callHistoryItem!.date = NSDate(timeIntervalSinceNow: -24*60*60)
						self.callHistoryItem!.text = "This is a sample question text, blah-blah-blah."
						
						let topic1 = Topic(identifier: -1)
						topic1.name = "QA (UI Testing)"
						let topic2 = Topic(identifier: 49)
						topic2.name = "Answer"
						let topic3 = Topic(identifier: 44)
						topic3.name = "Books"
						self.callHistoryItem!.topics = [topic1, topic3, topic2]
					}
					
					let user = User(identifier: -2)
					user.fullName = "Jennifer Maldonado-Christensen"
					user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/ANIMALS-%26-NATURE/ANIMALS/sloth-face-800x535.jpg?m=1444508263")
					User.currentUser = user
				},
				tests: [
					Test(title: "Paid answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.callHistoryItem!.rate = Money(cents: 125, currency: "USD")
						self.callHistoryItem!.duration = 659
						self.callHistoryItem!.callCost = NSTimeInterval(self.callHistoryItem!.duration) * self.callHistoryItem!.rate
						self.callHistoryItem!.totalCost = Money(cents: self.callHistoryItem!.callCost.cents - 100, currency: "USD")
						self.callHistoryItem!.callee = user
						self.callHistoryItem!.caller = User.currentUser
						self.callHistoryItem!.rating = 1
						
						let callDetails = CallHistoryItemDetails()
						callDetails.consultantCharge = ConsultantChargeData(serviceFee: Money(cents: 100, currency: "USD")!)
						self.callHistoryItem!.details = callDetails
					}),
					
					Test(title: "Free answer", action: {
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.callHistoryItem!.rate = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.duration = 659
						self.callHistoryItem!.callCost = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.totalCost = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.callee = user
						self.callHistoryItem!.caller = User.currentUser
						self.callHistoryItem!.rating = 2
						
						let callDetails = CallHistoryItemDetails()
						callDetails.consultantCharge = nil
						callDetails.clientCharge = nil
						self.callHistoryItem!.details = callDetails
					}),
					
					Test(title: "Paid question (paid invoice)", action: {
						let callee = User(identifier: -2)
						callee.fullName = "John Doe"
						callee.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/ANIMALS-%26-NATURE/ANIMALS/sloth-face-800x535.jpg?m=1444508263")
						
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.callHistoryItem!.rate = Money(cents: 75, currency: "GBP")
						self.callHistoryItem!.duration = 659
						self.callHistoryItem!.callCost = NSTimeInterval(self.callHistoryItem!.duration) * self.callHistoryItem!.rate
						self.callHistoryItem!.totalCost = Money(cents: self.callHistoryItem!.callCost.cents + 100, currency: "GBP")
						self.callHistoryItem!.callee = User.currentUser
						self.callHistoryItem!.caller = user
						self.callHistoryItem!.rating = 3
						
						let callDetails = CallHistoryItemDetails()
						callDetails.clientCharge = ClientChargeData(baseClientFee: Money(cents: 100, currency: "GBP")!, paid: true, amount: self.callHistoryItem!.totalCost, paymentCard: PaymentCardInfo(cardBrandString: "Visa", last4: "6789"))
						self.callHistoryItem!.details = callDetails
					}),
					
					Test(title: "Paid question (unpaid invoice)", action: {
						let callee = User(identifier: -2)
						callee.fullName = "John Doe"
						callee.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/ANIMALS-%26-NATURE/ANIMALS/sloth-face-800x535.jpg?m=1444508263")
						
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.callHistoryItem!.rate = Money(cents: 75, currency: "GBP")
						self.callHistoryItem!.duration = 659
						self.callHistoryItem!.callCost = NSTimeInterval(self.callHistoryItem!.duration) * self.callHistoryItem!.rate
						self.callHistoryItem!.totalCost = Money(cents: self.callHistoryItem!.callCost.cents + 100, currency: "GBP")
						self.callHistoryItem!.callee = User.currentUser
						self.callHistoryItem!.caller = user
						self.callHistoryItem!.rating = 4
						
						let callDetails = CallHistoryItemDetails()
						callDetails.clientCharge = ClientChargeData(baseClientFee: Money(cents: 100, currency: "GBP")!, paid: false, amount: nil, paymentCard: nil)
						self.callHistoryItem!.details = callDetails
					}),
					
					Test(title: "Free question", action: {
						let callee = User(identifier: -2)
						callee.fullName = "John Doe"
						callee.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/ANIMALS-%26-NATURE/ANIMALS/sloth-face-800x535.jpg?m=1444508263")
						
						let user = User(identifier: -1)
						user.fullName = "Даздраперма Ногизадерищенко"
						user.imageURL = NSURL(string: "http://www.avatarys.com/var/albums/Cool-Avatars/Cartoons-Avatars/funny-avatar_by-avatarys_cartoon-avatar-by-avatarys.jpg?m=1432822352")
						
						self.callHistoryItem!.rate = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.duration = 659
						self.callHistoryItem!.callCost = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.totalCost = Money(cents: 0, currency: "USD")
						self.callHistoryItem!.callee = User.currentUser
						self.callHistoryItem!.caller = user
						self.callHistoryItem!.rating = 5
						
						let callDetails = CallHistoryItemDetails()
						callDetails.consultantCharge = nil
						callDetails.clientCharge = nil
						self.callHistoryItem!.details = callDetails
					}),
				]
			),
			
			TestSection(sectionTitle: "Onboarding",
				controller: {
					return self.testedViewController!
				},
				commonAction: {
					self.testedViewController = OnboardingViewController(completion: { (authorized) -> Void in })
				},
				tests: [
					Test(title: "Step 1", action: {
						(self.testedViewController as! OnboardingViewController).initialStep = 0
					}),
					
					Test(title: "Step 2", action: {
						(self.testedViewController as! OnboardingViewController).initialStep = 1
					}),
					
					Test(title: "Step 3", action: {
						(self.testedViewController as! OnboardingViewController).initialStep = 2
					}),
				]
			),
		]
	}
	
	internal func close() {
		API.sharedAPI.resetMockServerResponses()
		User.currentUser = savedCurrentUser
		testSections = nil
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
	}
}


// MARK: - UITableViewDataSource


extension TestsViewController {
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return testSections.count
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return testSections[section].sectionTitle
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return testSections[section].tests.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let test = testSections[indexPath.section].tests[indexPath.row]
		
		let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
		cell.textLabel?.text = test.title
		cell.accessoryType = .DisclosureIndicator
//		cell.textLabel?.textColor = test.action != nil ? UIColor.blackColor() : UIColor.grayColor()
		return cell
	}
}


// MARK: - UITableViewDelegate


extension TestsViewController {
//	override func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//		let test = testSections[indexPath.section].tests[indexPath.row]
//		return test.action != nil
//	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		let section = testSections[indexPath.section]
		let test = section.tests[indexPath.row]
		section.commonAction?()
		test.action?()
		
		let vc = TestController(viewController: section.controller())
		vc.title = test.title
		self.navigationController?.pushViewController(vc, animated: true)
	}
}


// MARK: - TestController


class TestController: UIViewController {
	private var testedViewController: UIViewController!
	
	convenience init(viewController: UIViewController) {
		self.init(nibName: nil, bundle: nil)
		testedViewController = viewController
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor.whiteColor()
		
		self.addChildViewController(testedViewController)
		testedViewController.view.frame = self.view.bounds
		testedViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		self.view.addSubview(testedViewController.view)
		testedViewController.didMoveToParentViewController(self)
		
		let overlayView = UIView(frame: self.view.bounds)
		overlayView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//		overlayView.backgroundColor = UIColor.magentaColor().colorWithAlphaComponent(0.2)
		self.view.addSubview(overlayView)
		
		let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(TestController.handleSwipe))
		swipeRecognizer.direction = .Right
		overlayView.addGestureRecognizer(swipeRecognizer)
		
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TestController.handleSwipe))
		overlayView.addGestureRecognizer(tapRecognizer)
	}
	
	internal func handleSwipe() {
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.navigationController?.setNavigationBarHidden(false, animated: true)
	}
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
}
