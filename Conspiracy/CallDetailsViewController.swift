//
//  CallDetailsViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 15.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class CallDetailsViewController: UIViewController {
	var callHistoryItem: CallHistoryItem!
	private var isFreeCall = true
	
	@IBOutlet weak var dateLabel: UILabel!
	
	@IBOutlet weak var callerAvatarView: AvatarView!
	@IBOutlet weak var calleeAvatarView: AvatarView!
	@IBOutlet weak var subjectLabel: UILabel!
	@IBOutlet weak var questionTextLabel: UILabel!
	@IBOutlet weak var rateLabel: UILabel!
	@IBOutlet weak var topicsView: TopicsView!
	
	@IBOutlet weak var ratingControlCaptionLabel: UILabel!
	@IBOutlet weak var ratingControl: RatingControl!
	@IBOutlet weak var rateUserButton: UIButton!
	
	@IBOutlet weak var totalPriceCaptionLabel: UILabel!
	@IBOutlet weak var totalPriceLabel: UILabel!
	
	@IBOutlet weak var stackView: UIStackView!
	@IBOutlet weak var detailsRateLegendLabel: UILabel!
	@IBOutlet weak var detailsRateLabel: UILabel!
	@IBOutlet weak var detailsDurationLegendLabel: UILabel!
	@IBOutlet weak var detailsDurationLabel: UILabel!
	@IBOutlet weak var detailsCallCostLegendLabel: UILabel!
	@IBOutlet weak var detailsCallCostLabel: UILabel!
	@IBOutlet weak var detailsFeeLabel: UILabel!
	@IBOutlet weak var detailsFeeLegendLabel: UILabel!
	@IBOutlet weak var detailsTotalLegendLabel: UILabel!
	@IBOutlet weak var detailsTotalLabel: UILabel!
	
	@IBOutlet weak var paymentCardButton: PaymentCardButton!
	@IBOutlet weak var shortCallsInfoLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let dateFormatter = NSDateFormatter()
		dateFormatter.dateStyle = .MediumStyle
		dateFormatter.timeStyle = .NoStyle
		let dateString = dateFormatter.stringFromDate(callHistoryItem.date)
		dateFormatter.dateStyle = .NoStyle
		dateFormatter.timeStyle = .ShortStyle
		let timeString = dateFormatter.stringFromDate(callHistoryItem.date)
		dateLabel.text = NSString(format: NSLocalizedString("history.call date %@ at time %@", comment: "History"), dateString, timeString) as String
		
		callerAvatarView.user = callHistoryItem.caller
		calleeAvatarView.user = callHistoryItem.callee
		
		let subject = NSMutableAttributedString()
		if callHistoryItem.caller == User.currentUser {
			let whoAnsweredText = NSAttributedString(string: NSLocalizedString("history.you answered", comment: "History").stringByAppendingString("\n"), attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
			let usernameText = NSAttributedString(string: callHistoryItem.callee.fullName, attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor.blackColor()])
			subject.appendAttributedString(whoAnsweredText)
			subject.appendAttributedString(usernameText)
		} else {
			let usernameText = NSAttributedString(string: callHistoryItem.caller.fullName, attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor.blackColor()])
			let whoAnsweredText = NSAttributedString(string: "\n".stringByAppendingString(NSLocalizedString("history.peer answered", comment: "History")), attributes: [NSFontAttributeName: subjectLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
			subject.appendAttributedString(usernameText)
			subject.appendAttributedString(whoAnsweredText)
		}
		subjectLabel.attributedText = subject
		
		questionTextLabel.text = callHistoryItem.text
		
		if let rate = callHistoryItem.rate {
			if rate.doubleValue != 0 {
				isFreeCall = false
				
				let rateColor: UIColor
				if callHistoryItem.caller == User.currentUser {
					rateColor = UIColor.brandGreenColor()
				} else {
					rateColor = UIColor.blackColor()
				}
				
				let font = rateLabel.font
				let attributedString = NSMutableAttributedString(string: NSLocalizedString("card.price.%@ per minute", comment: "Card"), attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
				let range = (attributedString.string as NSString).rangeOfString("%@")
				attributedString.setAttributes([NSForegroundColorAttributeName: rateColor], range: range)
				attributedString.replaceCharactersInRange(range, withString: rate.formattedValue)
				rateLabel.attributedText = attributedString
			}
		}
		if isFreeCall {
			rateLabel.text = NSLocalizedString("card.price.free", comment: "Card")
		}
		
		topicsView.textColor = UIColor(white: 38.0/255.0, alpha: 1.0)
		topicsView.bubbleBackgroundColor = UIColor(white: 241.0/255.0, alpha: 1.0)
		topicsView.allowsEditing = false
		topicsView.topics = callHistoryItem.topics
		
		ratingControlCaptionLabel.text = callHistoryItem.rating > 0 ? NSLocalizedString("call details.your rating", comment: "Call Details") : NSLocalizedString("call details.no rating", comment: "Call Details")
		ratingControl.rating = callHistoryItem.rating
		rateUserButton.enabled = callHistoryItem.rating == 0
		
		shortCallsInfoLabel.text = NSLocalizedString("call details.label.short calls are free", comment: "Call Details")
		shortCallsInfoLabel.hidden = !(callHistoryItem.rate.cents > 0 && callHistoryItem.totalCost.cents == 0)
		
		paymentCardButton.hidden = true
		if callHistoryItem.details != nil {
			self.view.layoutIfNeeded()
			displayCallDetails()
		} else {
			stackView.hidden = true
			API.sharedAPI.getCallDetails(callHistoryItem) { (callDetails, error) -> Void in
				guard error == nil else {
					self.reportError(error)
					return
				}
				
				if callDetails != nil {
					self.callHistoryItem.details = callDetails
					self.displayCallDetails()
				}
			}
		}
	}
	
	private func displayCallDetails() {
		guard let callDetails = callHistoryItem.details else { return }
		
		let durationFormatter = NSDateComponentsFormatter()
		durationFormatter.unitsStyle = .Abbreviated
		durationFormatter.zeroFormattingBehavior = .DropLeading
		let callDurationString = durationFormatter.stringFromTimeInterval(NSTimeInterval(callHistoryItem.duration))!
		
		stackView.hidden = false
		
		self.detailsRateLegendLabel.text = NSLocalizedString("call details.legend.rate", comment: "Call Details")
		self.detailsDurationLegendLabel.text = NSLocalizedString("call details.legend.duration", comment: "Call Details")
		self.detailsCallCostLegendLabel.text = NSLocalizedString("call details.legend.call cost", comment: "Call Details")
		self.detailsTotalLegendLabel.text = NSLocalizedString("call details.legend.total", comment: "Call Details")
		
		if isFreeCall {
			self.detailsDurationLabel.text = callDurationString
			self.totalPriceLabel.superview?.hidden = true
			self.detailsRateLabel.superview?.hidden = true
			self.detailsCallCostLabel.superview?.hidden = true
			self.detailsFeeLabel.superview?.hidden = true
			self.detailsTotalLabel.superview?.hidden = true
			return
		}
		
		totalPriceCaptionLabel.text = callHistoryItem.caller == User.currentUser ? NSLocalizedString("call details.you earned.total", comment: "Call Details") : NSLocalizedString("call details.you paid.total", comment: "Call Details")
		if callHistoryItem.caller == User.currentUser {
			totalPriceLabel.text = callHistoryItem.totalCost.formattedValue
			totalPriceLabel.textColor = UIColor.brandGreenColor()
		} else {
			totalPriceLabel.text = callHistoryItem.totalCost.formattedValue
			totalPriceLabel.textColor = UIColor.blackColor()
		}
		
		self.detailsRateLabel.text = callHistoryItem.rate.formattedValue
		self.detailsDurationLabel.text = callDurationString
		self.detailsCallCostLabel.text = (callHistoryItem.callCost).formattedValue
		self.detailsTotalLabel.text = callHistoryItem.totalCost.formattedValue
		
		if callDetails.clientCharge == nil && callDetails.consultantCharge == nil {
			self.detailsFeeLabel.superview?.hidden = true
		} else {
			if let clientChargeData = callDetails.clientCharge {
				self.detailsFeeLabel.text = clientChargeData.baseClientFee.formattedValue
				self.detailsFeeLegendLabel.text = NSLocalizedString("call details.legend.base fee", comment: "Call Details")
				if let paymentCardInfo = clientChargeData.paymentCard {
					self.paymentCardButton.cardInfo = paymentCardInfo
					self.paymentCardButton.hidden = false
				}
			}
			if let consultantChargeData = callDetails.consultantCharge {
				self.detailsFeeLabel.text = "(\(consultantChargeData.serviceFee.formattedValue))"
				self.detailsFeeLegendLabel.text = NSLocalizedString("call details.legend.service fee", comment: "Call Details")
			}
		}
	}
	
	@IBAction func rateUser() {
		let popoverContent = MiniRatingViewController(callID: callHistoryItem.callIdentifier) { (rating) in
			self.ratingControlCaptionLabel.text = NSLocalizedString("call details.your rating", comment: "Call Details")
			self.callHistoryItem.rating = rating
			self.ratingControl.rating = rating
			self.rateUserButton.enabled = rating == 0
			self.dismissViewControllerAnimated(true, completion: nil)
		}
		let popover = popoverContent.popoverPresentationController
		popover?.sourceView = self.ratingControl
		popover?.sourceRect = self.ratingControl.bounds
		self.presentViewController(popoverContent, animated: true, completion: nil)
	}
	
	@IBAction func back(sender: UIButton) {
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func reportAbuse(sender: UIButton) {
		let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/call/\(callHistoryItem.identifier)/report")!
		let webViewController = WebViewController.navigationController(url, title: nil, excludedDomains: nil, callback: nil)
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
	
	override func ignoresPullToDismissGesture() -> Bool {
		return true
	}
}
