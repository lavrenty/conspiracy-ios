//
//  RatingViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 17.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class RatingViewController: UIViewController {
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var durationLabel: UILabel!
	@IBOutlet weak var personRatingControl: RatingControl!
	@IBOutlet weak var qualityRatingControl: RatingControl!
	
	var call: Call!
	var completion: ((Void) -> Void)?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if call.isClientSide {
			if call.totalCost != nil && call.totalCost?.cents != 0 {
				let fmt = NSLocalizedString("rating.you paid user %@ amount %@", comment: "Rating")
				priceLabel.text = NSString(format: fmt, call.caller!.fullName!, call.totalCost!.formattedValue) as String
			} else {
				priceLabel.text = NSString(format: NSLocalizedString("rating.user %@ answered your question for free", comment: "Rating"), call.caller!.fullName!) as String
			}
		} else {
			if call.totalCost != nil && call.totalCost?.cents != 0 {
				let fmt = NSLocalizedString("rating.user %@ paid you %@", comment: "Rating")
				priceLabel.text = NSString(format: fmt, call.callee!.fullName!, call.totalCost!.formattedValue) as String
			} else {
				priceLabel.text = NSString(format: NSLocalizedString("rating.you answered user %@'s question for free", comment: "Rating"), call.callee!.fullName!) as String
			}
		}
		
		let time = call.duration
		let s = time % 60
		let m = time / 60
		durationLabel.text = String(format: "%d:%02d", m, s)
	}
	
	@IBAction func reportAbuse(sender: UIButton) {
		let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/call/\(call.identifier)/report")!
		let webViewController = WebViewController.navigationController(url, title: nil, excludedDomains: nil, callback: nil)
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
	
	@IBAction func rateAndClose(sender: UIButton) {
		API.sharedAPI.rateCall(call.identifier, userRating: personRatingControl.rating, connectionQuality: qualityRatingControl.rating, completion: nil)
		self.completion?()
	}
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
}
