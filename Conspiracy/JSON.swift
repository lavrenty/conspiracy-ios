//
//  JSON.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


func JSON(data: NSData?) -> AnyObject? {
	guard let theData = data else {
		return nil
	}
	guard theData.length >= 2 else {
		return nil
	}
	do {
		let result = try NSJSONSerialization.JSONObjectWithData(theData, options: [])
		return result
	} catch let error as NSError {
		fatalError("Invalid JSON: \(error)")
	}
}
