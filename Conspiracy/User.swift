//
//  User.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


enum Gender {
	case Male, Female
}


class User: APIEntity {
//	var firstName: String!
//	var middleName: String!
//	var lastName: String!
	var fullName: String!
	var gender: Gender?
//	var locale: String?
	var imageURL: NSURL? = nil
	
	var isVerified = false
	var hasPushNotificationsEnabled = false
	var hasConnectedFacebook = false
	var hasConnectedLinkedIn = false
	var facebookPermissions: [String]?
	var paymentCard: PaymentCardInfo?
	var isPayable = false
	var hasUnpaidInvoice = false
	
	var languages: [String]!
	var topics: [Topic]?
	var balance = Money(cents: 0, currency: nil)!
	var answerCount: Int = 0
	var questionCount: Int = 0
	
	static var currentUser: User?
	
	#if DEBUG
	override var description: String {
		return "<\(self.dynamicType) id=\(identifier) \(fullName.isEmpty ? "ANONYMOUS" : fullName)>"
	}
	#endif
}
