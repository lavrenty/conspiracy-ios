//
//  Estimate.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 07.09.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation


struct Estimate {
	var rate: Money
	var userCount: Int = 0
	var score: Int = 0
}
