//
//  APIEntity.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 4.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


class APIEntity: Equatable, CustomStringConvertible {
	var identifier: Int
	init(identifier: Int) {
		self.identifier = identifier
	}
	
	var description: String {
		return "<\(self.dynamicType) id=\(identifier)>"
	}
}


func ==(lhs: APIEntity, rhs: APIEntity) -> Bool { return lhs.dynamicType === rhs.dynamicType && lhs.identifier == rhs.identifier }
