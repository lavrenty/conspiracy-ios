//
//  CardEditor.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import Amplitude_iOS


struct NewCardData {
	var questionText: String?
	var topics: [Topic]?
	var rate: Money!
}


class CardEditor: UIViewController {
	static var savedCardData: NewCardData?
	
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var topicsView: TopicsView!
	@IBOutlet weak var questionView: TextView!
	@IBOutlet weak var questionViewHeightConstraint: NSLayoutConstraint!
	@IBOutlet weak var rateLabel: UILabel!
	@IBOutlet weak var rateSlider: RateSlider!
	@IBOutlet weak var explanationLabel: UILabel!
	@IBOutlet weak var firstMinuteExplanationLabel: UILabel!
	@IBOutlet weak var callButton: UIButton!
	private var suggestedTopics: [Topic]!
	private var recentTopics: [Topic]?
	private var estimates: [Estimate]?
	private var skipSavingCardData = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("new card.controller title", comment: "New Card")
		
		suggestedTopics = Array()
		topicsView.placeholderText = NSLocalizedString("new card.placeholder.area of expertise", comment: "New Card")
		topicsView.tapHandler = { [weak self] in
			self?.performSegueWithIdentifier("addTopic", sender: nil)
		}
		topicsView.deleteHandler = { [weak self] (deletedTopic) -> Void in
			self?.validatePairingButton()
			self?.getEstimates()
		}
		
		questionView.text = ""
		questionView.placeholderText = NSLocalizedString("new card.placeholder.what do you need to ask", comment: "New Card")
//		rateSlider.currencyCode = "USD"
		rateSlider.values = [0.0, 0.2, 0.3, 0.5, 0.8, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0]
		rateSlider.value = User.currentUser!.paymentCard != nil ? 1.0 : 0.0
		setPrice(rateSlider)
		
		firstMinuteExplanationLabel.text = NSLocalizedString("new card.first minute explanation", comment: "New Card")
		callButton.setTitle(NSLocalizedString("new card.button.call", comment: "New Card"), forState: .Normal)
		
		// Restore saved card data
		if let savedCardData = CardEditor.savedCardData {
			self.questionView.text = savedCardData.questionText
			if savedCardData.topics != nil {
				self.topicsView.topics = savedCardData.topics!
			}
			self.rateSlider.value = savedCardData.rate.doubleValue
			setPrice(rateSlider)
		}
		self.validatePairingButton()
		
		API.sharedAPI.getSuggestedTopics { (recentTopics, popularTopics, error) -> Void in
			if popularTopics != nil {
				self.suggestedTopics = popularTopics!
				self.recentTopics = recentTopics
			}
		}
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		questionView.resignFirstResponder()
		
		if !self.skipSavingCardData {
			if CardEditor.savedCardData == nil {
				CardEditor.savedCardData = NewCardData()
			}
			CardEditor.savedCardData!.questionText = self.questionView.text
			CardEditor.savedCardData!.topics = self.topicsView.topics
			CardEditor.savedCardData!.rate = Money(doubleValue: self.rateSlider.value, currency: nil)!
		}
	}
	
	private func getEstimates() {
		if let question = questionView.text {
			if !question.isEmpty && !topicsView.topics.isEmpty {
				API.sharedAPI.getEstimates(question, topics: topicsView.topics, completion: { (estimates, error) in
					if estimates != nil {
						self.estimates = estimates
						self.setPrice(self.rateSlider)
					}
				})
			}
		}
	}
	
	private func validatePairingButton() {
		self.callButton.enabled = questionView.text != nil && !questionView.text!.isEmpty && topicsView.topics.count > 0
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func setPrice(sender: AnyObject) {
		let rate = Money(doubleValue: rateSlider.value, currency: nil)!
		
		if rate.doubleValue > 0 {
			let formatString = NSLocalizedString("new card.rate.%@ per minute, %@ per hour", comment: "New Card")
			let perMinuteRateRange = (formatString as NSString).rangeOfString("%1$@")
			let perMinuteRateString = rate.formattedValue
			let perHourRateRange = (formatString as NSString).rangeOfString("%2$@")
			let perHourRateString = (rate * 60).formattedValue
			
			let defaultAttrs = [NSFontAttributeName: rateLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)]
			let attributedString = NSMutableAttributedString(string: formatString, attributes: defaultAttrs)
			attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: perMinuteRateRange)
			if perMinuteRateRange.location < perHourRateRange.location {
				attributedString.replaceCharactersInRange(perHourRateRange, withString: perHourRateString)
				attributedString.replaceCharactersInRange(perMinuteRateRange, withString: perMinuteRateString)
			} else {
				attributedString.replaceCharactersInRange(perMinuteRateRange, withString: perMinuteRateString)
				attributedString.replaceCharactersInRange(perHourRateRange, withString: perHourRateString)
			}
			
			rateLabel.attributedText = attributedString
		} else {
			rateLabel.text = NSLocalizedString("new card.label.free call", comment: "New Card")
		}
		
		var estimateTextShown = false
		if estimates != nil {
			for e in estimates! {
				if rate >= e.rate {
					Log("\(e)")
					explanationLabel.text = NSString(format: NSLocalizedString("new card.you'll reach %d users with score %d", comment: "New Card"), e.userCount, e.score) as String
					estimateTextShown = true
					break
				}
			}
		}
		if !estimateTextShown {
			let style = NSMutableParagraphStyle()
			style.alignment = .Center
			style.minimumLineHeight = explanationLabel.font.pointSize + 4 // 16
			explanationLabel.attributedText = NSAttributedString(string: NSLocalizedString("new card.rate explanation", comment: "New Card"), attributes: [NSFontAttributeName: explanationLabel.font, NSForegroundColorAttributeName: explanationLabel.textColor, NSParagraphStyleAttributeName: style])
		}
		
		firstMinuteExplanationLabel.hidden = rate.doubleValue == 0
		
		if rate.doubleValue == 0 {
			self.dismissConspiracySheet(true)
		} else {
			if !self.isPresentingConspiracySheet() {
				if User.currentUser!.paymentCard == nil {
					let sheet = Sheet(title: NSLocalizedString("sheet.title.add payment card", comment: "New Card"), message: nil)
					sheet.backgroundColor = UIColor.brandOrangeColor()
					sheet.tintColor = UIColor.whiteColor()
					sheet.addAction(UIAlertAction(title: NSLocalizedString("sheet.button.add payment card", comment: "New Card"), style: .Default, handler: { (_) -> Void in
						let vc = self.storyboard!.instantiateViewControllerWithIdentifier("payment-card-vc") as! PaymentCardEditor
						vc.completion = { (cardInfo) -> Void in
							self.dismissConspiracySheet(true)
						}
						self.presentViewController(vc, animated: true, completion: nil)
					}))
					self.presentConspiracySheet(sheet, animated: true)
				} else
				if User.currentUser!.hasUnpaidInvoice {
					let sheet = Sheet(title: NSLocalizedString("sheet.title.unpaid invoice", comment: "New Card"), message: nil)
					sheet.backgroundColor = UIColor.brandOrangeColor()
					sheet.tintColor = UIColor.whiteColor()
					sheet.addAction(UIAlertAction(title: NSLocalizedString("sheet.button.pay now", comment: "New Card"), style: .Default, handler: { (_) -> Void in
						let vc = self.storyboard!.instantiateViewControllerWithIdentifier("payment-card-vc") as! PaymentCardEditor
						vc.completion = { (cardInfo) -> Void in
							self.dismissConspiracySheet(true)
						}
						self.presentViewController(vc, animated: true, completion: nil)
					}))
					self.presentConspiracySheet(sheet, animated: true)
				}
			}
		}
	}
	
	@IBAction func startPairing(sender: UIButton) {
		assert(User.currentUser!.paymentCard != nil || rateSlider.value == 0)
		self.performSegueWithIdentifier("showVideo", sender: sender)
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if let segueIdentifier = segue.identifier {
			if segueIdentifier == "addTopic" {
				self.skipSavingCardData = true // viewWillDisappear() is *not* called
				
				let topicPicker = (segue.destinationViewController as! SpecialPresentationController).contentViewController as! TopicPicker
				topicPicker.suggestedTopics = suggestedTopics
				topicPicker.recentTopics = recentTopics
				topicPicker.excludedTopics = topicsView.topics
				topicPicker.completion = { (topic) -> Void in
					if !self.topicsView.topics.contains(topic) {
						self.topicsView.addTopic(topic)
						self.validatePairingButton()
						self.getEstimates()
						self.skipSavingCardData = false
					}
				}
				return
			}
			
			if segueIdentifier == "showVideo" {
				let card = UserCard(identifier: 0)
				card.user = User.currentUser
				card.text = questionView.text
				card.topics = topicsView.topics
				card.price = Money(doubleValue: rateSlider.value, currency: nil)!
				card.live = true
				
				let callViewController = segue.destinationViewController as! VideoViewController
				callViewController.card = card
				callViewController.completion = { (callDidSucceed) -> Void in
					if callDidSucceed {
						// Fall through to the dashboard
						self.skipSavingCardData = true
						CardEditor.savedCardData = nil
						self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
					} else {
						// Dismiss the call view controller
						self.skipSavingCardData = false
						self.navigationController?.popToViewController(self, animated: true)
					}
					NSNotificationCenter.defaultCenter().postNotificationName(RadarDidDisconnectNotification, object: nil)
				}
			}
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
}


// MARK: - UITextViewDelegate


extension CardEditor {
	func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
		if text == "\n" {
			textView.resignFirstResponder()
			getEstimates()
			return false
		}
		
		return true
	}
	
	func textViewDidChange(textView: UITextView) {
//		Log("frame: \(textView.frame.size.height), size to fit: \(textView.sizeThatFits(textView.bounds.size).height)/\(textView.intrinsicContentSize().height), content: \(textView.contentSize.height)")
		textView.scrollEnabled = textView.sizeThatFits(textView.bounds.size).height > questionViewHeightConstraint.constant
		self.validatePairingButton()
	}
}
