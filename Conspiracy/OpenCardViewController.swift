//
//  OpenCardViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 21.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import Amplitude_iOS


let CardHasBeenAnsweredNotification = "CardHasBeenAnsweredNotification"


class OpenCardViewController: UIViewController {
	@IBOutlet weak var avatarView: AvatarView?
	@IBOutlet weak var callerAvatarView: AvatarView!
	@IBOutlet weak var userNameLabel: UILabel?
	@IBOutlet weak var cardTextLabel: UILabel?
	@IBOutlet weak var rateLabel: UILabel?
	@IBOutlet weak var topicsView: TopicsView?
	@IBOutlet weak var actionButton: UIButton?
	@IBOutlet weak var skipButton: UIButton?
	
	var card: Card!
	var actionDelegate: CardViewActionDelegate!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		guard card != nil else {
			Log("*** Card has not been set")
			return
		}
		
		let cardViewNibName: String
		switch card {
			case is UserCard:
				cardViewNibName = "OpenCardView-user"
				break
				
			case is SystemEndorsementCard:
				cardViewNibName = "OpenCardView-endorsement"
				break
			
			case is TopicCard:
				cardViewNibName = "OpenCardView-topic"
				break
			
			case is PushNotificationsCard:
				cardViewNibName = "OpenCardView-push-notifications"
				break
			
			default:
				Log("*** Card type \(card.dynamicType) is not supported")
				return
		}
		let cardViewNib = UINib(nibName: cardViewNibName, bundle: nil)
		
		if let cardView = cardViewNib.instantiateWithOwner(self, options: nil).first as? UIView {
			cardView.frame = self.view.bounds
			self.view.addSubview(cardView)
			self.view.backgroundColor = cardView.backgroundColor
			
			cardView.translatesAutoresizingMaskIntoConstraints = false
			var viewBindingsDict = [String: AnyObject]()
			viewBindingsDict["cardView"] = cardView
			NSLayoutConstraint.activateConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[cardView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewBindingsDict))
			NSLayoutConstraint.activateConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[cardView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewBindingsDict))
			
			switch card {
				case is UserCard:
					cardView.backgroundColor = UIColor.clearColor()
					
					let userCard = card as! UserCard
					avatarView?.user = userCard.user
					cardTextLabel?.text = userCard.text
					
					var isFree = true
					if let rate = userCard.price {
						if rate.doubleValue != 0 {
							isFree = false
							
							let font = rateLabel!.font
							let attributedString = NSMutableAttributedString(string: NSLocalizedString("card.price.%@ per minute", comment: "Card"), attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(white: 192.0/255.0, alpha: 1.0)])
							let range = (attributedString.string as NSString).rangeOfString("%@")
							attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
							attributedString.replaceCharactersInRange(range, withString: rate.formattedValue)
							rateLabel!.attributedText = attributedString
						}
					}
					if isFree {
						rateLabel!.text = NSLocalizedString("card.price.free", comment: "Card")
					}
					
					topicsView!.textColor = UIColor(white: 38.0/255.0, alpha: 1.0)
					topicsView!.allowsEditing = false
					topicsView!.topics = userCard.topics ?? []
					
					NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OpenCardViewController.cardDidUpdate(_:)), name: UserCardDidUpdateNotification, object: card)
					self.cardDidUpdate(NSNotification(name: UserCardDidUpdateNotification, object: card))
					
					actionButton?.setTitle(NSLocalizedString("open card.button.answer", comment: "Open Card"), forState: .Normal)
					Amplitude.instance().logEvent("Card opened", withEventProperties: ["card_id": userCard.identifier])
					break
					
				case is SystemEndorsementCard:
					let endorsementCard = card as! SystemEndorsementCard
					avatarView?.user = endorsementCard.user
					cardTextLabel?.text = NSString(format: NSLocalizedString("open card.text.endorse %@ in %@", comment: "Open Card"), endorsementCard.user.fullName, endorsementCard.topic.name) as String
					skipButton?.setTitle(NSLocalizedString("open card.button.skip endorsement", comment: "Open Card"), forState: .Normal)
					actionButton?.setTitle(NSLocalizedString("open card.button.endorse", comment: "Open Card"), forState: .Normal)
					break
					
				case is TopicCard:
					let topicCard = card as! TopicCard
					cardTextLabel?.text = NSString(format: NSLocalizedString("open card.text.add topic %@", comment: "Open Card"), topicCard.topic.name) as String
					actionButton?.setTitle(NSLocalizedString("open card.button.add topic", comment: "Open Card"), forState: .Normal)
					break
				
				case is PushNotificationsCard:
					cardTextLabel?.text = NSLocalizedString("open card.text.enable push notifications", comment: "Open Card")
					actionButton?.setTitle(NSLocalizedString("open card.button.enable push notifications", comment: "Open Card"), forState: .Normal)
					break
				
				default:
					break
			}
		}
		
		self.view.layer.cornerRadius = 6
	}
	
	private func displaySheetIfNeeded() {
		guard let userCard = card as? UserCard else { return }
		
		var isFreeCard = true
		if userCard.price != nil && userCard.price!.doubleValue != 0 {
			isFreeCard = false
		}
		
		var sheet: Sheet?
		if userCard.caller != nil || !userCard.live {
			let sheetTitleFormatString = userCard.caller != nil ? NSLocalizedString("sheet.title.%@ is already being called", comment: "Open Card") : NSLocalizedString("sheet.title.missed call from %@", comment: "Open Card")
			sheet = Sheet(title: NSString(format: sheetTitleFormatString, userCard.user.fullName) as String, message: nil)
			sheet!.backgroundColor = UIColor(white: 155.0/255.0, alpha: 1.0)
			sheet!.tintColor = UIColor.whiteColor()
			sheet!.addAction(UIAlertAction(title: actionButton?.titleForState(.Normal), style: .Default, handler: { (_) -> Void in
				let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/billing/setup")!
				let webViewController = WebViewController.navigationController(url, title: NSLocalizedString("payout.controller title", comment: "Payout"), excludedDomains: ["stripe.com"], callback: { (callbackURL) -> Void in
					API.sharedAPI.getCurrentUser { (user, error) -> Void in
						if User.currentUser!.isPayable {
							self.dismissConspiracySheet(true)
						}
					}
				})
				webViewController.topViewController?.title = NSLocalizedString("payout.controller title", comment: "Payout")
				self.presentViewController(webViewController, animated: true, completion: nil)
			}))
			if let sheetButton = sheet!.actionButton(0) {
				sheetButton.setImage(actionButton!.imageForState(.Normal), forState: .Normal)
				sheetButton.setTitleColor(UIColor(white: 205.0/255.0, alpha: 1.0), forState: .Disabled)
				sheetButton.contentEdgeInsets = actionButton!.contentEdgeInsets
				sheetButton.titleEdgeInsets = actionButton!.titleEdgeInsets
				sheetButton.imageEdgeInsets = actionButton!.imageEdgeInsets
				sheetButton.enabled = false
			}
		} else
		if !User.currentUser!.isPayable && !isFreeCard {
			sheet = Sheet(title: NSLocalizedString("sheet.title.payout not set up", comment: "Open Card"), message: nil)
			sheet!.backgroundColor = UIColor.brandOrangeColor()
			sheet!.tintColor = UIColor.whiteColor()
			sheet!.addAction(UIAlertAction(title: NSLocalizedString("sheet.button.add payout", comment: "Open Card"), style: .Default, handler: { (_) -> Void in
				let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/billing/setup")!
				let webViewController = WebViewController.navigationController(url, title: NSLocalizedString("payout.controller title", comment: "Payout"), excludedDomains: ["stripe.com"], callback: { (callbackURL) -> Void in
					API.sharedAPI.getCurrentUser { (user, error) -> Void in
						if User.currentUser!.isPayable {
							self.dismissConspiracySheet(true)
						}
					}
				})
				webViewController.topViewController?.title = NSLocalizedString("payout.controller title", comment: "Payout")
				self.presentViewController(webViewController, animated: true, completion: nil)
			}))
		}
		
		if sheet != nil {
			self.presentConspiracySheet(sheet!, animated: false)
		} else {
			self.dismissConspiracySheet(true)
		}
	}
	
	internal func cardDidUpdate(notification: NSNotification) {
		guard let userCard = card as? UserCard else { return }
		
		let textFormatString: String
		if let caller = userCard.caller {
			callerAvatarView.user = caller
			textFormatString = NSLocalizedString("card.text.user %@ is talking", comment: "Card")
		} else {
			callerAvatarView.image = UIImage(named: userCard.live ? "btn-card-call-big" : "btn-card-call-big-disabled")
			textFormatString = userCard.live ? NSLocalizedString("card.text.user %@ is asking", comment: "Card") : NSLocalizedString("card.text.user %@ was asking", comment: "Card")
		}
		let attributedString = NSMutableAttributedString(string: textFormatString, attributes: [NSFontAttributeName: userNameLabel!.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
		let range = (attributedString.string as NSString).rangeOfString("%@")
		attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
		attributedString.replaceCharactersInRange(range, withString: userCard.user.fullName)
		userNameLabel!.attributedText = attributedString
		
		self.view.backgroundColor = userCard.live ? UIColor.whiteColor() : UIColor(white: 220.0/255.0, alpha: 1.0)
		topicsView?.bubbleBackgroundColor = UIColor(white: userCard.live ? 237.0/255.0 : 200.0/255.0, alpha: 1.0)
		actionButton?.enabled = userCard.live
		
		self.displaySheetIfNeeded()
	}
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	@IBAction func close(sender: UIButton) {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func endorseUser(sender: UIButton) {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: {
			self.actionDelegate.endorse(self.card as! SystemEndorsementCard)
		})
	}
	
	@IBAction func skipEndorsingUser(sender: UIButton) {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: {
			self.actionDelegate.closeCard(self.card)
		})
	}
	
	@IBAction func addTopic(sender: UIButton) {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: {
			self.actionDelegate.addTopic(self.card as! TopicCard)
		})
	}
	
	@IBAction func enablePushNotifications(sender: UIButton) {
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: {
			self.actionDelegate.enablePushNotifications(self.card as! PushNotificationsCard)
		})
	}
	
	@IBAction func answer(sender: UIButton) {
		assert(card is UserCard)
		let userCard = card as! UserCard
		
		if !userCard.live {
			let alert = UIAlertController(title: NSLocalizedString("open card.card is busy", comment: "Open Card"), message: nil, preferredStyle: .Alert)
			alert.addAction(UIAlertAction(title: NSLocalizedString("button.ok", comment: "Buttons"), style: .Default, handler: nil))
			self.presentViewController(alert, animated: true, completion: nil)
			return
		}
		
		let presentingViewController = self.presentingViewController!
		let callViewController = self.storyboard!.instantiateViewControllerWithIdentifier("call-vc") as! VideoViewController
		callViewController.card = userCard
		callViewController.actionDelegate = self.actionDelegate
		callViewController.completion = { (callDidSucceed) -> Void in
			presentingViewController.dismissViewControllerAnimated(true, completion: nil)
			NSNotificationCenter.defaultCenter().postNotificationName(CardHasBeenAnsweredNotification, object: userCard)
		}
		self.navigationController?.setViewControllers([callViewController], animated: true)
	}
	
	@IBAction func reportAbuse(sender: UIButton) {
		let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/card/\(card.identifier)/report")!
		let webViewController = WebViewController.navigationController(url, title: nil, excludedDomains: nil, callback: nil)
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
}
