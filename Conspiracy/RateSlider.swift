//
//  RateSlider.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 30.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class RateSlider: UIControl, UIScrollViewDelegate {
	private var scrollView: UIScrollView!
	private var labels: [UILabel]!
	private var indicator: UIImageView!
	private var hasShownInitialValue = false
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		let indicatorSize = CGSize(width: CGRectGetHeight(self.bounds), height: CGRectGetHeight(self.bounds))
		UIGraphicsBeginImageContextWithOptions(indicatorSize, false, 0)
		UIColor.brandGreenColor().setStroke()
		let circle = UIBezierPath(ovalInRect: CGRectInset(CGRect(origin: CGPointZero, size: indicatorSize), 1, 1))
		circle.lineWidth = 2
		circle.stroke()
		indicator = UIImageView(image: UIGraphicsGetImageFromCurrentImageContext())
		UIGraphicsEndImageContext()
		
		self.addSubview(indicator)
		
		scrollView = UIScrollView(frame: self.bounds)
		scrollView.backgroundColor = UIColor.clearColor()
		scrollView.showsHorizontalScrollIndicator = false
		scrollView.showsVerticalScrollIndicator = false
		scrollView.decelerationRate = UIScrollViewDecelerationRateFast
		scrollView.delegate = self
		self.addSubview(scrollView)
		
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(RateSlider.handleTap(_:)))
		self.addGestureRecognizer(tapRecognizer)
		
		labels = Array()
	}
	
	var currencyCode: String?
	
	var values: [Double]! {
		didSet {
			for label in labels {
				label.removeFromSuperview()
			}
			labels.removeAll()
			
			let labelSize = CGSize(width: CGRectGetHeight(self.bounds), height: CGRectGetHeight(self.bounds))
			var origin = CGPointZero
			for value in values {
				let label = UILabel(frame: CGRect(origin: origin, size: labelSize))
				label.font = UIFont.systemFontOfSize(14.0)
				label.textColor = UIColor.blackColor()
				label.highlightedTextColor = UIColor.whiteColor()
				label.textAlignment = .Center
//				label.text = Money(doubleValue: value, currency: currencyCode)!.formattedValue
				if value == round(value) {
					label.text = NSString(format: "%g", value) as String
				} else {
					label.text = NSString(format: "%.2f", value) as String
				}
				labels.append(label)
				scrollView.addSubview(label)
				
				origin.x = origin.x + labelSize.width + 8.0
			}
			scrollView.contentSize = CGSize(width: origin.x - 8.0, height: CGRectGetHeight(scrollView.bounds))
		}
	}
	
	var value: Double!
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		indicator.center = CGPoint(x: CGRectGetMidX(self.bounds), y: CGRectGetMidY(self.bounds))
		scrollView.frame = self.bounds
		let horizontalInset = 0.5 * (CGRectGetWidth(self.bounds) - CGRectGetHeight(self.bounds))
		scrollView.contentInset = UIEdgeInsets(top: 0, left: horizontalInset, bottom: 0, right: horizontalInset)
		
		if value == nil && !values.isEmpty {
			value = values.first
		}
		if !hasShownInitialValue {
			if let defaultValue = value {
				if let index = values.indexOf(defaultValue) {
					scrollView.setContentOffset(CGPoint(x: labels[index].center.x - 0.5 * CGRectGetWidth(self.bounds), y: 0), animated: false)
//					labels[index].highlighted = true
					hasShownInitialValue = true
				}
			}
		}
	}
	
	override func intrinsicContentSize() -> CGSize {
		return CGSize(width: CGRectGetWidth(self.bounds), height: indicator.image!.size.height)
	}
	
	func handleTap(recognizer: UITapGestureRecognizer) {
		if let closestLabel = getLabelClosestToPoint(recognizer.locationInView(scrollView).x) {
			scrollView.setContentOffset(CGPoint(x: closestLabel.center.x - 0.5 * CGRectGetWidth(self.bounds), y: 0), animated: true)
			self.selectedValueIndex = labels.indexOf(closestLabel)
		}
	}
	
	private var selectedValueIndex: Int?
	
	func getLabelClosestToPoint(offset: CGFloat) -> UILabel? {
		var minDistance = CGFloat.max
		var theLabel: UILabel?
		for label in labels {
			let distance = abs(label.center.x - offset)
			if distance < minDistance {
				minDistance = distance
				theLabel = label
			}
		}
		return theLabel
	}
	
	// MARK: - UIScrollViewDelegate
	
	func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		let offset = targetContentOffset.memory.x + 0.5 * CGRectGetWidth(self.bounds)
		if let closestLabel = getLabelClosestToPoint(offset) {
			targetContentOffset.memory.x = closestLabel.center.x - 0.5 * CGRectGetWidth(self.bounds)
			self.selectedValueIndex = labels.indexOf(closestLabel)
		}
	}
	
	func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
//		Log("\(#function)")
		if self.selectedValueIndex != nil {
			self.value = values[self.selectedValueIndex!]
			self.sendActionsForControlEvents(.ValueChanged)
		}
	}
	
	func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
//		Log("\(#function)")
		if self.selectedValueIndex != nil {
			self.value = values[self.selectedValueIndex!]
			self.sendActionsForControlEvents(.ValueChanged)
		}
	}
}
