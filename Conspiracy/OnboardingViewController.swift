//
//  OnboardingViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 15.03.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


struct OnboardingStep {
	let localizableString: String
	let color: UIColor
	let hidePageControl: Bool
}


class OnboardingViewController: UIViewController {
	@IBOutlet weak var transitionView: UIView!
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var pageControl: PageControl!
	var snapshotView: UIView?
	var futurePageIndex: Int = 0 // set before animations
	var currentPageIndex: Int = 0 // set after animations
	let steps = [
		OnboardingStep(localizableString: "onboarding.step1.text", color: UIColor.whiteColor(), hidePageControl: false),
		OnboardingStep(localizableString: "onboarding.step2.text", color: UIColor(red: 49.0/255.0, green: 214.0/255.0, blue: 136.0/255.0, alpha: 1.0), hidePageControl: false),
		OnboardingStep(localizableString: "onboarding.step3.text", color: UIColor(red: 245.0/255.0, green: 166.0/255.0, blue: 35.0/255.0, alpha: 1.0), hidePageControl: false),
		OnboardingStep(localizableString: "", color: UIColor.brandBlueColor(), hidePageControl: true)
	]
	var authenticationController: AuthenticationController?
	
	var completion: ((authorized: Bool) -> Void)!
	var initialStep: Int = 0
	convenience init(completion: (authorized: Bool) -> Void) {
		self.init(nibName: "Onboarding", bundle: nil)
		self.completion = completion
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Replace the top constraint in xib, marked to be removed at build time
		NSLayoutConstraint(item: self.topLayoutGuide, attribute: .Bottom, relatedBy: .Equal, toItem: transitionView, attribute: .Top, multiplier: 1, constant: 0).active = true
		containerView.layer.cornerRadius = 6
		
		// Initial page
		pageControl.numberOfPages = steps.count
		pageControl.currentPage = initialStep
		currentPageIndex = initialStep
		configurePageViewWithPageIndex(currentPageIndex)
	}
	
	@IBAction func setPage() {
		makeSnapshotOfCurrentPage()
		futurePageIndex = pageControl.currentPage
		configurePageViewAndSetTransform(pageControl.currentPage)
		animatePageChange(self.containerView.transform.tx, initialVelocity: 0)
	}
	
	// Disable back swipe after reaching the authentication controller step
//	func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//		return self.pageControl.currentPage < self.steps.count - 1
//	}
	
	@IBAction func handlePanGesture(panRecognizer: UIPanGestureRecognizer) {
		switch panRecognizer.state {
			case .Began:
				makeSnapshotOfCurrentPage()
				break
			
			case .Changed:
				var translation = panRecognizer.translationInView(transitionView).x
				let pageIndexDelta = translation < 0 ? 1 : -1
				if !configureAdjacentPageView(pageIndexDelta) {
					translation = 0
				}
				transitionView.bounds.origin.x = -translation
				break
			
			case .Ended:
				var pageIncrement = 0 // cancelled by default
				let translation = panRecognizer.translationInView(transitionView).x
				let velocity = panRecognizer.velocityInView(transitionView).x
				
				if velocity != 0 {
					// Use velocity direction
					if translation <= 0 && velocity < 0 {
						pageIncrement = 1
					} else
					if translation >= 0 && velocity > 0 {
						pageIncrement = -1
					}
				} else {
					// Use translation direction
					if translation < 0 {
						pageIncrement = 1
					} else
					if translation > 0 {
						pageIncrement = -1
					}
				}
				
				var boundsTranslation: CGFloat = 0
				if pageIncrement != 0 {
					boundsTranslation = containerView.transform.tx
					if !configureAdjacentPageView(pageIncrement) {
						boundsTranslation = 0
						pageIncrement = 0
					}
				}
				
				futurePageIndex = currentPageIndex + pageIncrement
				animatePageChange(boundsTranslation, initialVelocity: velocity)
				break
			
			case .Cancelled:
				futurePageIndex = currentPageIndex
				animatePageChange(0, initialVelocity: 0)
				break
			
			default:
				break
		}
	}
	
	func animatePageChange(boundsTranslation: CGFloat, initialVelocity: CGFloat) {
		let distance = abs(self.transitionView.bounds.origin.x - boundsTranslation)
		var duration: NSTimeInterval = 0.4
		if initialVelocity != 0 {
			duration = min(duration, NSTimeInterval(distance / abs(initialVelocity)))
		}
		
		self.view.userInteractionEnabled = false
		UIView.animateWithDuration(duration, delay: 0, options: [.CurveEaseOut], animations: {
			self.transitionView.bounds.origin.x = boundsTranslation
			}, completion: { (finished) in
				if self.currentPageIndex != self.futurePageIndex {
					self.currentPageIndex = self.futurePageIndex
				} else {
					// Cancelled
					self.configurePageViewWithPageIndex(self.currentPageIndex)
				}
				self.pageControl.currentPage = self.currentPageIndex
				
				self.transitionView.bounds.origin.x = 0
				self.containerView.transform = CGAffineTransformIdentity
				self.removeSnapshotOfCurrentPage()
				self.view.userInteractionEnabled = true
		})
	}
	
	func makeSnapshotOfCurrentPage() {
		snapshotView = containerView.snapshotViewAfterScreenUpdates(false)
		snapshotView!.frame = containerView.frame
		self.transitionView.addSubview(snapshotView!)
	}
	
	func removeSnapshotOfCurrentPage() {
		snapshotView?.removeFromSuperview()
		snapshotView = nil
	}
	
	func configureAdjacentPageView(pageIndexDelta: Int) -> Bool {
		let newPageIndex = currentPageIndex + pageIndexDelta
		guard newPageIndex >= 0 && newPageIndex < pageControl.numberOfPages else { return false }
		
		// Make sure we configure the page view only when necessary
		if futurePageIndex != newPageIndex {
			futurePageIndex = newPageIndex
			pageControl.currentPage = futurePageIndex
			configurePageViewAndSetTransform(futurePageIndex)
		}
		pageControl.hidden = steps[futurePageIndex].hidePageControl
		return true
	}
	
	func configurePageViewAndSetTransform(newPageIndex: Int) {
		configurePageViewWithPageIndex(newPageIndex)
		let appearingPageOffset = CGRectGetWidth(transitionView.bounds) + 10
		containerView.transform = CGAffineTransformMakeTranslation(futurePageIndex > currentPageIndex ? appearingPageOffset : -appearingPageOffset, 0)
	}
	
	func configurePageViewWithPageIndex(pageIndex: Int) {
		let theStep = steps[pageIndex]
		
		containerView.backgroundColor = theStep.color
		
		if theStep.localizableString != "" {
			// Compose attributed text
			let fontSize = textLabel.font.pointSize
			let text = NSLocalizedString(theStep.localizableString, comment: "Onboarding") as NSString
			let normalAttrs = [NSFontAttributeName: UIFont.systemFontOfSize(fontSize, weight: UIFontWeightRegular), NSForegroundColorAttributeName: textLabel.textColor]
			let highlightAttrs = [NSFontAttributeName: UIFont.systemFontOfSize(fontSize, weight: UIFontWeightSemibold), NSForegroundColorAttributeName: textLabel.textColor]
			
			textLabel.attributedText = text.attributedTagString(normalAttrs, tagAttributes: highlightAttrs)
			textLabel.hidden = false
			
			if let authController = self.authenticationController {
				authController.willMoveToParentViewController(nil)
				authController.view.removeFromSuperview()
				authController.removeFromParentViewController()
				self.authenticationController = nil
			}
		} else {
			if self.authenticationController == nil {
				let vc = AuthenticationController(completion: self.completion)
				vc.view.backgroundColor = UIColor.clearColor()
				vc.view.frame = containerView.bounds
				self.addChildViewController(vc)
				containerView.addSubview(vc.view)
				vc.didMoveToParentViewController(self)
				self.authenticationController = vc
			}
			
			textLabel.hidden = true
		}
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
}
