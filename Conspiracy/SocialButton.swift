//
//  SocialButton.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 28.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


@IBDesignable
class SocialButton: UIButton {
	private var badgeView: UIImageView?
	@IBInspectable var iconTemplateName: String!
	
	var connected = false {
		didSet {
			let image = UIImage(named: iconTemplateName)
			if connected {
				self.setImage(image, forState: .Normal)
			} else {
				self.setImage(image?.imageWithTintColor(UIColor(white: 203.0/255.0, alpha: 1.0)), forState: .Normal)
				if badgeView == nil {
					badgeView = UIImageView(image: UIImage(named: "btn-social-connect-badge"))
					self.addSubview(badgeView!)
				}
			}
			badgeView?.hidden = connected
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if badgeView != nil {
			if let image = self.imageForState(.Normal) {
				badgeView!.center = CGPoint(x: 0.5 * (CGRectGetWidth(self.bounds) + image.size.width), y: 0.5 * (CGRectGetHeight(self.bounds) - image.size.height))
				self.bringSubviewToFront(badgeView!)
			}
		}
	}
}
