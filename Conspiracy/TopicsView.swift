//
//  TopicsView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 18.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


enum TopicsViewLayoutMode {
	case SingleLineByWrapping, SingleLineByTruncatingTail, Multiline
}


class TopicsView: UIView, UIGestureRecognizerDelegate {
	private var topicViews = [TopicBubbleView]()
	private var placeholderLabel: UILabel?
	private var truncatedTopicView: TopicBubbleView?
	var lineHeight = CGFloat(24.0)
	let lineSpacing = CGFloat(5.0)
	let topicSpacing = CGFloat(5.0)
	var tapHandler: ((Void) -> Void)?
	var deleteHandler: ((deletedTopic: Topic) -> Void)?
	var font: UIFont!
	var textColor: UIColor!
	var allowsEditing = true
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		font = UIFont.systemFontOfSize(12)
		textColor = UIColor.blackColor()
		bubbleBackgroundColor = UIColor(white: 237.0/255.0, alpha: 1.0)
		
		let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(TopicsView.handleTap(_:)))
		tapRecognizer.delegate = self
		self.addGestureRecognizer(tapRecognizer)
	}
	
	var bubbleBackgroundColor: UIColor! {
		didSet {
			for topicView in topicViews {
				topicView.backgroundColor = bubbleBackgroundColor
			}
			truncatedTopicView?.backgroundColor = bubbleBackgroundColor
		}
	}
	
	var layoutMode = TopicsViewLayoutMode.Multiline {
		didSet {
			if layoutMode == .SingleLineByTruncatingTail {
				if truncatedTopicView == nil {
					let topicView = TopicBubbleView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 0, height: lineHeight)))
					topicView.font = font
					topicView.textColor = textColor
					topicView.backgroundColor = bubbleBackgroundColor
					topicView.text = "…"
					topicView.bounds.size.width = topicView.intrinsicContentSize().width
					truncatedTopicView = topicView
				}
			} else {
				truncatedTopicView?.removeFromSuperview()
				truncatedTopicView = nil
			}
			self.setNeedsLayout()
			self.invalidateIntrinsicContentSize()
		}
	}
	
	var placeholderText: String? {
		get {
			return placeholderLabel?.text
		}
		set {
			if placeholderLabel == nil {
				placeholderLabel = UILabel(frame: CGRectZero)
				placeholderLabel!.font = UIFont.systemFontOfSize(16)
				placeholderLabel!.textColor = UIColor(white: 209.0/255.0, alpha: 1.0)
				self.insertSubview(placeholderLabel!, atIndex: 0)
			}
			placeholderLabel!.text = newValue
			placeholderLabel!.sizeToFit()
		}
	}
	
	var topics: [Topic] {
		get {
			return topicViews.map { $0.topic! }
		}
		set(newTopics) {
			topicViews.forEach { $0.removeFromSuperview() }
			topicViews.removeAll()
			newTopics.forEach { addTopic($0) }
		}
	}
	
	func addTopic(topic: Topic) {
		let topicView = TopicBubbleView(frame: CGRect(origin: CGPointZero, size: CGSize(width: 0, height: lineHeight)))
		topicView.font = font
		topicView.textColor = textColor
		topicView.backgroundColor = bubbleBackgroundColor
		topicView.topic = topic
		if allowsEditing {
			topicView.deleteButton.addTarget(self, action: #selector(TopicsView.deleteTopic(_:)), forControlEvents: .TouchUpInside)
		}
		topicViews.append(topicView)
		self.addSubview(topicView)
		self.setNeedsLayout()
		self.invalidateIntrinsicContentSize()
	}
	
	override var bounds : CGRect {
		didSet {
			self.setNeedsLayout()
			self.invalidateIntrinsicContentSize()
		}
	}
	
	override func intrinsicContentSize() -> CGSize {
		let height: CGFloat
		if self.layoutMode == .SingleLineByWrapping || self.layoutMode == .SingleLineByTruncatingTail {
			height = lineHeight
		} else {
			if let lastTopicView = topicViews.last {
				layoutTokenViews()
				height = CGRectGetMaxY(lastTopicView.frame)
			} else {
				height = lineHeight
			}
		}
		return CGSizeMake(CGRectGetWidth(self.bounds), height)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if CGRectGetWidth(self.bounds) > 0.0 {
			layoutTokenViews()
		}
		placeholderLabel?.frame.size.height = lineHeight
		placeholderLabel?.hidden = topicViews.count > 0
	}
	
	private func layoutTokenViews() {
		let layoutWidth = CGRectGetWidth(self.bounds)
		var origin = CGPointZero
		var isFirstTopic = true, isFirstLine = true, ellipsisAdded = false
		self.truncatedTopicView?.hidden = true
		for topicView in topicViews {
			var topicViewSize = topicView.intrinsicContentSize()
			
			if origin.x + topicViewSize.width > layoutWidth {
				origin.x = 0
				if !isFirstTopic {
					origin.y = origin.y + lineHeight + lineSpacing
					isFirstLine = false
				}
				topicViewSize.width = min(topicViewSize.width, layoutWidth)
				
				if self.layoutMode == .SingleLineByTruncatingTail && !ellipsisAdded {
					if !isFirstTopic {
						let previousTopicView = topicViews[topicViews.indexOf(topicView)! - 1]
//						if CGRectGetMaxX(previousTopicView.frame) + topicSpacing + CGRectGetWidth(self.truncatedTopicView!.frame) > layoutWidth {
//							previousTopicView.hidden = true
//							self.truncatedTopicView!.frame.origin = previousTopicView.frame.origin
//						} else {
//							self.truncatedTopicView!.frame.origin = CGPoint(x: CGRectGetMaxX(previousTopicView.frame) + topicSpacing, y: 0)
//						}
						previousTopicView.frame.size.width = min(CGRectGetWidth(previousTopicView.frame), layoutWidth - CGRectGetMinX(previousTopicView.frame) - topicSpacing - CGRectGetWidth(self.truncatedTopicView!.frame))
						self.truncatedTopicView!.frame.origin = CGPoint(x: CGRectGetMaxX(previousTopicView.frame) + topicSpacing, y: 0)
						
						self.truncatedTopicView!.hidden = false
						self.addSubview(self.truncatedTopicView!)
					}
					ellipsisAdded = true
				}
			}
			
			topicView.frame = CGRect(origin: origin, size: topicViewSize)
			topicView.hidden = !isFirstLine && (self.layoutMode == .SingleLineByWrapping || self.layoutMode == .SingleLineByTruncatingTail)
			origin.x = origin.x + topicViewSize.width + topicSpacing
			isFirstTopic = false
		}
	}
	
	@objc private func deleteTopic(sender: UIButton) {
		let index = topicViews.indexOf { (topicView) -> Bool in
			topicView.deleteButton == sender
		}
		if let viewIndex = index {
			let deletedTopic = topicViews[viewIndex].topic!
			topicViews[viewIndex].removeFromSuperview()
			topicViews.removeAtIndex(viewIndex)
			deleteHandler?(deletedTopic: deletedTopic)
			self.setNeedsLayout()
			self.invalidateIntrinsicContentSize()
		}
	}
	
	@objc private func handleTap(sender: UITapGestureRecognizer) {
		tapHandler?()
	}
	
	func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
		guard tapHandler != nil else { return false }
		
		let location = touch.locationInView(self)
		for topicView in topicViews {
			if CGRectContainsPoint(topicView.frame, location) {
				return false
			}
		}
		return true
	}
}
