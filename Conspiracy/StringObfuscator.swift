//
//  StringObfuscator.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 17.08.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation


func DemangleString(string: String) -> String {
	let zero = "0".unicodeScalars.first!.value
	let nine = "9".unicodeScalars.first!.value
	let a = "a".unicodeScalars.first!.value
	let A = "A".unicodeScalars.first!.value
	let z = "z".unicodeScalars.first!.value
	let Z = "Z".unicodeScalars.first!.value
	
	var result = ""
	for var scalar in string.unicodeScalars.reverse() {
		if scalar >= "0" && scalar <= "9" {
			scalar = UnicodeScalar(nine - (scalar.value - zero))
		} else
		if scalar >= "a" && scalar <= "z" {
			scalar = UnicodeScalar(Z - (scalar.value - a))
		} else
		if scalar >= "A" && scalar <= "Z" {
			scalar = UnicodeScalar(z - (scalar.value - A))
		} else
		if scalar == "_" || scalar == "." {
			scalar = scalar == "_" ? "." : "_"
		}
		result.append(scalar)
	}
	return result
}
