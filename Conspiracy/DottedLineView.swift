//
//  DottedLineView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 14.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class DottedLineView: UIView {
	private var patternView: UIView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		patternView = UIView(frame: CGRectZero)
		patternView.backgroundColor = DottedLineView.patternColor()
		self.backgroundColor = UIColor.clearColor()
		self.addSubview(patternView)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let patternSize = CGSize(width: 6, height: 2)
		let dotDiameter: CGFloat = 2
		let dotViewWidth = floor((CGRectGetWidth(self.bounds) - 2 * 4.0 - dotDiameter) / patternSize.width) * patternSize.width + dotDiameter
		patternView.frame = CGRect(x: 0.5 * (CGRectGetWidth(self.bounds) - dotViewWidth), y: CGRectGetHeight(self.bounds) - patternSize.height, width: dotViewWidth, height: patternSize.height)
	}
	
	private static var dotPatternColor: UIColor? = nil
	private static var token: dispatch_once_t = 0
	
	class func patternColor() -> UIColor {
		dispatch_once(&token) {
			let dotRadius: CGFloat = 1.5
			
			let patternSize = CGSize(width: 6, height: 2)
			UIGraphicsBeginImageContextWithOptions(patternSize, false, 0)
			UIColor.blackColor().setFill()
			UIBezierPath(ovalInRect: CGRect(x: 0, y: patternSize.height - dotRadius, width: dotRadius, height: dotRadius)).fill()
			dotPatternColor = UIColor(patternImage: UIGraphicsGetImageFromCurrentImageContext())
			UIGraphicsEndImageContext()
		}
		
		return dotPatternColor!
	}
}
