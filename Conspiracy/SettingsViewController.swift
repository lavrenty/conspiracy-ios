//
//  SettingsViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 18.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var logOutButton: UIButton!
	@IBOutlet weak var paymentCardButton: PaymentCardButton!
	@IBOutlet weak var hasUnpaidInvoiceIcon: UIImageView!
	@IBOutlet weak var deletePaymentCardButton: UIButton!
	@IBOutlet weak var editPayoutButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("settings.controller title", comment: "Settings")
		logOutButton.setTitle(NSLocalizedString("settings.button.logout", comment: "Settings"), forState: .Normal)
		deletePaymentCardButton.setTitle(NSLocalizedString("settings.button.delete payment card", comment: "Settings"), forState: .Normal)
		editPayoutButton.setTitle(NSLocalizedString("settings.button.edit payout", comment: "Settings"), forState: .Normal)
		
		self.paymentCardButton.cardInfo = User.currentUser!.paymentCard
		self.hasUnpaidInvoiceIcon.hidden = !User.currentUser!.hasUnpaidInvoice
		self.deletePaymentCardButton.hidden = self.paymentCardButton.cardInfo == nil
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "add-payment-card" {
			let cardEditor = segue.destinationViewController as! PaymentCardEditor
			cardEditor.completion = { (cardInfo) -> Void in
				self.paymentCardButton.cardInfo = cardInfo
				self.deletePaymentCardButton!.hidden = false
			}
		}
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func deletePaymentCard() {
		API.sharedAPI.deletePaymentCard { (error) -> Void in
			if error != nil {
				self.reportError(error)
			} else {
				self.paymentCardButton.cardInfo = nil
				self.deletePaymentCardButton!.hidden = true
			}
		}
	}
	
	@IBAction func showPayoutDetails() {
		let url = NSURL(string: API.sharedAPI.APIBaseURLString + "/v1/billing/setup")!
		let webViewController = WebViewController.navigationController(url, title: NSLocalizedString("payout.controller title", comment: "Payout"), excludedDomains: ["stripe.com"], callback: { (callbackURL) -> Void in
			API.sharedAPI.getCurrentUser(nil)
		})
		self.presentViewController(webViewController, animated: true, completion: nil)
	}
	
	@IBAction func logout() {
		API.sharedAPI.logOut()
		
		let mainWindow = UIApplication.sharedApplication().windows.first!
		let rootViewController = mainWindow.rootViewController! as! DashboardViewController
		rootViewController.showAuthorizationController()
		rootViewController.dismissViewControllerAnimated(true, completion: nil)
	}
}
