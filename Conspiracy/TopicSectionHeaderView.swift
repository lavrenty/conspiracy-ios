//
//  TopicSectionHeaderView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 14.06.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class TopicSectionHeaderView: UIView {
	private var label: TopicBubbleView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		label = TopicBubbleView(frame: self.bounds)
		label.backgroundColor = UIColor(white: 246.0/255.0, alpha: 1.0)
		label.textColor = UIColor(white: 147.0/255.0, alpha: 1.0)
		label.font = UIFont.systemFontOfSize(12)
		self.addSubview(label)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		label.frame = CGRectInset(self.bounds, 10, 0)
	}
	
	func setText(text: String) {
		label.text = text
	}
}
