//
//  TopicPicker.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class TopicPicker: UIViewController {
	var completion: ((topic: Topic) -> Void)?
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var searchBar: UISearchBar!
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var placeholderLabel: UILabel!
	@IBOutlet weak var addTopicButton: UIButton!
	@IBOutlet weak var addTopicButtonBottomConstraint: NSLayoutConstraint!
	var topics = [Topic]()
	var excludedTopics: [Topic]?
	var suggestedTopics: [Topic]?
	var recentTopics: [Topic]?
	var canAddNewTopic = false
	private var showSuggestedTopics = false
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TopicPicker.keyboardWillAppear(_:)), name: UIKeyboardWillShowNotification, object: nil)
	}
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("topic picker.controller title", comment: "Topic picker")
		addTopicButton.setTitle(NSLocalizedString("topic picker.button.add topic", comment: "Topic picker"), forState: .Normal)
		placeholderLabel.text = NSLocalizedString("topic picker.placeholder label", comment: "Topic picker")
		placeholderLabel.hidden = true
		
		if suggestedTopics != nil && excludedTopics != nil {
			suggestedTopics = suggestedTopics!.filter { excludedTopics!.contains($0) == false }
		}
		if recentTopics != nil && excludedTopics != nil {
			recentTopics = recentTopics!.filter { excludedTopics!.contains($0) == false }
		}
		showSuggestedTopics = suggestedTopics != nil
		
		UIGraphicsBeginImageContextWithOptions(CGSize(width: 3, height: 30), false, 0)
		let transparentImage = UIGraphicsGetImageFromCurrentImageContext().resizableImageWithCapInsets(UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1), resizingMode: .Tile)
		UIGraphicsEndImageContext()
		searchBar.setSearchFieldBackgroundImage(transparentImage, forState: .Normal)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		searchBar.becomeFirstResponder()
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		searchBar.resignFirstResponder()
	}
	
	internal func keyboardWillAppear(notification: NSNotification) {
		let keyboardHeight = CGRectGetHeight((notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue())
		tableView.contentInset.bottom = keyboardHeight
		tableView.scrollIndicatorInsets.bottom = keyboardHeight
		addTopicButtonBottomConstraint.constant = 8 + keyboardHeight
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func addTopic(sender: AnyObject) {
		if completion != nil {
			let topicName = searchBar.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
			let newTopic = Topic(name: topicName.capitalizedString)
			completion!(topic: newTopic)
		}
		self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
	}
	
	func search(query: String) {
		API.sharedAPI.getMatchingTopics(query) { (topics, error) -> Void in
			if topics != nil {
				Log("\(topics!)")
				// Exclude self.excludedTopics
				var filteredTopics: [Topic]
				if self.excludedTopics != nil {
					filteredTopics = topics!.filter { self.excludedTopics!.contains($0) == false }
				} else {
					filteredTopics = topics!
				}
				
				self.showSuggestedTopics = filteredTopics.isEmpty && self.suggestedTopics != nil
				self.topics = filteredTopics
				self.placeholderLabel.hidden = !filteredTopics.isEmpty
				self.tableView.hidden = filteredTopics.isEmpty
				self.tableView.reloadData()
				if self.canAddNewTopic {
					self.addTopicButton.hidden = !filteredTopics.isEmpty
				}
			}
		}
	}
}


// MARK: - UISearchBarDelegate


extension TopicPicker: UISearchBarDelegate {
	func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
		NSObject.cancelPreviousPerformRequestsWithTarget(self)
		let query = searchText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
		if query.isEmpty {
			topics = []
			showSuggestedTopics = suggestedTopics != nil
			placeholderLabel.hidden = true
			tableView.hidden = false
			tableView.reloadData()
			addTopicButton.hidden = true
		} else {
			self.performSelector(#selector(TopicPicker.search(_:)), withObject: query, afterDelay: 0.3)
		}
	}
	
	func searchBarSearchButtonClicked(searchBar: UISearchBar) {
		if let searchText = searchBar.text {
			self.searchBar(searchBar, textDidChange: searchText)
		}
	}
}


// MARK: - UITableViewDataSource


extension TopicPicker: UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return showSuggestedTopics ? (recentTopics != nil ? 2 : 1) : 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let displayedTopics = showSuggestedTopics ? (section == 0 && recentTopics != nil ? recentTopics! : suggestedTopics!) : topics
		return displayedTopics.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let displayedTopics = showSuggestedTopics ? (indexPath.section == 0 && recentTopics != nil ? recentTopics! : suggestedTopics!) : topics
		
		let cell = tableView.dequeueReusableCellWithIdentifier("topic", forIndexPath: indexPath)
		cell.textLabel?.font = UIFont.systemFontOfSize(16)
		cell.textLabel?.text = displayedTopics[indexPath.row].name
		return cell
	}
}


// MARK: - UITableViewDelegate


extension TopicPicker: UITableViewDelegate {
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return showSuggestedTopics ? tableView.sectionHeaderHeight : 0
	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if showSuggestedTopics {
			let title = section == 0 && recentTopics != nil ? NSLocalizedString("topic picker.section.recent", comment: "Topic picker") : NSLocalizedString("topic picker.section.top", comment: "Topic picker")
			let sectionHeaderView = TopicSectionHeaderView(frame: CGRectZero)
			sectionHeaderView.setText(title)
			return sectionHeaderView
		} else {
			return nil
		}
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if completion != nil {
			let displayedTopics = showSuggestedTopics ? (indexPath.section == 0 && recentTopics != nil ? recentTopics! : suggestedTopics!) : topics
			completion!(topic: displayedTopics[indexPath.row])
			self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
		}
	}
}
