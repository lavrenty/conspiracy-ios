//
//  UIViewExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 8.04.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


extension UIView {
	func animateRotationWithDuration(duration: NSTimeInterval, clockwise: Bool) {
		// We need 3 steps to ensure 360° rotation
		let oneThird = 1.0 / 3.0
		let options = UIViewKeyframeAnimationOptions(rawValue: UIViewKeyframeAnimationOptions.Repeat.rawValue | UIViewAnimationOptions.CurveLinear.rawValue)
		let angleSign = clockwise ? 1.0 : -1.0
		UIView.animateKeyframesWithDuration(2.0, delay: 0, options: options, animations: { () -> Void in
			UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: oneThird, animations: { () -> Void in
				self.transform = CGAffineTransformMakeRotation(CGFloat(angleSign * M_PI * 2 * oneThird))
			})
			UIView.addKeyframeWithRelativeStartTime(oneThird, relativeDuration: oneThird, animations: { () -> Void in
				self.transform = CGAffineTransformMakeRotation(CGFloat(angleSign * M_PI * 4 * oneThird))
			})
			UIView.addKeyframeWithRelativeStartTime(2 * oneThird, relativeDuration: oneThird, animations: { () -> Void in
				self.transform = CGAffineTransformMakeRotation(CGFloat(angleSign * M_PI * 6 * oneThird))
			})
			}, completion: nil)
	}
}
