//
//  VideoViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import OpenTok
import Crashlytics
import Amplitude_iOS


class VideoViewController: UIViewController {
	@IBOutlet weak var infoContainerView: UIView!
	@IBOutlet weak var muteButton: UIButton!
	@IBOutlet weak var disconnectButton: UIButton!
	@IBOutlet weak var cameraButton: UIButton!
	@IBOutlet weak var inView: UIView!
	@IBOutlet weak var outView: UIView!
	@IBOutlet var outViewAspectRatioConstraint: NSLayoutConstraint!
	@IBOutlet weak var connectingLabel: UILabel!
	@IBOutlet weak var rateLabel: UILabel!
	@IBOutlet weak var durationLabel: UILabel!
	@IBOutlet weak var priceLabel: UILabel!
	@IBOutlet weak var freeToCancelLabel: UILabel!
	@IBOutlet weak var cardContainerView: UIView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var testLiveButton: UIButton!
	
	var card: UserCard!
	var call: Call! // public only for testing
	var session: Session! // public only for testing
	var completion: ((callDidSucceed: Bool) -> Void)?
	var actionDelegate: CardViewActionDelegate?
	var testMode = false
	
	private var otSession: OTSession?
	private var publisher: OTPublisher?
	private var subscriber: OTSubscriber?
	private var webSocket: APIWebSocket?
	private var hasConnected = false
	private var hasHungUp = false
	private var sheetIsUp = true
	private var automaticallyForwardsAppearanceMethods = false
	private var durationFormatter: NSDateComponentsFormatter!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if testMode {
			automaticallyForwardsAppearanceMethods = true
			if call == nil {
				infoContainerView.hidden = true
				addRadarViewController()
			} else {
				initializeUI()
				handleUpdateMessage([
					"method": "update",
					"live": true,
					"rate": call.rate!.cents,
					"duration": 120,
					"cost": call.rate!.cents * 2
				])
			}
			return
		}
		
		#if INTERNAL_TEST_BUILD
		testLiveButton.hidden = false
		#endif
		
		UIApplication.sharedApplication().idleTimerDisabled = true
		if card.user == User.currentUser {
			// Asking a question
			createCard()
		} else {
			// Answering a question
			answerCard()
		}
	}
	
	private func createCard() {
		infoContainerView.hidden = true
		
		connectingLabel.hidden = true
		rateLabel.hidden = true
		durationLabel.hidden = true
		priceLabel.hidden = true
		freeToCancelLabel.hidden = true
		muteButton.hidden = true
		disconnectButton.hidden = true
		cameraButton.hidden = true
		activityIndicator.startAnimating()
		
		API.sharedAPI.createCard(card.text, topics: card.topics, price: card.price!) { (cardIdentifier, error) -> Void in
			self.activityIndicator.stopAnimating()
			guard error == nil else {
				self.reportError(error, actions: [
					UIAlertAction(title: NSLocalizedString("button.retry", comment: "Buttons"), style: .Default, handler: { (_) -> Void in
						self.createCard()
					}),
					UIAlertAction(title: NSLocalizedString("button.cancel", comment: "Buttons"), style: .Cancel, handler: { (_) -> Void in
						self.doDismiss()
					}),
					])
				return
			}
			
			if cardIdentifier != nil {
				Amplitude.instance().logEvent("Card created", withEventProperties: nil)
				
				self.card.identifier = cardIdentifier!
				self.addRadarViewController()
			}
		}
	}
	
	override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
		// Important: depending on how long the network request takes,
		// we may call addRadarViewController() during navigation push animation,
		// missing the opportunity to auto-forward viewWillAppear() to the radar view controller.
		return automaticallyForwardsAppearanceMethods
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		automaticallyForwardsAppearanceMethods = true
	}
	
	private func addRadarViewController() {
		let radarViewController = self.storyboard!.instantiateViewControllerWithIdentifier("radar-vc") as! RadarViewController
		radarViewController.card = card!
		radarViewController.answerCallback = { (call, session) -> Void in
			self.call = call
			self.session = session
			self.initializeUI()
			self.infoContainerView.hidden = false
			self.removeRadarViewController()
		}
		radarViewController.testMode = self.testMode
		
		let forwardAppearanceMethodsManually = !automaticallyForwardsAppearanceMethods
		if forwardAppearanceMethodsManually {
			radarViewController.beginAppearanceTransition(true, animated: false)
		}
		
		radarViewController.view.frame = self.view.bounds
		radarViewController.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		radarViewController.view.translatesAutoresizingMaskIntoConstraints = true
		self.addChildViewController(radarViewController)
		self.view.addSubview(radarViewController.view)
		radarViewController.didMoveToParentViewController(self)
		
		if forwardAppearanceMethodsManually {
			radarViewController.endAppearanceTransition()
		}
	}
	
	private func removeRadarViewController() {
		self.muteButton.hidden = false
		self.disconnectButton.hidden = false
		self.cameraButton.hidden = false
		
		let vc = self.childViewControllers.first!
		
		UIView.animateWithDuration(0.3, animations: { () -> Void in
			vc.view.alpha = 0
			}, completion: { (finished) -> Void in
				vc.willMoveToParentViewController(nil)
				vc.view.removeFromSuperview()
				vc.removeFromParentViewController()
		})
	}
	
	private func answerCard() {
		connectingLabel.hidden = true
		rateLabel.hidden = true
		durationLabel.hidden = true
		priceLabel.hidden = true
		freeToCancelLabel.hidden = true
		muteButton.hidden = true
		disconnectButton.hidden = true
		cameraButton.hidden = true
		activityIndicator.startAnimating()
		
		API.sharedAPI.answer(card) { (call, session, error) -> Void in
			self.activityIndicator.stopAnimating()
			guard error == nil else {
				self.reportError(error, actions: [
					UIAlertAction(title: NSLocalizedString("button.retry", comment: "Buttons"), style: .Default, handler: { (_) -> Void in
						self.answerCard()
					}),
					UIAlertAction(title: NSLocalizedString("button.cancel", comment: "Buttons"), style: .Cancel, handler: { (_) -> Void in
						self.doDismiss()
					}),
					])
				return
			}
			
			Log("\(#function) \(call!), \(session!)")
			Amplitude.instance().logEvent("Card answered", withEventProperties: ["card_id": self.card.identifier])
			
			self.actionDelegate?.answerQuestion(self.card)
			
			self.call = call
			self.session = session
			
			self.muteButton.hidden = false
			self.disconnectButton.hidden = false
			self.cameraButton.hidden = false
			self.initializeUI()
		}
	}
	
	private func initializeUI() {
		assert(call != nil)
		assert(session != nil)
		Log("\(call.isClientSide ? "Asking" : "Answering")")
		
		NSLayoutConstraint(item: self.outView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.GreaterThanOrEqual, toItem: self.view, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 20.0).active = true
		
		durationFormatter = NSDateComponentsFormatter()
		durationFormatter.allowedUnits = [.Hour, .Minute, .Second]
		durationFormatter.unitsStyle = .Positional
		durationFormatter.zeroFormattingBehavior = .None
		
		let callRate = call.rate!
		if callRate.doubleValue != 0 {
			let perMinuteRateString = callRate.formattedValue
			let perHourRateString = (callRate * 60).formattedValue
			rateLabel.text = NSString(format: NSLocalizedString("new card.rate.%@ per minute, %@ per hour", comment: "New Card"), perMinuteRateString, perHourRateString) as String
		} else {
			rateLabel.text = NSLocalizedString("call.label.price is free", comment: "Call")
		}
		priceLabel.text = ""
		freeToCancelLabel.text = NSLocalizedString("call.label.free to cancel", comment: "Call")
		connectingLabel.text = NSLocalizedString("call.label.connecting", comment: "Call")
		
		let cardViewNib = UINib(nibName: "CardView-user", bundle: nil)
		let cardView = cardViewNib.instantiateWithOwner(nil, options: nil).first as! UserCardView
		cardView.card = card
		cardView.setCallParticipants(call.callee!, caller: call.caller!)
		cardView.frame = cardContainerView.bounds
		cardView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		cardContainerView.addSubview(cardView)
		
		// Initial label visibility
		connectingLabel.hidden = false
		rateLabel.hidden = !connectingLabel.hidden
		durationLabel.hidden = true
		priceLabel.hidden = true
		freeToCancelLabel.hidden = true
		
		muteButton.selected = false
		muteButton.setImage(UIImage(named: "btn-unmute"), forState: .Normal)
		
		muteButton.enabled = false
		disconnectButton.enabled = false
		cameraButton.enabled = false
		connect()
	}
	
	private func connect() {
		guard testMode == false else { return }
		guard otSession == nil else {
			Log("Already connected")
			return
		}
		guard session != nil else {
			Log("*** No session set")
			return
		}
		
		let openTokAPIKey = "45425742"
		otSession = OTSession(apiKey: openTokAPIKey, sessionId: session!.identifier, delegate: self)
		
		var error: OTError?
		otSession!.connectWithToken(session!.token, error: &error)
		if error != nil {
			Log("*** connectWithToken: \(error)")
		}
		// see sessionDidConnect
		
		disconnectButton.enabled = true
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		closeWebSocket()
	}
	
	override func preferredStatusBarStyle() -> UIStatusBarStyle {
		return .LightContent
	}
	
	override func prefersStatusBarHidden() -> Bool {
		return !sheetIsUp
	}
	
	override func ignoresPullToDismissGesture() -> Bool {
		return true
	}
	
	// MARK: - IB Actions
	
	@IBAction func testSendLive(sender: UIButton) {
		#if INTERNAL_TEST_BUILD
		webSocket?.call("live", parameters: nil)
		sender.hidden = true
		#endif
	}
	
	@IBAction func toggleSheet(sender: UITapGestureRecognizer) {
		sheetIsUp = !sheetIsUp
		UIView.animateWithDuration(0.3) { () -> Void in
			self.infoContainerView.alpha = self.sheetIsUp ? 1.0 : 0.0
			self.setNeedsStatusBarAppearanceUpdate()
		}
	}
	
	@IBAction func toggleMute(sender: UIButton) {
		if publisher != nil {
			publisher!.publishAudio = !publisher!.publishAudio;
			sender.selected = !sender.selected
			sender.setImage(UIImage(named: sender.selected ? "btn-mute" : "btn-unmute"), forState: .Normal)
			Log("Mic \(publisher!.publishAudio ? "enabled" : "muted")")
		}
	}
	
	@IBAction func toggleCamera(sender: UIButton) {
		if publisher?.cameraPosition == .Front {
			publisher?.cameraPosition = .Back
		} else {
			publisher?.cameraPosition = .Front
		}
	}
	
	@IBAction func disconnect(sender: UIButton) {
		guard webSocket != nil else {
			// The session is not connected yet
			dismiss()
			return
		}
		
		guard webSocket!.isOpen else {
			// Cannot send 'hangup' message
			dismiss()
			return
		}
		
		muteButton.enabled = false
		disconnectButton.enabled = false
		cameraButton.enabled = false
		
		webSocket!.call("hangup", parameters: nil)
		// Wait for the "hangup" signal
	}
	
	private func openWebSocket() {
		guard webSocket == nil else { return }
		guard testMode == false else { return }
		
		webSocket = APIWebSocket(APIEndpoint: "/call/\(call.identifier)")
		webSocket!.addRoute("update", handler: { (message) -> Void in
			self.handleUpdateMessage(message)
		})
		
//		webSocket!.addRoute("error", handler: { (message) -> Void in
//			self.hasHungUp = true
//			self.closeWebSocket()
//			self.disconnect()
//		})
		
		webSocket!.onCloseRoute = {
			Log("Web socket is closed")
			if self.webSocket != nil && !self.hasHungUp {
				Log("Reconnecting...")
				self.webSocket!.open()
			}
		}
	}
	
	private func handleUpdateMessage(message: [String: AnyObject]) {
		self.call.duration = message["duration"] as! Int
		let totalCallCost = Money(cents: message["cost"] as! Int, currency: nil)!
		self.call.totalCost = totalCallCost
		if let isLive = message["live"] as? Bool {
			if isLive {
				self.hasConnected = true
			}
		}
		
		if self.call.rate!.doubleValue == 0 {
			self.freeToCancelLabel.hidden = true
		} else {
			self.freeToCancelLabel.hidden = totalCallCost.cents != 0 || !self.call.isClientSide
		}
		self.connectingLabel.hidden = self.hasConnected
		self.rateLabel.hidden = !self.connectingLabel.hidden || (!self.call.isClientSide && self.call.rate!.doubleValue == 0)
		self.durationLabel.hidden = !self.hasConnected
//		self.priceLabel.hidden = !self.hasConnected
//		self.priceLabel.text = totalCallCost.formattedValue
		
		self.durationLabel.text = self.durationFormatter.stringFromTimeInterval(NSTimeInterval(self.call.duration))
		
		if let otherSideHasHungUp = message["hangup"] as? Bool {
			if otherSideHasHungUp {
				Log("Received \"hangup\" signal")
				self.hasHungUp = true
				self.closeWebSocket()
				self.disconnect()
			}
		}
	}
	
	private func closeWebSocket() {
		webSocket?.close()
		webSocket = nil
	}
	
	private func disconnect() {
		guard otSession != nil else {
			Log("No session to disconnect")
			return
		}
		
		if otSession!.sessionConnectionStatus != .Connected {
			Log("*** Session is not connected: \(otSession!.sessionConnectionStatus.rawValue)")
		}
		
		var error: OTError?
		if subscriber != nil {
			otSession!.unsubscribe(subscriber!, error: &error)
			if error != nil {
				Log("*** unsubscribe: \(error!)")
				error = nil
			}
		}
		if publisher != nil {
			otSession!.unpublish(publisher!, error: &error)
			if error != nil {
				Log("*** unpublish: \(error!)")
				error = nil
			}
		}
		otSession!.disconnect(&error)
		if error != nil {
			Log("*** disconnect: \(error!)")
		}
	}
	
	private func dismiss() {
		if hasConnected {
			self.performSegueWithIdentifier("rate", sender: nil)
		} else {
			self.doDismiss()
		}
	}
	
	private func doDismiss() {
		UIApplication.sharedApplication().idleTimerDisabled = false
		Log("\(#function) calling completion(\(hasConnected))")
		self.completion?(callDidSucceed: hasConnected)
		self.completion = nil
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier! == "rate" {
			let vc = segue.destinationViewController as! RatingViewController
			vc.call = self.call
			vc.completion = {
				self.doDismiss()
			}
		}
	}
}


// MARK: - OTSessionDelegate

extension VideoViewController: OTSessionDelegate {
	func sessionDidConnect(session: OTSession!) {
		Log("\(#function)")
		if publisher == nil {
			publisher = OTPublisher(delegate: self)
			
			muteButton.enabled = true
			cameraButton.enabled = true
			
			var error: OTError?
			session.publish(publisher, error: &error)
			if error != nil {
				Log("*** publish: \(error)")
			}
			
			publisher!.viewScaleBehavior = .Fit
			publisher!.view.translatesAutoresizingMaskIntoConstraints = true
			publisher!.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
			publisher!.view.frame = outView.bounds
			outView.insertSubview(publisher!.view, atIndex: 0)
			// see session(_:streamCreated:)
			// see publisher(_:streamCreated:)
		}
		
		openWebSocket()
	}
	
	func sessionDidDisconnect(session: OTSession!) {
		Log("\(#function)")
		dismiss()
	}
	
	func session(session: OTSession!, didFailWithError error: OTError!) {
		Log("\(#function) \(error)")
		let alert = UIAlertController(title: error.localizedDescription, message: error.localizedFailureReason, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: "Close", style: .Default, handler: { (action) -> Void in
			self.dismiss()
		}))
		self.presentViewController(alert, animated: true, completion: nil)
		
		if error.domain != NSURLErrorDomain {
			Crashlytics.sharedInstance().recordError(error)
		}
	}
	
	func session(session: OTSession!, streamCreated stream: OTStream!) {
		Log("\(#function) \(stream.streamId)")
		if subscriber == nil {
			subscriber = OTSubscriber(stream: stream, delegate: self)
			var error: OTError?
			session.subscribe(subscriber, error: &error)
			if error != nil {
				Log("*** subscribe: \(error)")
			}
			// see subscriberDidConnectToStream
		}
	}
	
	func session(session: OTSession!, streamDestroyed stream: OTStream!) {
		Log("\(#function) \(stream.streamId)")
		if stream == self.subscriber!.stream && self.subscriber != nil {
			Log("\(#function) removing subscriber")
			self.subscriber!.view.removeFromSuperview()
			self.subscriber = nil
		}
	}
}


// MARK: - OTPublisherKitDelegate

extension VideoViewController: OTPublisherKitDelegate {
	func publisher(publisher: OTPublisherKit!, didFailWithError error: OTError!) {
		Log("\(#function) \(error)")
		
		if error.domain != NSURLErrorDomain {
			Crashlytics.sharedInstance().recordError(error)
		}
	}
	
	func publisher(publisher: OTPublisherKit!, streamCreated stream: OTStream!) {
		// To do: video dimensions may be (and actually are) reversed because the video is rotated,
		// but we have no way to query the actual video orientation
		var aspectRatio = stream.videoDimensions.height / stream.videoDimensions.width
		if aspectRatio > 1 { aspectRatio = 1.0/aspectRatio }
		Log("\(#function) \(stream.streamId), dimensions: \(stream.videoDimensions), ratio: \(aspectRatio)")
		self.outViewAspectRatioConstraint.active = false
		self.outViewAspectRatioConstraint = NSLayoutConstraint(item: self.outView, attribute: .Width, relatedBy: .Equal, toItem: self.outView, attribute: .Height, multiplier: aspectRatio, constant: 0)
		self.outViewAspectRatioConstraint.active = true
	}
	
	func publisher(publisher: OTPublisherKit!, streamDestroyed stream: OTStream!) {
		Log("\(#function) \(stream.streamId)")
		if self.publisher != nil {
			Log("\(#function) removing publisher")
			self.publisher!.view.removeFromSuperview()
			self.publisher = nil
		}
	}
}


// MARK: - OTSubscriberKitDelegate

extension VideoViewController: OTSubscriberKitDelegate {
	func subscriberDidConnectToStream(subscriber: OTSubscriberKit!) {
		Log("\(#function)")
		self.subscriber!.view.translatesAutoresizingMaskIntoConstraints = true
		self.subscriber!.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		self.subscriber!.view.frame = inView.bounds
		inView.addSubview(self.subscriber!.view)
		webSocket?.call("live", parameters: nil)
	}
	
	func subscriberDidDisconnectFromStream(subscriber: OTSubscriberKit!) {
		Log("\(#function)")
	}
	
	func subscriber(subscriber: OTSubscriberKit!, didFailWithError error: OTError!) {
		Log("\(#function) \(error)")
		
		if error.domain != NSURLErrorDomain {
			Crashlytics.sharedInstance().recordError(error)
		}
	}
	
	func subscriberVideoDisableWarning(subscriber: OTSubscriberKit!) {
		Log("\(#function) Stream quality degraded")
	}
	
	func subscriberVideoDisableWarningLifted(subscriber: OTSubscriberKit!) {
		Log("\(#function) Stream quality improved")
	}
	
	func subscriberVideoDisabled(subscriber: OTSubscriberKit!, reason: OTSubscriberVideoEventReason) {
		Log("\(#function) \(reason)")
		webSocket?.call("dead", parameters: nil)
	}
	
	func subscriberVideoEnabled(subscriber: OTSubscriberKit!, reason: OTSubscriberVideoEventReason) {
		Log("\(#function) \(reason)")
		webSocket?.call("live", parameters: nil)
	}
}
