//
//  AppDelegate.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import FBSDKCoreKit
import Stripe
//import Security
import Amplitude_iOS


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		#if INTERNAL_TEST_BUILD
			Log("INTERNAL_TEST_BUILD = ON")
			Stripe.setDefaultPublishableKey(DemangleString("rm8Sg8uhyxbJgBSf2egdlOX9.GHVG.PK"))
		#else
			Log("INTERNAL_TEST_BUILD = OFF")
			Stripe.setDefaultPublishableKey(DemangleString("zDTqOyrPj0PP0Ybnhda43kTw.VERO.PK"))
		#endif
		#if !DEBUG && !(arch(i386) || arch(x86_64))
			Fabric.with([Crashlytics.self])
		#else
			#if arch(i386) || arch(x86_64)
				Log("Sandbox: \(NSHomeDirectory())")
			#endif
		#endif
		
		Amplitude.instance().initializeApiKey(DemangleString("UU33409632Z3X32Y2V29022Z5XUYZ697"))
		
		// UIApplication.isRegisteredForRemoteNotifications() returns false after we call unregisterForRemoteNotifications() on logout.
		// In other words, isRegisteredForRemoteNotifications() is basically the same as API.sharedAPI.accessToken != nil.
		// We never reset API.sharedAPI.pushNotificationsDeviceToken so that we know that if it is nil,
		// we haven't asked the user's permission to send push notifications yet.
		if API.sharedAPI.accessToken != nil && API.sharedAPI.pushNotificationsDeviceToken != nil {
			registerForPushNotifications(nil)
		}
		
		return true
	}
	
	func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
		if url.scheme == "conspiracy" {
			NSNotificationCenter.defaultCenter().postNotificationName(OpenURLNotification, object: url)
			return true
		}
		return FBSDKApplicationDelegate.sharedInstance().application(
			application,
			openURL: url,
			sourceApplication: sourceApplication,
			annotation: annotation
		)
	}

	// MARK: - Push Notifications
	
	private var pushNotificationsRegistrationCompletion: ((error: NSError?) -> Void)?
	func registerForPushNotifications(completion: ((error: NSError?) -> Void)?) {
		pushNotificationsRegistrationCompletion = completion
		
//		let noActionGroupCategory = UIMutableUserNotificationCategory()
//		noActionGroupCategory.identifier = "no actions"
		
//		let settings = UIUserNotificationSettings(forTypes: [.Alert], categories: Set([noActionGroupCategory]))
		let settings = UIUserNotificationSettings(forTypes: [.Alert], categories: nil)
		UIApplication.sharedApplication().registerUserNotificationSettings(settings)
		UIApplication.sharedApplication().registerForRemoteNotifications()
	}
	
	func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
		Log("\(#function) \(deviceToken)")
		if API.sharedAPI.pushNotificationsDeviceToken != deviceToken {
			API.sharedAPI.pushNotificationsDeviceToken = deviceToken
			if pushNotificationsRegistrationCompletion == nil {
				Log("\(#function) Uploading new APNS device token")
				API.sharedAPI.registerDeviceForPushNotifications(nil)
			} // else called from API.registerDeviceForPushNotifications(), will call pushNotificationsRegistrationCompletion()
		}
		pushNotificationsRegistrationCompletion?(error: nil)
		pushNotificationsRegistrationCompletion = nil
	}
	
	func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
		Log("\(#function) \(error)")
		pushNotificationsRegistrationCompletion?(error: error)
		pushNotificationsRegistrationCompletion = nil
	}
	
	/*
	Push notification example
{
	"aps": {"content-available": 1},
	"type": "card_live",
	"data": {
		"card": {
			"views": 13,
			"rate": 309,
			"id": 231002,
			"live": true,
			"topic_ids": [6,20,21,40],
			"created": 1471936360,
			"language": "en",
			"text": "One dog rolled before him, well-nigh slashed in half; but a second had him by the thigh, and a third gripped his collar behind.",
			"currency": "usd",
			"type": "card",
			"user_id": 1172
		},
		"users": {
			"1172": {
				"gender": "male",
				"languages": ["en"],
				"name": "Matthew M.",
				"id": 1172,
				"last_name": "M.",
				"picture": "http:\/\/lorempixel.com\/200\/200\/animals\/?7994890808",
				"type": "user",
				"is_verified": true,
				"first_name": "Matthew"
			}
		},
		"topics": {
			"21": {"id": 21, "name": "Physics", "type": "topic"},
			"6": {"id": 6, "name": "MySQL", "type": "topic"},
			"20": {"id": 20, "name": "Fashion and Style", "type": "topic"},
			"40": {"id": 40, "name": "Movies", "type": "topic"}
		}
	}
}
	*/
	func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject: AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
		Log("\(#function)")
		
		application.cancelAllLocalNotifications()
		// An idiotic way to clear the notification center
		application.applicationIconBadgeNumber = 1
		application.applicationIconBadgeNumber = 0
		
		guard application.applicationState != .Active else {
			Log("\(#function) Ignoring push notification in active state")
			completionHandler(.NewData)
			return
		}
		guard let notificationType = userInfo["type"] as? String else {
			Log("\(#function) Unsupported push notification: no \"type\"")
			completionHandler(.NoData)
			return
		}
		guard notificationType == "card_live" else {
			Log("\(#function) Unsupported push notification type: \"\(notificationType)\"")
			completionHandler(.NoData)
			return
		}
		guard let cardData = userInfo["data"] as? [String: AnyObject] else {
			Log("\(#function) Invalid push notification: no \"data\"")
			completionHandler(.NoData)
			return
		}
		
		let card = API.sharedAPI.parseCardObject(cardData["card"] as! [String: AnyObject], json: cardData)
		
		let notification = UILocalNotification()
		if card.price != nil && card.price!.cents > 0 {
			notification.alertTitle = NSString(format: NSLocalizedString("apns.user-card.%@ is asking about %@ for %@", comment: "APNS"), card.user.fullName, card.topics.first!.name, card.price!.formattedValue) as String
		} else {
			notification.alertTitle = NSString(format: NSLocalizedString("apns.user-card.%@ is asking about %@ for free", comment: "APNS"), card.user.fullName, card.topics.first!.name) as String
		}
		notification.alertBody = card.text
		notification.userInfo = cardData
		UIApplication.sharedApplication().presentLocalNotificationNow(notification)
		
		completionHandler(.NewData)
	}
	
	func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
		guard let cardData = notification.userInfo as? [String: AnyObject] else {
			Log("\(#function) Invalid notification: no card data")
			return
		}
		
		Log("\(#function)")
		let card = API.sharedAPI.parseCardObject(cardData["card"] as! [String: AnyObject], json: cardData)
		card.live = true // assume the card is live to avoid sheets disabling the action button
		let vc = self.window!.rootViewController as! DashboardViewController
		vc.openCard(card)
		
		API.sharedAPI.getCard(card.identifier, completion: { (updatedCard, error) in
			if updatedCard != nil {
				card.live = updatedCard!.live
				NSNotificationCenter.defaultCenter().postNotificationName(UserCardDidUpdateNotification, object: card)
			}
		})
	}
	
	func applicationDidBecomeActive(application: UIApplication) {
		application.cancelAllLocalNotifications()
	}
	
	/*
	func logAllKeychainItems() {
		let query: NSMutableDictionary = [kSecReturnAttributes as String: true, kSecMatchLimit as String: kSecMatchLimitAll]
		let secItemClasses = [kSecClassGenericPassword as String, kSecClassInternetPassword as String, kSecClassCertificate as String, kSecClassKey as String, kSecClassIdentity as String]
		for secItemClass in secItemClasses {
			query[kSecClass as String] = secItemClass
			var result: CFTypeRef?
			SecItemCopyMatching(query, &result)
			if result != nil {
				Log("\(secItemClass): \(result!)")
			}
		}
	}
	*/
}
