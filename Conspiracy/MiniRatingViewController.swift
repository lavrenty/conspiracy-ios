//
//  MiniRatingViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 18.08.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class MiniRatingViewController: UIViewController, UIPopoverPresentationControllerDelegate {
	@IBOutlet weak var personRatingControl: RatingControl!
	@IBOutlet weak var submitButton: UIButton!
	private var callID: Int!
	private var completion: ((rating: Int) -> Void)!
	
	convenience init(callID: Int, completion: ((rating: Int) -> Void)) {
		self.init(nibName: "MiniRatingView", bundle: nil)
		self.modalPresentationStyle = .Popover
		self.popoverPresentationController?.delegate = self
		self.callID = callID
		self.completion = completion
	}
	
	deinit {
		Log("\(#function)")
	}
	
	func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
		return .None
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.preferredContentSize = self.view.bounds.size
		submitButton.enabled = false
	}
	
	@IBAction func setRating() {
		submitButton.enabled = personRatingControl.rating > 0
	}
	
	@IBAction func submitRating() {
		self.view.userInteractionEnabled = false
		API.sharedAPI.rateCall(callID, userRating: personRatingControl.rating, connectionQuality: 0, completion: { (error) -> Void in
			if error != nil {
				self.view.userInteractionEnabled = true
				self.reportError(error)
			} else {
				self.completion(rating: self.personRatingControl.rating)
			}
		})
	}
}
