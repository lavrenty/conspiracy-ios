//
//  LanguagePicker.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 5.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class LanguageSection {
	var sectionTitle: String = ""
	var languages: [Language] = []
}


class Language {
	var ISOLanguageCode: String = ""
	var languageName: String = ""
	var selected = false
}


class LanguagePicker: UIViewController {
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	private var languages: [LanguageSection]!
	var selectedLanguages: [String]?
	var completion: ((selectedLanguages: [String]) -> Void)?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("language picker.controller title", comment: "Language picker")
		
		let locale = NSLocale.currentLocale()
		var sectionLookupTable = [String: LanguageSection]()
		
		for languageCode in NSLocale.ISOLanguageCodes() {
			if languageCode.characters.count == 2 {
				if let languageName = locale.displayNameForKey(NSLocaleLanguageCode, value: languageCode) {
					let language = Language()
					language.ISOLanguageCode = languageCode
					language.languageName = languageName
					
					let sectionTitle: String
					if selectedLanguages != nil && selectedLanguages!.contains(languageCode) {
						sectionTitle = ""
						language.selected = true
					} else {
						sectionTitle = languageName.substringToIndex(languageName.startIndex.advancedBy(1))
					}
					var section = sectionLookupTable[sectionTitle]
					if section == nil {
						section = LanguageSection()
						section!.sectionTitle = sectionTitle
						sectionLookupTable[sectionTitle] = section!
					}
					section!.languages.append(language)
				}
			}
		}
		for section in sectionLookupTable.values {
			section.languages.sortInPlace({ (one, two) -> Bool in
				return one.languageName < two.languageName
			})
		}
		languages = sectionLookupTable.values.sort({ (one, two) -> Bool in
			return one.sectionTitle < two.sectionTitle
		})
	}
	
	@IBAction func close() {
		if completion != nil {
			var theSelectedLanguages = [String]()
			for section in languages {
				for language in section.languages {
					if language.selected {
						theSelectedLanguages.append(language.ISOLanguageCode)
					}
				}
			}
			completion!(selectedLanguages: theSelectedLanguages)
		}
		
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
}


// MARK: - UITableViewDataSource


extension LanguagePicker {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return languages.count
	}
	
	func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
		return languages.map { $0.sectionTitle }
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		let sectionTitle = languages[section].sectionTitle
		return sectionTitle.isEmpty ? nil : sectionTitle
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return languages[section].languages.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let language = languages[indexPath.section].languages[indexPath.row]
		let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
		cell.textLabel?.text = language.languageName
		cell.accessoryType = language.selected ? .Checkmark : .None
		return cell
	}
}


// MARK: - UITableViewDelegate


extension LanguagePicker {
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let language = languages[indexPath.section].languages[indexPath.row]
		language.selected = !language.selected
		let cell = tableView.cellForRowAtIndexPath(indexPath)
		cell!.accessoryType = language.selected ? .Checkmark : .None
		
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
	}
}
