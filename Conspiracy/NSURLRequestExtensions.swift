//
//  NSURLRequestExtensions.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 10.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


extension NSURLRequest {
	var cURLCommand: String {
		get {
			let commandString = NSMutableString(string: "curl -k -i ")
			if let method = self.HTTPMethod {
				commandString.appendFormat("-X \(method) ")
			}
			
			if let bodyData = self.HTTPBody {
				if let bodyString = NSString(data: bodyData, encoding: NSUTF8StringEncoding) {
					let sanitizedBodyString = bodyString.stringByReplacingOccurrencesOfString("\\/", withString: "/")
					commandString.appendFormat("-d \'\(sanitizedBodyString)\' ")
				}
			}
			
			if let headerFields = self.allHTTPHeaderFields {
				for (key, value) in headerFields {
					commandString.appendFormat("-H \"\(key): \(value)\" ")
				}
			}
			
			commandString.appendFormat("\"\(self.URL!.absoluteString)\"")
			return commandString as String
		}
	}
}
