//
//  Session.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


struct Session: CustomStringConvertible {
	var identifier: String
	var token: String
	
	var description: String {
		return "<\(self.dynamicType) id=\(identifier)>"
	}
}
