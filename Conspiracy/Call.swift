//
//  Call.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


class Call: APIEntity {
	var caller: User?
	var callee: User?
	var rate: Money?
	var duration: Int = 0
	var totalCost: Money?
	var card: UserCard?
	var isClientSide: Bool { // whether the question is asked by the current user (the current user is the client)
		get {
			return callee == nil || callee == User.currentUser
		}
	}
	
	#if DEBUG
	override var description: String {
		return "<\(self.dynamicType) id=\(identifier) from \(caller != nil ? caller!.description : "NONE") to \(callee != nil ? callee!.description : "NONE")>"
	}
	#endif
}
