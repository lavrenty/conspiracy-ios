//
//  WebViewController.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 14.03.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit
import WebKit


class WebViewController: UIViewController, WKNavigationDelegate {
	class func navigationController(URL: NSURL, title: String?, excludedDomains: [String]?, callback: ((NSURL?) -> Void)?) -> UINavigationController {
		let webViewController = WebViewController(nibName: nil, bundle: nil)
		webViewController.url = URL
		webViewController.title = title
		webViewController.exclusionDomains = excludedDomains
		webViewController.callback = callback
		
		let navController = UINavigationController(rootViewController: webViewController)
		navController.navigationBar.tintColor = UIColor.blackColor()
//		navController.toolbarHidden = false
		return navController
	}
	
	private var url: NSURL!
	private var baseHostName: String!
	private var webView: WKWebView!
	private var progressView: UIProgressView!
	private var backBarButtonItem: UIBarButtonItem!
	private var forwardBarButtonItem: UIBarButtonItem!
	private var kvoContextTitle: UInt8 = 1
	private var kvoContextProgress: UInt8 = 2
	private var titlePreset = false
	private var shouldDismissOnEnteringBackground = false
	private var dismissed = false
	private var exclusionDomains: [String]?
	private var callback: ((NSURL?) -> Void)?
	private var callbackURL: NSURL?
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WebViewController.applicationDidEnterBackground), name: UIApplicationDidEnterBackgroundNotification, object: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	deinit {
		if !titlePreset {
			webView.removeObserver(self, forKeyPath: "title")
		}
		webView.removeObserver(self, forKeyPath: "estimatedProgress")
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav-btn-close"), style: .Plain, target: self, action: #selector(WebViewController.close))
		titlePreset = self.title != nil
		
		backBarButtonItem = UIBarButtonItem(image: UIImage(named: "arrow-back"), style: .Plain, target: self, action: #selector(WebViewController.goBack))
		forwardBarButtonItem = UIBarButtonItem(image: UIImage(named: "arrow-forward"), style: .Plain, target: self, action: #selector(WebViewController.goForward))
		self.toolbarItems = [
			backBarButtonItem,
			UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil),
			forwardBarButtonItem
		]
		
		let configuration = WKWebViewConfiguration()
		webView = WKWebView(frame: self.view.bounds, configuration: configuration)
		webView.navigationDelegate = self
		webView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		webView.translatesAutoresizingMaskIntoConstraints = true
		if !titlePreset {
			webView.addObserver(self, forKeyPath: "title", options: [], context: &kvoContextTitle)
		}
		webView.addObserver(self, forKeyPath: "estimatedProgress", options: [], context: &kvoContextProgress)
		self.view.addSubview(webView)
		
		progressView = UIProgressView(progressViewStyle: .Default)
		progressView.trackTintColor = UIColor.clearColor()
		progressView.progressTintColor = UIColor.brandBlueColor()
		progressView.translatesAutoresizingMaskIntoConstraints = false
		self.view.addSubview(progressView)
		NSLayoutConstraint.activateConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[progress]|", options: [], metrics: nil, views: ["progress": progressView]))
		NSLayoutConstraint.activateConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[guide][progress]", options: [], metrics: nil, views: ["progress": progressView, "guide": self.topLayoutGuide]))
		
		// Get the host name with subdomains stripped
		let hostName = self.url.host!
		let hostNameComponents = hostName.componentsSeparatedByString(".")
		if hostNameComponents.count <= 2 {
			baseHostName = hostName
		} else {
			baseHostName = hostNameComponents[hostNameComponents.count - 2] + "." + hostNameComponents[hostNameComponents.count - 1]
		}
		baseHostName = baseHostName.lowercaseString
		
		let request = NSMutableURLRequest(URL: self.url, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 30)
		if let accessToken = API.sharedAPI.accessToken {
			request.setValue("BEARER \(accessToken)", forHTTPHeaderField: "Authorization")
		}
		webView.loadRequest(request)
	}
	
	override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
		if context == &kvoContextTitle {
			self.title = webView.title
		} else
		if context == &kvoContextProgress {
			progressView.progress = Float(webView.estimatedProgress)
			progressView.hidden = progressView.progress >= 1
		}
	}
	
	internal func goBack() {
		webView.goBack()
	}
	
	internal func goForward() {
		webView.goForward()
	}
	
	private func validateNavigationButtons() {
		backBarButtonItem.enabled = webView.canGoBack;
		forwardBarButtonItem.enabled = webView.canGoForward;
	}
	
	func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
		if let requestedURL = navigationAction.request.URL {
			let scheme = requestedURL.scheme.lowercaseString
			if scheme == "conspiracy" {
				// Callbacks
				decisionHandler(.Cancel)
				Log("Callback: \(requestedURL)")
				callbackURL = requestedURL
				close()
				return
			}
			if let hostName = requestedURL.host {
				if (scheme == "http" || scheme == "https") && !hostName.lowercaseString.hasSuffix(self.baseHostName) {
					// External URL
					if exclusionDomains != nil {
						for domainName in exclusionDomains! {
							if requestedURL.host!.hasSuffix(domainName) {
								Log("Allowed URL: \(requestedURL)")
								decisionHandler(.Allow)
								return
							}
						}
					}
					Log("External URL: \(requestedURL)")
					shouldDismissOnEnteringBackground = true
					UIApplication.sharedApplication().openURL(requestedURL)
					decisionHandler(.Cancel)
					return
				}
			}
			Log("Allowed URL: \(requestedURL)")
		}
		
		decisionHandler(.Allow)
	}
	
	func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
		validateNavigationButtons()
	}
	
	func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
		validateNavigationButtons()
	}
	
	func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
		self.reportError(error)
		progressView.hidden = true
	}
	
	func close() {
		if !dismissed {
			dismissed = true
			callback?(callbackURL)
			self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
		}
	}
	
	func applicationDidEnterBackground(notification: NSNotification) {
		if shouldDismissOnEnteringBackground {
			close()
		}
	}
}
