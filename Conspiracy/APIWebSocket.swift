//
//  APIWebSocket.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 28.03.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import Foundation
import SwiftWebSocket


class APIWebSocket: NSObject {
	private var endpoint: String
	private var webSocket: WebSocket!
	private var backgroundTaskID = UIBackgroundTaskInvalid
	private var shouldReopenInForeground = false
	private var routes: [String: ([String: AnyObject]) -> Void] = [:]
	private var webSocketIsOpen = false
	var onOpenRoute: ((Void) -> Void)?
	var onErrorRoute: ((ErrorType) -> Void)?
	var onCloseRoute: ((Void) -> Void)?
	private var pingTimer: NSTimer?
	var pingEnabled = false
	
	init(APIEndpoint: String) {
		endpoint = APIEndpoint
		super.init()
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(APIWebSocket.willMoveToForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
		open()
	}
	
	func open() {
		guard webSocket == nil else { return }
		
		let url = NSURL(string: API.sharedAPI.APIWebSocketURLString + "/v1" + endpoint)!
		let request = NSMutableURLRequest(URL: url)
		let accessToken = API.sharedAPI.accessToken
		assert(accessToken != nil, "Request \(endpoint) is not authorized")
		request.setValue("BEARER \(accessToken!)", forHTTPHeaderField: "Authorization")
		request.setValue("WebSocket", forHTTPHeaderField: "Upgrade")
		request.setValue("Upgrade", forHTTPHeaderField: "Connection")
		request.setValue(API.sharedAPI.userAgentString, forHTTPHeaderField: "User-Agent")
		
		webSocket = WebSocket(request: request)
		webSocket.eventQueue = API.sharedAPI.eventQueue
		webSocket.event.open = {
			self.webSocketIsOpen = true
			Log("\(self) is opened")
			
			if self.onOpenRoute != nil {
				dispatch_async(dispatch_get_main_queue(), {
					self.onOpenRoute?()
				})
			}
		}
		webSocket.event.close = { code, reason, clean in
			Log("\(self) is closed: \(reason) (\(code))")
			
			if let handler = self.onCloseRoute {
				dispatch_async(dispatch_get_main_queue(), {
					self.webSocketIsOpen = false
					self.webSocket = nil
					handler()
				})
			}
		}
		webSocket.event.error = { error in
			Log("\(self) *** Error: \(error)")
			
			if let handler = self.onErrorRoute {
				dispatch_async(dispatch_get_main_queue(), { handler(error) })
			}
		}
		webSocket.event.message = { message in
			let messageData: NSData?
			if message is NSString {
//				Log("\(self) received \(message.dynamicType): \((message as! NSString).stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()))")
				messageData = (message as! NSString).dataUsingEncoding(NSUTF8StringEncoding)
			} else
				if message is NSData {
//					Log("\(self) received \(message.dynamicType): \(message)")
					messageData = message as? NSData
				} else {
					Log("*** Unsupported message type: \(message.dynamicType)")
					return
			}
			guard messageData != nil else {
				Log("*** Failed to parse message: \(message)")
				return
			}
			guard let dictionary = JSON(messageData) else {
				Log("*** Not JSON: \(message)")
				return
			}
			guard let json = dictionary as? [String: AnyObject] else {
				Log("*** Not a valid JSON: \(message)")
				return
			}
			
			guard let method = json["method"] else {
				Log("*** no method specified: \(message)")
				return
			}
//			Log("\(self) received \"\(method)\"")
			if let handler = self.routes[method as! String] {
				dispatch_async(dispatch_get_main_queue(), {
					handler(json)
				})
			} else {
				Log("\(self) *** no route for: \(message)")
			}
		}
		
		self.pingTimer?.invalidate()
		self.pingTimer = self.pingEnabled ? NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(APIWebSocket.ping), userInfo: nil, repeats: true) : nil
		
		backgroundTaskID = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
			assert(NSThread.isMainThread())
			#if DEBUG
				Log("Expiration handler called")
				usleep(50)
			#endif
			self.shouldReopenInForeground = true
			self.close()
		}
	}
	
	func close() {
		Log("\(self) \(#function)")
		self.webSocketIsOpen = false
		if webSocket != nil {
			webSocket.event.close = {(code, reason, wasClean) in }
			webSocket.event.error = {(error) in }
			webSocket.close()
			webSocket = nil
		}
		
		self.pingTimer?.invalidate()
		self.pingTimer = nil
		
		if backgroundTaskID != UIBackgroundTaskInvalid {
			UIApplication.sharedApplication().endBackgroundTask(backgroundTaskID)
			backgroundTaskID = UIBackgroundTaskInvalid
		}
	}
	
	func call(method: String, parameters: [String: AnyObject]?) {
		if webSocket != nil {
			var json = ["method": method] as [String: AnyObject]
			if let unwrappedParameters = parameters {
				for (key, value) in unwrappedParameters {
					json[key] = value
				}
			}
			do {
				let jsonData = try NSJSONSerialization.dataWithJSONObject(json, options: [])
				webSocket.send(jsonData)
			} catch let error as NSError {
				fatalError("Invalid JSON: \(json.description)\n  (\(error))")
			}
		}
	}
	
	func addRoute(method: String, handler: ((message: [String: AnyObject]) -> Void)) {
		assert(routes[method] == nil, "Route \(method) already exists")
		routes[method] = handler
	}
	
	func removeRoute(method: String) {
		routes[method] = nil
	}
	
	var isOpen: Bool {
		get {
			return webSocketIsOpen
		}
	}
	
	internal func ping() {
		webSocket?.ping()
	}
	
	internal func willMoveToForeground(notification: NSNotification) {
		if shouldReopenInForeground {
			shouldReopenInForeground = false
			Log("\(self) re-opening in foreground…")
			open()
		}
	}
	
	deinit {
		Log("\(self) deinit")
		if webSocket != nil {
			close()
		}
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override var description: String {
		return "<\(self.dynamicType) \(endpoint)>"
	}
}
