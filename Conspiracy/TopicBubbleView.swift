//
//  TopicBubbleView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 13.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


@IBDesignable
class TopicBubbleView: UILabel {
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		super.backgroundColor = UIColor.clearColor()
		self.opaque = false
		
//		self.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
//		self.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
	}
	
	var topic: Topic? {
		didSet {
			self.text = topic?.name.uppercaseString
			self.invalidateIntrinsicContentSize()
		}
	}
	
	private var _deleteButton: UIButton?
	var deleteButton: UIButton {
		get {
			if _deleteButton == nil {
				let button = UIButton(type: .Custom)
				button.setImage(UIImage(named: "topic-delete"), forState: .Normal)
				button.sizeToFit()
				_deleteButton = button
				self.addSubview(button)
				self.invalidateIntrinsicContentSize()
			}
			self.userInteractionEnabled = true
			return _deleteButton!
		}
	}
	
	private var shapeBackgroundColor: UIColor?
	override var backgroundColor: UIColor? {
		get {
			return shapeBackgroundColor
		}
		set {
			shapeBackgroundColor = newValue
			self.setNeedsDisplay()
		}
	}
	
	@IBInspectable var highlightedBackgroundColor: UIColor? {
		didSet {
			self.setNeedsDisplay()
		}
	}
	
	override func intrinsicContentSize() -> CGSize {
		let shapeHeight = CGRectGetHeight(self.bounds)
		let textLabelWidth = super.intrinsicContentSize().width
		let buttonWidthAdjustment = _deleteButton != nil ? 7.0 + 0.5 * shapeHeight : 0.0
		
		return CGSizeMake(textLabelWidth + shapeHeight + buttonWidthAdjustment, shapeHeight)
	}
	
	override func drawRect(rect: CGRect) {
		let shapeRadius = 0.5 * CGRectGetHeight(self.bounds)
		let buttonWidthAdjustment = _deleteButton != nil ? 7.0 + shapeRadius : 0.0
		
		let bgColor = self.highlighted && highlightedBackgroundColor != nil ? highlightedBackgroundColor : shapeBackgroundColor
		bgColor?.setFill()
		UIBezierPath(roundedRect: self.bounds, cornerRadius: shapeRadius).fill()
		
		super.drawTextInRect(UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsets(top: 0, left: shapeRadius, bottom: 0, right: shapeRadius + buttonWidthAdjustment)))
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if _deleteButton != nil {
			let deleteButtonWidth = CGRectGetHeight(self.bounds) + 7.0
			_deleteButton!.frame = CGRectMake(CGRectGetWidth(self.bounds) - deleteButtonWidth, 0, deleteButtonWidth, CGRectGetHeight(self.bounds))
		}
	}
}
