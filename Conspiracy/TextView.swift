//
//  TextView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 14.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


class TextView: UITextView {
	private var placeholderLabel: UILabel!
	
	override init(frame: CGRect, textContainer: NSTextContainer?) {
		super.init(frame: frame, textContainer: textContainer)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		placeholderLabel = UILabel(frame: CGRectZero)
		placeholderLabel.font = self.font
		placeholderLabel.textColor = UIColor(white: 209.0/255.0, alpha: 1.0)
		self.insertSubview(placeholderLabel, atIndex: 0)
		
		self.textContainer.lineFragmentPadding = 0
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UITextInputDelegate.textDidChange(_:)), name: UITextViewTextDidChangeNotification, object: self)
	}
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let inset = self.textContainerInset
		let padding = self.textContainer.lineFragmentPadding
		placeholderLabel.frame.origin = CGPoint(x: inset.left + padding, y: 0)
		placeholderLabel.frame.size.height = CGRectGetHeight(self.bounds)
	}
	
	internal func textDidChange(notification: NSNotification?) {
		self.updatePlaceholderVisibility()
	}
	
	var placeholderText: String? {
		get {
			return placeholderLabel.text
		}
		set {
			placeholderLabel.text = newValue
			placeholderLabel.sizeToFit()
			self.setNeedsLayout()
		}
	}
	
	override var text: String! {
		didSet {
			self.textDidChange(nil)
		}
	}
	
	override func becomeFirstResponder() -> Bool {
		let result = super.becomeFirstResponder()
		if result {
			self.updatePlaceholderVisibility()
		}
		return result
	}
	
	override func resignFirstResponder() -> Bool {
		let result = super.resignFirstResponder()
		if result {
			self.updatePlaceholderVisibility()
		}
		return result
	}
	
	private func updatePlaceholderVisibility() {
		placeholderLabel.hidden = self.isFirstResponder() || !(self.text == nil || self.text!.isEmpty)
	}
}
