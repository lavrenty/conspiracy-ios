//
//  DashboardView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 2.02.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit


enum DashboardTrackingMode {
	case Undetermined, DraggingCard
}


class DashboardView: UIView, UIGestureRecognizerDelegate {
	var slotViews: [CardSlotView]!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupInternals()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
		slotViews = Array()
		
		let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(DashboardView.handleCardPanGesture(_:)))
		panRecognizer.delegate = self
		self.addGestureRecognizer(panRecognizer)
		
		animator = UIDynamicAnimator(referenceView: self)
	}
	
	var spacing: CGFloat = 4 {
		didSet {
			self.setNeedsLayout()
		}
	}
	
	func addArrangedSubview(slotView: CardSlotView) {
		slotViews.append(slotView)
		self.addSubview(slotView)
	}
	
	var arrangedSubviews: [CardSlotView] {
		get {
			return slotViews
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let cardCount = CGFloat(slotViews.count)
		let cardHeight = floor((CGRectGetHeight(self.bounds) - (cardCount - 1) * spacing) / cardCount)
		let cardSpacing = (CGRectGetHeight(self.bounds) - cardCount * cardHeight) / (cardCount - 1)
		
		var cardCenterOffsetY = cardHeight * 0.5
		for slotView in slotViews {
			slotView.bounds = CGRect(x: 0, y: 0, width: CGRectGetWidth(self.bounds), height: cardHeight)
			slotView.center = CGPoint(x: CGRectGetMidX(self.bounds), y: cardCenterOffsetY)
			
			cardCenterOffsetY = cardCenterOffsetY + cardHeight + cardSpacing
		}
	}
	
	private var trackingMode = DashboardTrackingMode.Undetermined
	private var touchDownLocation: CGPoint!
	private var trackedSlotView: CardSlotView?
	private var draggedCardView: CardView?
	private var draggedCardViewSnapshot: UIView?
	private var animator: UIDynamicAnimator!
	private var itemBehavior: UIDynamicItemBehavior!
	private var attachmentBehavior: UIAttachmentBehavior!
	private var snapBehavior: UISnapBehavior!
	
	override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
		return trackingMode == .Undetermined && draggedCardViewSnapshot == nil
	}
	
	internal func handleCardPanGesture(panRecognizer: UIPanGestureRecognizer) {
		switch(panRecognizer.state) {
			case .Began:
				assert(draggedCardViewSnapshot == nil)
//				assert(trackedSlotView == nil)
				assert(draggedCardView == nil)
				assert(snapBehavior == nil)
				
				trackingMode = .Undetermined
				touchDownLocation = panRecognizer.locationInView(self)
				
				trackedSlotView = slotViewAtPoint(touchDownLocation)
				draggedCardView = trackedSlotView?.cardView
				break
			
			case .Changed:
				if trackingMode == .Undetermined {
					let translation = panRecognizer.translationInView(self)
					if abs(translation.x) > 3 || abs(translation.y) > 3 {
						if draggedCardView != nil {
							trackingMode = .DraggingCard
							
							draggedCardViewSnapshot = draggedCardView!.snapshotViewAfterScreenUpdates(true)
							draggedCardViewSnapshot!.frame = draggedCardView!.convertRect(draggedCardView!.bounds, toView: self)
							self.addSubview(draggedCardViewSnapshot!)
							draggedCardView!.hidden = true
							
							itemBehavior = UIDynamicItemBehavior(items: [draggedCardViewSnapshot!])
							animator.addBehavior(itemBehavior)
							
							let pointInCard = draggedCardViewSnapshot!.convertPoint(touchDownLocation, fromView: self)
							let offsetFromCenter = UIOffset(horizontal: pointInCard.x - CGRectGetMidX(draggedCardViewSnapshot!.bounds), vertical: pointInCard.y - CGRectGetMidY(draggedCardViewSnapshot!.bounds))
							attachmentBehavior = UIAttachmentBehavior(item: draggedCardViewSnapshot!, offsetFromCenter: offsetFromCenter, attachedToAnchor: touchDownLocation)
							animator.addBehavior(attachmentBehavior)
						}
					}
				}
				
				if trackingMode == .DraggingCard {
					attachmentBehavior.anchorPoint = panRecognizer.locationInView(self)
				}
				break
			
			case .Ended:
				if trackingMode == .DraggingCard {
					animator.removeBehavior(attachmentBehavior)
					attachmentBehavior = nil
					
					let pythagoreanSqrt = { (x: CGFloat, y: CGFloat) -> CGFloat in return sqrt(x * x + y * y) }
					
					let velocity = panRecognizer.velocityInView(self)
					let absoluteVelocity = pythagoreanSqrt(velocity.x, velocity.y)
					if absoluteVelocity > 1000 {
						itemBehavior.addLinearVelocity(velocity, forItem: draggedCardViewSnapshot!)
						itemBehavior.action = {
							if !CGRectIntersectsRect(self.draggedCardViewSnapshot!.frame, self.bounds) {
								self.cleanUpAnimations(false)
							}
						}
					} else {
						snapDraggedCardViewBack()
					}
				}
				break
			
			default: // cancelled
				Log("Gesture cancelled")
				if trackingMode == .DraggingCard {
					snapDraggedCardViewBack()
				}
				break
		}
	}
	
	private func slotViewAtPoint(point: CGPoint) -> CardSlotView {
		var theSlotView = slotViews.first!
		for slotView in slotViews {
			if point.y < CGRectGetMinY(slotView.frame) {
				return theSlotView
			}
			if CGRectContainsPoint(slotView.frame, point) {
				return slotView
			}
			theSlotView = slotView
		}
		return theSlotView
	}
	
	private func snapDraggedCardViewBack() {
		snapBehavior = UISnapBehavior(item: draggedCardViewSnapshot!, snapToPoint: trackedSlotView!.center)
		snapBehavior.damping = 0.7
		snapBehavior.action = {
			if CGAffineTransformIsIdentity(self.draggedCardViewSnapshot!.transform) {
				self.cleanUpAnimations(true)
			}
		}
		animator.addBehavior(snapBehavior)
	}
	
	private func cleanUpAnimations(cancelled: Bool) {
		animator.removeAllBehaviors()
		itemBehavior = nil
		attachmentBehavior = nil
		snapBehavior = nil
		
		draggedCardViewSnapshot!.removeFromSuperview()
		draggedCardViewSnapshot = nil
		if cancelled {
			draggedCardView!.hidden = false
		} else {
			if trackedSlotView != nil {
				trackedSlotView!.swipeHandler(direction: .Right)
			}
		}
		
		trackedSlotView = nil
		draggedCardView = nil
		trackingMode = .Undetermined
	}
}


// MARK: -


class CardSlotView: UIView {
	var actionDelegate: CardViewActionDelegate!
	
	init(frame: CGRect, actionDelegate: CardViewActionDelegate) {
		self.actionDelegate = actionDelegate
		super.init(frame: frame)
		self.setupInternals()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupInternals()
	}
	
	private func setupInternals() {
//		panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(DashboardView.handleCardPanGesture(_:)))
//		panRecognizer.enabled = false
//		self.addGestureRecognizer(panRecognizer)
	}
	
//	internal func handleCardPanGesture(sender: UIPanGestureRecognizer) {
//		guard cardView != nil else { return }
//		
//		switch(panRecognizer.state) {
//			case .Began:
//				break
//			
//			case .Changed:
//				cardView!.transform = CGAffineTransformMakeTranslation(panRecognizer.translationInView(self).x, 0)
//				break
//			
//			case .Ended:
//				let translation = panRecognizer.translationInView(self).x
//				let velocity = panRecognizer.velocityInView(self).x
//				if (translation > 0 && velocity >= 0) || (translation < 0 && velocity <= 0) {
//					animateCard(velocity, cancelled: false)
//				} else {
//					animateCard(velocity, cancelled: true)
//				}
//				break
//			
//			default: // cancelled
//				animateCard(0, cancelled: true)
//				break
//		}
//	}
	
//	private func animateCard(velocity: CGFloat, cancelled: Bool) {
//		let currentTranslation = cardView!.transform.tx
//		var finalTranslation: CGFloat = 0
//		var toLeft = false
//		if !cancelled {
//			if currentTranslation != 0 {
//				toLeft = currentTranslation < 0
//			} else {
//				toLeft = velocity < 0
//			}
//			finalTranslation = toLeft ? -CGRectGetWidth(self.bounds) : CGRectGetWidth(self.bounds)
//		}
//		
//		var duration = 0.3
//		var options = UIViewAnimationOptions.CurveEaseInOut
//		if velocity != 0 {
//			options = UIViewAnimationOptions.CurveEaseOut
//			duration = min(Double(abs(finalTranslation - currentTranslation)/abs(velocity)), 0.3)
//		}
////		Log("x=\(currentTranslation) => \(finalTranslation), v=\(velocity)")
//		
//		self.userInteractionEnabled = false
//		UIView.animateWithDuration(duration, delay: 0, options: options, animations: { () -> Void in
//			self.cardView!.transform = CGAffineTransformMakeTranslation(finalTranslation, 0)
//			}) { (finished) -> Void in
//				self.userInteractionEnabled = true
//				if !cancelled {
//					self.swipeHandler(direction: toLeft ? .Left : .Right)
//				}
//		}
//	}
	
//	private var panRecognizer: UIPanGestureRecognizer!
	private(set) var cardView: CardView?
	var card: Card? {
		get {
			return cardView != nil ? cardView!.card : nil
		}
		set(newCard) {
			guard newCard != card else { return }
			
			let newCardView: CardView?
			if newCard != nil {
				newCardView = CardView.cardView(newCard!)
				if newCardView != nil {
					newCardView!.actionDelegate = actionDelegate
					newCardView!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
					newCardView!.frame = self.bounds
					newCardView!.translatesAutoresizingMaskIntoConstraints = true
				}
			} else {
				newCardView = nil
			}
			
			self.userInteractionEnabled = false
			let oldCardView = cardView
			UIView.transitionWithView(self, duration: 0.2, options: [.BeginFromCurrentState, .TransitionFlipFromTop],
				animations: { () -> Void in
					if oldCardView != nil {
						oldCardView!.removeFromSuperview()
					}
					if newCardView != nil {
						self.addSubview(newCardView!)
					}
				},
				completion: { (finished) -> Void in
					if newCardView != nil {
						self.userInteractionEnabled = true
					}
			})
			
			cardView = newCardView
//			panRecognizer.enabled = cardView != nil
		}
	}
	
	var swipeHandler: ((direction: UISwipeGestureRecognizerDirection) -> Void)!
	
	func updateCardView(message: [String: AnyObject]) {
		precondition(cardView != nil)
		precondition(cardView!.card != nil)
		
		if let userCard = card as? UserCard {
			userCard.live = message["live"] as! Bool
			userCard.numberOfViews = message["views"] as! Int
			userCard.caller = nil
			if let callerID = message["caller_id"] as? Int {
				if callerID != 0 {
					if let users = message["users"] as? [String: AnyObject] {
						userCard.caller = API.sharedAPI.parseUserObject(users["\(callerID)"] as! [String: AnyObject], json: [:])
					}
				}
			}
			cardView!.card = userCard
		}
	}
}
