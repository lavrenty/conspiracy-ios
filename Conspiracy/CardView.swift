//
//  CardView.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit


class CardView: UIView {
	var actionDelegate: CardViewActionDelegate!
	var card: Card! {
		didSet {
			updateViews()
		}
	}
	
	private func updateViews() {}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.layer.cornerRadius = 6
	}
	
	class func cardView(card: Card) -> CardView? {
		let cardViewNibName: String
		switch(card.dynamicType) {
			case is UserCard.Type:
				cardViewNibName = "CardView-user"
				break
			
			case is SystemEndorsementCard.Type:
				cardViewNibName = "CardView-endorsement"
				break
			
			case is TopicCard.Type:
				cardViewNibName = "CardView-topic"
				break
			
			case is PushNotificationsCard.Type:
				cardViewNibName = "CardView-push-notifications"
				break
			
			default:
				Log("*** Card type \(card.dynamicType) is not supported")
				return nil
		}
		let cardViewNib = UINib(nibName: cardViewNibName, bundle: nil)
		let cardView = cardViewNib.instantiateWithOwner(nil, options: nil).first as! CardView
		cardView.card = card
		return cardView
	}
}


protocol CardViewActionDelegate {
	func endorse(card: SystemEndorsementCard)
	func addTopic(card: TopicCard)
	func enablePushNotifications(card: PushNotificationsCard)
	func answerQuestion(card: UserCard)
	func closeCard(card: Card)
}


// MARK: - Private Concrete Classes


class UserCardView: CardView {
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var avatarView: AvatarView!
	@IBOutlet weak var callerAvatarView: AvatarView!
	@IBOutlet weak var rateLabel: UILabel!
	@IBOutlet weak var topicsView: TopicsView!
	@IBOutlet weak var userNameLabel: UILabel!
	
	override func updateViews() {
		let usercard = card as! UserCard
		
		topicsView.lineHeight = 18
		topicsView.font = UIFont.systemFontOfSize(10, weight: UIFontWeightMedium)
		topicsView.allowsEditing = false
		topicsView.topics = usercard.topics
		topicsView.layoutMode = .SingleLineByTruncatingTail
		
		var isFree = true
		if let rate = usercard.price {
			if rate.doubleValue != 0 {
				isFree = false
				
				let font = rateLabel.font
				let attributedString = NSMutableAttributedString(string: NSLocalizedString("card.price.%@ per minute", comment: "Card"), attributes: [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(white: 175.0/255.0, alpha: 1.0)])
				let range = (attributedString.string as NSString).rangeOfString("%@")
				attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
				attributedString.replaceCharactersInRange(range, withString: rate.formattedValue)
				rateLabel.attributedText = attributedString
			}
		}
		rateLabel.hidden = isFree
		
		avatarView.user = usercard.user
		let textFormatString: String
		if let caller = usercard.caller {
			callerAvatarView.user = caller
			textFormatString = NSLocalizedString("card.text.user %@ is talking", comment: "Card")
		} else {
			callerAvatarView.image = UIImage(named: usercard.live ? "btn-card-call" : "btn-card-call-disabled")
			textFormatString = usercard.live ? NSLocalizedString("card.text.user %@ is asking", comment: "Card") : NSLocalizedString("card.text.user %@ was asking", comment: "Card")
		}
		let attributedString = NSMutableAttributedString(string: textFormatString, attributes: [NSFontAttributeName: userNameLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
		let range = (attributedString.string as NSString).rangeOfString("%@")
		attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
		attributedString.replaceCharactersInRange(range, withString: usercard.user.fullName)
		userNameLabel.attributedText = attributedString
		
		let lineHeight: CGFloat
		if CGRectGetHeight(UIScreen.mainScreen().bounds) > 568 {
			lineHeight = 22
		} else {
			lineHeight = 20
		}
		
		let style = NSMutableParagraphStyle()
		style.minimumLineHeight = lineHeight
		style.alignment = descriptionLabel.textAlignment
		style.lineBreakMode = descriptionLabel.lineBreakMode
		descriptionLabel.attributedText = NSAttributedString(string: usercard.text, attributes: [NSFontAttributeName: descriptionLabel.font, NSForegroundColorAttributeName: descriptionLabel.textColor, NSParagraphStyleAttributeName: style])
		
		self.backgroundColor = usercard.live ? UIColor.whiteColor() : UIColor(white: 220.0/255.0, alpha: 1.0)
		topicsView.bubbleBackgroundColor = UIColor(white: usercard.live ? 237.0/255.0 : 200.0/255.0, alpha: 1.0)
	}
	
	func setCallParticipants(callee: User, caller: User) {
		avatarView.user = callee
		callerAvatarView.user = caller
		let interlocutor = caller == User.currentUser ? callee : caller
		let textFormatString = NSLocalizedString("call.card.text.in call with %@", comment: "Call")
		let attributedString = NSMutableAttributedString(string: textFormatString, attributes: [NSFontAttributeName: userNameLabel.font, NSForegroundColorAttributeName: UIColor(white: 155.0/255.0, alpha: 1.0)])
		let range = (attributedString.string as NSString).rangeOfString("%@")
		attributedString.setAttributes([NSForegroundColorAttributeName: UIColor.blackColor()], range: range)
		attributedString.replaceCharactersInRange(range, withString: interlocutor.fullName)
		userNameLabel.attributedText = attributedString
	}
}


// MARK: -


class EndorsementCardView: CardView {
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var avatarView: AvatarView!
	@IBOutlet weak var actionButton: UIButton!
	
	override func updateViews() {
		let theCard = card as! SystemEndorsementCard
		descriptionLabel.text = NSString(format: NSLocalizedString("card.text.endorse %@ in %@", comment: "Card"), theCard.user.fullName, theCard.topic.name) as String
		avatarView.user = theCard.user
		actionButton.setTitle(NSLocalizedString("card.button.endorse", comment: "Card"), forState: .Normal)
	}
	
	@IBAction func endorse() {
		actionDelegate.endorse(card as! SystemEndorsementCard)
	}
}


// MARK: -


class TopicCardView: CardView {
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var actionButton: UIButton!
	
	override func updateViews() {
		let theCard = card as! TopicCard
		descriptionLabel.text = NSString(format: NSLocalizedString("card.text.add topic %@", comment: "Card"), theCard.topic.name) as String
		actionButton.setTitle(NSLocalizedString("card.button.add topic", comment: "Card"), forState: .Normal)
	}
	
	@IBAction func addTopic() {
		actionDelegate.addTopic(card as! TopicCard)
	}
}


// MARK: -


class PushNotificationsCardView: CardView {
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var actionButton: UIButton!
	
	override func updateViews() {
		descriptionLabel.text = NSLocalizedString("card.text.enable push notifications", comment: "Card")
		actionButton.setTitle(NSLocalizedString("card.button.enable push notifications", comment: "Card"), forState: .Normal)
	}
	
	@IBAction func enablePushNotifications() {
		actionDelegate.enablePushNotifications(card as! PushNotificationsCard)
	}
}
