//
//  Debugging.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 11.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation


#if DEBUG
	func Log(message: String, functionName: String = #function, filePath: String = #file, line: Int = #line) {
	struct Internals {
		static var token: dispatch_once_t = 0
		static var timeFormatter: NSDateFormatter!
		static var queue: dispatch_queue_t!
	}
	dispatch_once(&Internals.token) {
		Internals.timeFormatter = NSDateFormatter()
		Internals.timeFormatter.dateFormat = "HH:mm:ss.SSS"
		
		Internals.queue = dispatch_queue_create("log-queue", DISPATCH_QUEUE_SERIAL)
	}
	
	let date = NSDate()
	dispatch_async(Internals.queue) {
//		print("\(Internals.timeFormatter.stringFromDate(date)) \(functionName) \(message)")
		
		let fileURL = NSURL(string: filePath)!
		let fileName = fileURL.URLByDeletingPathExtension!.lastPathComponent!
		print("\(Internals.timeFormatter.stringFromDate(date)) \(fileName):\(line) \(message)")
	}
}
#else
func Log(message: String) {}
#endif


@available(iOS, deprecated=1.0, message="*** NOT IMPLEMENTED ***")
func FunctionNotImplemented(msg: String) {}
