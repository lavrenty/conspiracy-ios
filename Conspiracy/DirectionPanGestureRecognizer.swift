//
//  DirectionPanGestureRecognizer.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 29.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass


enum PanDirection {
	case Vertical, Horizontal
}


class DirectionPanGestureRecognizer: UIPanGestureRecognizer {
	let direction: PanDirection
	
	init(direction: PanDirection, target: AnyObject, action: Selector) {
		self.direction = direction
		super.init(target: target, action: action)
	}
	
	override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent) {
		super.touchesMoved(touches, withEvent: event)
		
		if self.state == .Began {
			let velocity = velocityInView(self.view!)
			
			switch direction {
				case .Horizontal where fabs(velocity.y) > fabs(velocity.x):
					self.state = .Cancelled
				case .Vertical where fabs(velocity.x) > fabs(velocity.y):
					self.state = .Cancelled
				default:
					break
			}
		}
	}
}
