//
//  Topic.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 4.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import Foundation
import UIKit


class Topic: APIEntity {
	var name: String!
	var callCount: Int = 0
	var isSuggested = false
//	var isReviewed: Bool?
//	var isPublic: Bool?
	var bubbleSize: CGSize?
	
	convenience init(name: String) {
		self.init(identifier: 0)
		self.name = name
	}
	
	override var description: String {
		return "<\(self.dynamicType) id=\(identifier): \(self.name)>"
	}
}
