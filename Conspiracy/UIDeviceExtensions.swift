//
//  UIDeviceExtensions.swift
//  CallMe
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import UIKit
import Darwin


extension UIDevice {
	var platformType: String {
		get {
			var systemInfo = utsname()
			uname(&systemInfo)
			// This is Swift's convoluted way to treat "char a[N]" as "char *"
			let appleModelID = withUnsafePointer(&systemInfo.machine) {
				String.fromCString(UnsafePointer($0))!
			}
			
			if appleModelID == "i386" || appleModelID == "x86_64" {
				let platforms = [
					UIUserInterfaceIdiom.Unspecified: "Simulator",
					UIUserInterfaceIdiom.Phone: "iPhoneSimulator",
					UIUserInterfaceIdiom.Pad: "iPadSimulator",
					UIUserInterfaceIdiom.TV: "TVSimulator",
				]
				let platform = platforms[UIDevice.currentDevice().userInterfaceIdiom]
				return platform != nil ? platform! : platforms[UIUserInterfaceIdiom.Unspecified]!
			}
			
			return appleModelID
		}
	}
}
