//
//  PaymentCardEditor.swift
//  Conspiracy
//
//  Created by Konstantin Anoshkin on 12.01.16.
//  Copyright © 2016 Conspiracy Inc. All rights reserved.
//

import UIKit
import Stripe


class PaymentCardEditor: UIViewController {
	@IBOutlet weak var controllerTitleLabel: UILabel!
	@IBOutlet weak var cardField: STPPaymentCardTextField!
	@IBOutlet weak var saveButton: UIButton!
	var completion: ((cardInfo: PaymentCardInfo) -> Void)?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		controllerTitleLabel.text = NSLocalizedString("payment card.controller title", comment: "Payment Card")
		saveButton.setTitle(NSLocalizedString("payment card.button.save", comment: "Payment Card"), forState: .Normal)
		
		cardField.borderColor = UIColor.clearColor()
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		cardField.becomeFirstResponder()
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		cardField.resignFirstResponder()
	}
	
	@IBAction func close() {
		self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func save() {
		self.saveButton.enabled = false
		STPAPIClient.sharedClient().createTokenWithCard(cardField.cardParams) { (token, stripeError) -> Void in
			if let stripeToken = token {
				Log("Stripe token: \(stripeToken)")
				API.sharedAPI.addPaymentCard(stripeToken.tokenId, completion: { (error) -> Void in
					guard error == nil else {
						self.saveButton.enabled = true
						self.reportError(error)
						return
					}
					
					self.presentingViewController!.dismissViewControllerAnimated(true, completion: {
						self.completion?(cardInfo: PaymentCardInfo(cardBrand: stripeToken.card!.brand, last4Digits: stripeToken.card!.last4()))
					})
				})
			} else {
				self.saveButton.enabled = true
				self.reportError(stripeError)
			}
		}
	}
	
	func paymentCardTextFieldDidChange(textField: STPPaymentCardTextField) {
		saveButton.enabled = textField.valid
	}
}
