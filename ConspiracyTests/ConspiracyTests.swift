//
//  ConspiracyTests.swift
//  ConspiracyTests
//
//  Created by Konstantin Anoshkin on 1.12.15.
//  Copyright © 2015 Conspiracy Inc. All rights reserved.
//

import XCTest
@testable import Conspiracy


class PriceTests: XCTestCase {
	func testPrices() {
		XCTAssertNil(Money(cents: 0, currency: "invalid currency"), "Should be nil")
		XCTAssertNil(Money(cents: 0, currency: "ZZZ"), "Should be nil")
		XCTAssertNil(Money(cents: -1, currency: "USD"), "Should be nil")
		XCTAssertNotNil(Money(cents: 0, currency: nil))
		XCTAssertNotNil(Money(cents: 100, currency: "RUB"))
		
		let p1 = Money(cents: 100, currency: "GBP")
		XCTAssertNotNil(p1)
		let p2 = Money(cents: 100, currency: "GBP")
		XCTAssertNotNil(p2)
		XCTAssertEqual(p1, p2)
		let p3 = Money(cents: 100, currency: "JPY")
		XCTAssertNotNil(p3)
		XCTAssertNotEqual(p1, p3)
	}
}


class ObfuscatorTests: XCTestCase {
	func testObfuscation() {
		let a = "01234_abcdefghij+qrstuvwxyz-56789.ABCDEFGHIJ/QRSTUVWXYZ"
		XCTAssertEqual(a, DemangleString(DemangleString(a)))
	}
}


//class ConspiracyTests: XCTestCase {
//	
//	override func setUp() {
//		super.setUp()
//		// Put setup code here. This method is called before the invocation of each test method in the class.
//	}
//	
//	override func tearDown() {
//		// Put teardown code here. This method is called after the invocation of each test method in the class.
//		super.tearDown()
//	}
//	
//	func testExample() {
//		// This is an example of a functional test case.
//		// Use XCTAssert and related functions to verify your tests produce the correct results.
//	}
//	
//	func testPerformanceExample() {
//		// This is an example of a performance test case.
//		self.measureBlock {
//			// Put the code you want to measure the time of here.
//		}
//	}
//	
//}
